/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : design

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-11-25 11:49:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for catalogs
-- ----------------------------
DROP TABLE IF EXISTS `catalogs`;
CREATE TABLE `catalogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of catalogs
-- ----------------------------

-- ----------------------------
-- Table structure for element_events
-- ----------------------------
DROP TABLE IF EXISTS `element_events`;
CREATE TABLE `element_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `generated_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `event_id_foreignkey` (`event_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of element_events
-- ----------------------------

-- ----------------------------
-- Table structure for element_msgs
-- ----------------------------
DROP TABLE IF EXISTS `element_msgs`;
CREATE TABLE `element_msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_type` varchar(1255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of element_msgs
-- ----------------------------

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `designed` tinyint(1) NOT NULL DEFAULT '0',
  `catalog_id` int(11) DEFAULT NULL,
  `designer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_user_id_foreign` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of events
-- ----------------------------

-- ----------------------------
-- Table structure for external_msgs
-- ----------------------------
DROP TABLE IF EXISTS `external_msgs`;
CREATE TABLE `external_msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reply` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of external_msgs
-- ----------------------------

-- ----------------------------
-- Table structure for group_msgs
-- ----------------------------
DROP TABLE IF EXISTS `group_msgs`;
CREATE TABLE `group_msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of group_msgs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2016_11_22_161140_create_zips_table', '1');

-- ----------------------------
-- Table structure for msgs
-- ----------------------------
DROP TABLE IF EXISTS `msgs`;
CREATE TABLE `msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of msgs
-- ----------------------------

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) unsigned NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of notifications
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `setting_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `key` (`setting_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('msgActivateEmail', 's:372:\" ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.\";');
INSERT INTO `settings` VALUES ('contentUs', 's:3083:\"<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص م<strong>ن مولد النص العربى، حيث يمكنك أن تو</strong>لد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;<strong>هذا النص يمكن</strong> أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص<strong> منسوخ، غير منظم، غير منسق، أ</strong>و حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p><p><br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك<strong> مولد النص العربى زيادة عدد الفقرات</strong> كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>\";');
INSERT INTO `settings` VALUES ('goals', 's:3203:\"<p>أهدافناأهدافناأهدافناأهدافنا</p><p>أهدافنا أهدافنا</p><p><br></p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص م<strong>ن مولد النص العربى، حيث يمكنك أن تو</strong>لد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;<strong>هذا النص يمكن</strong> أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص<strong>&nbsp;منسوخ، غير منظم، غير منسق، أ</strong>و حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p><p><br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك<strong>&nbsp;مولد النص العربى زيادة عدد الفقرات</strong> كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>\";');
INSERT INTO `settings` VALUES ('vision', 's:3195:\"<p>رؤيتنارؤيتنارؤيتنارؤيتنارؤيتنارؤيتنارؤيتنا</p><p><br></p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص م<strong>ن مولد النص العربى، حيث يمكنك أن تو</strong>لد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;<strong>هذا النص يمكن</strong> أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص<strong>&nbsp;منسوخ، غير منظم، غير منسق، أ</strong>و حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p><p><br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك<strong>&nbsp;مولد النص العربى زيادة عدد الفقرات</strong> كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br>&nbsp;هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>\";');
INSERT INTO `settings` VALUES ('contact', 's:395:\"<p>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p><p><br></p>\";');
INSERT INTO `settings` VALUES ('terms', 's:1626:\"<p>شروط الإستخدامشروط الإستخدام</p><p>شروط الإستخدام</p><p>شروط الإستخدام</p><p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br>&nbsp;إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br>&nbsp;ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br>&nbsp;هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>\";');

-- ----------------------------
-- Table structure for subscribes
-- ----------------------------
DROP TABLE IF EXISTS `subscribes`;
CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `days` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ended` tinyint(1) NOT NULL DEFAULT '0',
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `started_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of subscribes
-- ----------------------------

-- ----------------------------
-- Table structure for subs_catalogs
-- ----------------------------
DROP TABLE IF EXISTS `subs_catalogs`;
CREATE TABLE `subs_catalogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `days` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `popular` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `msg` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of subs_catalogs
-- ----------------------------
INSERT INTO `subs_catalogs` VALUES ('1', 'المبتدئ', '700.00', '365', '1', '[\"\\u0633\\u0646\\u0629 \\u0648\\u0627\\u062d\\u062f\\u0629\",\"\\u0623\\u062e\\u062a\\u064a\\u0627\\u0631 \\u0627\\u0644\\u0635\\u0648\\u0631\",\"\\u0641\\u064a\\u062f\\u064a\\u0648 + \\u0635\\u0648\\u0631\",\"\\u0646\\u0633\\u062e \\u0648\\u0631\\u0642\\u064a\\u0629 \\u0648 \\u0631\\u0642\\u0645\\u064a\\u0629\"]', '2016-11-07 19:14:07', '2016-11-13 18:15:19', '0', '1', '<p>لتفعيل الإشتراك الرجاء تحويل قيمة الاشتراك و هي : <strong>700 ريال </strong>على الحسابات التالية :</p><p>مؤسسة ركن التينات للتجارة</p><p><strong>حساب رقم </strong>: &nbsp;2313574745</p><p><strong>آي بان</strong> : Sا5445425435453</p><p>و لتأكيد الحوالة الرجاء التواصل مع الرقم الآتي : <strong>5241545435454</strong></p>');
INSERT INTO `subs_catalogs` VALUES ('2', 'الأساسي', '1000.00', '365', '2', '[\"\\u0633\\u0646\\u0629 \\u0648\\u0627\\u062d\\u062f\\u0629\",\"\\u0623\\u062e\\u062a\\u064a\\u0627\\u0631 \\u0627\\u0644\\u0635\\u0648\\u0631\",\"\\u0641\\u064a\\u062f\\u064a\\u0648 + \\u0635\\u0648\\u0631\",\"\\u0646\\u0633\\u062e \\u0648\\u0631\\u0642\\u064a\\u0629 \\u0648 \\u0631\\u0642\\u0645\\u064a\\u0629\"]', '2016-11-07 19:17:04', '2016-11-12 21:05:06', '1', '1', '<p>لتفعيل الإشتراك الرجاء تحويل قيمة الاشتراك و هي : <strong>700 ريال </strong>على الحسابات التالية :</p><p>مؤسسة ركن التينات للتجارة</p><p><strong>حساب رقم </strong>: &nbsp;2313574745</p><p><strong>آي بان</strong> : Sا5445425435453</p><p>و لتأكيد الحوالة الرجاء التواصل مع الرقم الآتي : <strong>5241545435454</strong></p>');
INSERT INTO `subs_catalogs` VALUES ('3', 'المتقدم ', '1500.00', '365', '3', '[\"\\u0633\\u0646\\u0629 \\u0648\\u0627\\u062d\\u062f\\u0629\",\"\\u0623\\u062e\\u062a\\u064a\\u0627\\u0631 \\u0627\\u0644\\u0635\\u0648\\u0631\",\"\\u0641\\u064a\\u062f\\u064a\\u0648 + \\u0635\\u0648\\u0631\",\"\\u0646\\u0633\\u062e \\u0648\\u0631\\u0642\\u064a\\u0629 \\u0648 \\u0631\\u0642\\u0645\\u064a\\u0629\"]', '2016-11-07 19:18:20', '2016-11-12 21:05:06', '0', '1', '<p>لتفعيل الإشتراك الرجاء تحويل قيمة الاشتراك و هي : <strong>700 ريال </strong>على الحسابات التالية :</p><p>مؤسسة ركن التينات للتجارة</p><p><strong>حساب رقم </strong>: &nbsp;2313574745</p><p><strong>آي بان</strong> : Sا5445425435453</p><p>و لتأكيد الحوالة الرجاء التواصل مع الرقم الآتي : <strong>5241545435454</strong></p>');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngbh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `follower_id` int(11) DEFAULT NULL,
  `role` enum('admin','superadmin','designer','user') COLLATE utf8_unicode_ci DEFAULT 'user',
  `subscriber` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailverified` tinyint(1) NOT NULL DEFAULT '0',
  `subscribe_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  KEY `users_follower_id_foreign` (`follower_id`) USING BTREE,
  KEY `fk_subscribe_id` (`subscribe_id`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'userni ck nameas tna me', 'bardouni.naoufal@gmail.com', '$2y$10$4lDhZI9TeApboPylgqUbDOeliVfmOg05lJyv9CImD3PDq81Dc2Brq', 'ktks8Wd8SHi2tg8WFcEe6aJrxLaPXb0xh6f4xr4xb5fx2DmNS8F4sBxCBt2V', '2016-11-05 12:50:58', '2016-11-22 11:54:49', 'kjh', 'kjh', '2016-11-12', '34354', 'kjh', 'kjh', 'jkh', '4', 'user', '1', null, 'zDc8XjiIi44BpX3u8mijL2SSYwAlPNlxOwRep4ph14792480222564.jpeg', '1', '3');
INSERT INTO `users` VALUES ('2', 'user2', 'user2@gmail.com', '$2y$10$TrV/dMEv9MO26teys6uEHefdjVvg/kl52eyNOQl.YIESXclMMM1BS', '8UZlFuxxpQmHMfxRwoS8jzH4flyGVLNt3jN3o5SSlmyFwkmrExT4f6xu2RTy', '2016-11-05 12:51:22', '2016-11-22 01:31:09', '', '', '2016-11-12', '651324', '', '', '', '4', 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('3', 'admin', 'admin@gmail.com', '$2y$10$bItpPraBWw9rDG7P3mVWteQU4pSajGX9U34hafMJlVT3fkyB9uZHe', 'U60PeMsVwL7hIrRnS0blwEQLYbiQlOfJtzUp0MxCdWXATZX6s4Slqn9FwMV3', '2016-11-05 12:51:33', '2016-11-22 01:31:09', null, null, '2016-11-12', null, null, null, null, null, 'admin', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('4', 'designer1', 'designer1@gmail.com', '$2y$10$KJh6r3PDpapb0jwo/Mv5WuL7SljXWNszIiAOp9yNH08XaqbBn8FgW', 'dF8xu31vKkkqQYxlTTbk80pzN44ivdkC1w0AalL16NJ4cB7oloZ21Bhturr2', '2016-11-05 12:51:44', '2016-11-22 17:12:08', null, null, '2016-11-12', null, null, null, null, null, 'designer', '0', null, null, '1', '1');
INSERT INTO `users` VALUES ('5', 'designer2', 'designer2@gmail.com', '$2y$10$c1XUKxKYN..X4iQxuKA2tOryUlMWVo9kuH/C9sLGMofsxLLqs6m9W', 'oRhfFgGmfmcNbB3XPrL5yr31uFmwnfmyQTKNHvILjZicoFvKfXAYqGQv2WxQ', '2016-11-05 12:51:51', '2016-11-09 23:22:08', null, null, '2016-11-12', null, null, null, null, null, 'designer', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('56', 'sadmin', 'sadmin@gmail.com', '$2y$10$j2auM1pGDy00d.90xXHgYeXAsxggMozPxl5fshErQ5YrvafY8xtUG', '8rozpU83EmfNeFaTuE1FloIWDc0bmdZ5suv6xumaELcPv9RSxqHGGZ1MT70J', '2016-11-09 12:32:37', '2016-11-23 13:32:47', null, null, '2016-11-12', null, null, null, null, null, 'superadmin', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('57', 'user3', 'user3@gmail.com', '$2y$10$YHQWilE0NZHLJGYQhTANmO/5zfFPnbHWgWviY8dr1PqiBw6afYjD6', null, '2016-11-09 21:20:20', '2016-11-14 22:13:54', 'j', 'location', '2016-11-12', 'kh', 'k', 'street', 'h', '4', 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('58', 'gfh', 'user4@gmail.com', '$2y$10$2h.tyavc1E8qNyGz5LV5N.80dCqVn.oaTZbPIxDqildUMi12fDKVa', null, '2016-11-09 21:22:24', '2016-11-14 22:13:54', 'hf', 'f', '2016-11-12', 'gh', 'fg', 'fg', 'gh', '4', 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('69', '4435', '2919@gmail.com', '$2y$10$ERDpfamAjX9sMnva4cg6FOZ1tfsvyQiD.rdd2dfLEFTlq2kDNDjtm', null, '2016-11-12 16:35:54', '2016-11-14 22:13:54', null, null, '2016-11-12', null, null, null, null, null, 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('70', '2186', '6880@gmail.com', '$2y$10$QHByX41NFeKEzeZFHgSTB.0qft6J3VmmBkZswIx4P2g3PbypcQ/pS', null, '2016-11-12 17:04:48', '2016-11-14 22:13:54', null, null, '2016-11-12', null, null, null, null, null, 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('71', '7767', '6440@gmail.com', '$2y$10$qLp2plTqE19LnExz177zm.QIgfkTmv.EgQmvAanFUVrV1ybYQvTCe', null, '2016-11-12 17:27:13', '2016-11-14 22:13:54', null, null, '2016-11-12', null, null, null, null, null, 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('72', '1070', '3068@gmail.com', '$2y$10$tIXEDb0poj78VUB5ob6hseomfB3W9NascMUn5nHUZ9Ojq.z72h5zW', null, '2016-11-12 17:41:40', '2016-11-14 22:13:54', null, null, '2016-11-12', null, null, null, null, null, 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('73', 'name compe*let ', 'g@gmail.com', '$2y$10$ApA6Fle8kwEcmRRflz4krOp0mqqVAn8SZoHFyIrqUzdHwWVR1MtQi', '8p1kdoZksPy9rfvP1bUpcT4bYTS6IcsL4t2eeaEVlJGUi71GvXvIxzxptR90', '2016-11-13 14:30:33', '2016-11-20 11:27:11', 'agadir', 'lmlm', '2016-11-12', '3154', 'mail', 'street', 'olk', null, 'user', '0', null, 'VMWjazDkX3DEkF3XGe07dqmed35PgzSImrcTCt6S14790474338697.png', '0', '1');
INSERT INTO `users` VALUES ('74', 'name', 't@gmail.com', '$2y$10$yMXXZ7XDBBDsV22T8h.uTe22siNoon/K2EAFgCHeKr8dsvwB4I6qi', 'roWJe6myF5Si8FRI98oEaBHmAetyRoujymf99oN3CGyKnEBKo11sAOCziwNx', '2016-11-19 21:44:18', '2016-11-20 11:40:34', 'city', 'location', '2017-04-02', '543454', 'postal', 'street', 'nib', null, 'user', '0', null, null, '1', '1');
INSERT INTO `users` VALUES ('75', 'dfs', 'ggg@gmail.com', '$2y$10$f.Ukm.KpzsMQaxRgop3L1.s8gVTytlDF1M5Jp7XsOXqE5PJhZvRiW', null, '2016-11-20 11:08:04', '2016-11-20 11:08:04', 'fgh', 'ghg', '2016-11-07', '45645', 'fgh', 'ghgh', 'hgh', null, 'user', '0', null, null, '0', '1');
INSERT INTO `users` VALUES ('76', 'naoufal bardouni', 'bardouni@gmail.com', '$2y$10$znvo7Hc4iZjmC8n5Mgzu0.iEfRdfI65.wj09BNk7lQEV72gLBj9va', 'Bxynp1pRGrQSPjLyFT7aZxRnYvdLDZd0nAK2qo21t7NGpLJfrhlWAxupvLs0', '2016-11-20 11:41:28', '2016-11-20 12:22:14', 'agadir', 'sous masa ', '2016-11-08', '134354534', 'postal code ', 'bensergaou', 'aghroud', null, 'user', '1', null, null, '1', '2');
INSERT INTO `users` VALUES ('77', 'dkfjhdsfkjh', 'ttt@gmail.com', '$2y$10$si31nAYXgzZVf.Y11LnypexNhf7N9SsdODQjcQ.wsZyTKjy0FjVy2', null, '2016-11-22 11:36:38', '2016-11-22 11:36:38', 'hfjk,dhsqd', 'jhgjhg', '2016-11-22', '24542254', 'jhgjh', 'hjhgjhg', 'jhgj', null, 'user', '0', null, null, '0', '1');

-- ----------------------------
-- Table structure for verefyemailtable
-- ----------------------------
DROP TABLE IF EXISTS `verefyemailtable`;
CREATE TABLE `verefyemailtable` (
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of verefyemailtable
-- ----------------------------

-- ----------------------------
-- Table structure for zips
-- ----------------------------
DROP TABLE IF EXISTS `zips`;
CREATE TABLE `zips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of zips
-- ----------------------------
