<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password ;
    $name = "sadmin" ;
    return [
        // 'name' => $faker->name,
        // 'email' => $faker->unique()->safeEmail,
        // 'password' => $password ?: $password = bcrypt('123456'),
        // 'remember_token' => str_random(10),

        'name' => $name,
        'email' => $name . "@gmail.com" ,
        'password' => bcrypt('pppppp'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\GroupMsg::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'from_user_id' => \App\User::get()->random()->id ,
        'to_user_id' => \App\User::get()->random()->id,
    ];
});


$factory->define(App\Msg::class, function (Faker\Generator $faker) {
	$grp = \App\GroupMsg::get()->random() ;
    return [
        'content' => $faker->sentence,
        'from_user_id' => $grp->from_user_id ,
        'to_user_id' => $grp->to_user_id,
        "group_id" => $grp->id,
    ];
});

$factory->define(App\SubsCatalog::class, function (Faker\Generator $faker) {
    
    // return [
    //     "name" => "المبتدئ" ,
    //     "price" => 700 ,
    //     "days" => 365 ,
    //     "data" => [
    //         "سنة واحدة" ,
    //         "أختيار الصور" ,
    //         "فيديو + صور" ,
    //         "نسخ ورقية و رقمية" ,
    //     ],
    //     "order" => 1
    // ];

    // return [
    //     "name" => "الأساسي" ,
    //     "price" => 1000 ,
    //     "days" => 365 ,
    //     "data" => [
    //         "سنة واحدة" ,
    //         "أختيار الصور" ,
    //         "فيديو + صور" ,
    //         "نسخ ورقية و رقمية" ,
    //     ],
    //     "order" => 2
    // ];

    return [
        "name" => "المتقدم" ,
        "price" => 1500 ,
        "days" => 365 ,
        "data" => [
            "سنة واحدة" ,
            "أختيار الصور" ,
            "فيديو + صور" ,
            "نسخ ورقية و رقمية" ,
        ],
      "order" => 3
    ] ;

});