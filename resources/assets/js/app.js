
require('./bootstrap');

Vue.component('custom-check-box', require('./components/customCheckBox.vue'));
Vue.component('dropdown-menu-auth', require('./components/dropdownMenuAuth.vue'));
Vue.component('form-add-file', require('./components/form-add-file.vue'));
Vue.component('form-add-new-msg', require('./components/form-add-new-msg.vue'));
Vue.component('element-receive-msgs', require('./components/element-receive-msgs.vue'));
Vue.component('follow-user', require('./components/follow-user.vue'));
Vue.component('activat-button', require('./components/activat-button.vue'));
Vue.component('list-files', require('./components/list-files.vue'));
Vue.component('send-catalog', require('./components/send-catalog.vue'));
Vue.component('change-password', require('./components/change-password.vue'));
Vue.component('form-uploaf-newversion-catalog', require('./components/form-uploaf-newversion-catalog.vue'));


const app = new Vue({
	el: '#app' ,
	data : {
		stateNavBar : false ,
		price:null ,

		subscribe:null,
		toActivate:null,
		showingFile:null ,
	},
	methods : {
		tg(){
			this.stateNavBar = !this.stateNavBar ;
		},
		startActiving(data) {
			this.toActivate = data ;
		},
		continueActivate(){
			if(!this.subscribe){
				this.$emit("alert-danger","المرجو ادخال نوع الإشتراك") ;
				return ;
			}
			this.$http.post("/admin/users",{ user:this.toActivate , subscribe : Number.parseInt(this.subscribe) }).then(function (response) {
				this.$emit("activated",this.toActivate) ;
			});
		}
	}
});

app.$on("alert-danger",function (msg) {
	window.alert(msg) ;
}) ;