require('./bootstrapadmin');

Vue.component('change-password', require('./scomponents/change-password.vue'));
Vue.component('attributes-catalog', require('./scomponents/attributes-catalog.vue'));

Vue.component('table-users', require('./scomponents/tables/table-users.vue'));
Vue.component('table-designers', require('./scomponents/tables/table-designers.vue'));
Vue.component('table-msgs', require('./scomponents/tables/table-msgs.vue'));
Vue.component('table-catalogs', require('./scomponents/tables/table-catalogs.vue'));
Vue.component('table-events', require('./scomponents/tables/table-events.vue'));
Vue.component('table-subscriptions', require('./scomponents/tables/table-subscriptions.vue'));
Vue.component('table-externalmsgs', require('./scomponents/tables/table-externalmsgs.vue'));

Vue.component('pagination', require('./scomponents/pagination.vue'));
Vue.component('columns', require('./scomponents/columns.vue'));


const app = new Vue({
	el: '#app'
});

