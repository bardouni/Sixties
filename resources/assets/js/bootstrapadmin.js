window.Vue = require('vue');

window.cd = {
	id : 'الرقم' ,
	name:"الاسم",
	email:"البريد الإلكتروني",
	city:"المدينة",
	location:"المنطقة",
	birthday:"تاريخ الميلاد",
	phone:"الهاتف",
	postal:"البريد",
	street:"الشارع",
	ngbh:"الحي",
	follower_id:"رقم المتابع",

	title:"العنوان",
	created_at:"تاريخ الإنشاء" ,

	msgs : {
		created_at:"تاريخ الإرسال"
	} ,
	catalogs:{
		created_at:"تاريخ إنشاء المجلة" ,
		updated_at :"تاريخ آخر تعديل" ,
		user_id:"رقم العميل",
		designer_id:"رقم المصمم"
	} ,
	events:{
		title:"اسم الحدث",
		description:"وصف الحدث",
		user_id:"رقم المشترك",
		designer_id:"رقم المصمم",
		created_at:"تاريخ الإضافة"
	} ,
	subscriptions :{
		id:"رقم الاشتراك",
		user_id:"رقم المستخدم",
		days:"عدد أيام الإشتراك",
		name:"اسم الإشتراك",
		created_at:"تاريخ طلب الاشتراك"
	} ,
	externalmsgs :{
		id:" رقم الرسالة " ,
		name : "مرسل الرسالة" ,
		email : " البريد الاكتروني"
	}

} ;

window.mixin = {
	props:{
		json:String,
		table:String,
		hide :{
			type:Object,
			default:function () {
				return {};
			}
		}
	},
	data(){
		return {
			page : null ,
			data : [] 			
		}
	},
	methods : {
		updateData(data){
			this.data = data ;
		},
		fireUpdateData(){
			this.page = 1 ;
			Vue.nextTick(function () {
				this.$refs.paginator.getPage(this.page) ;
			}.bind(this)) ;
		} ,
		rest(){
			for(var e in this.model){
				if( e , this.hide[e] )
					continue ;
				this.model[e] = "" ;
			}
			this.$refs.paginator.getPage(null) ;
		},
		formatDate(v){
			var v = new Date(v) ;
			return v.getFullYear() + "-" + (v.getMonth() + 1) + "-" + v.getDate() ;
		}
	}
}

require('vue-resource');
Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});
