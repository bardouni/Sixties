@extends("sadmin/layouts/app")

@section("title","الإشعارات")

@section("content")

<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		

		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bell-o"></i>@yield('title')
				</div>
			</div>
			<div class="portlet-body">
				<ul class="list-group">
					@foreach($notifications as $notif)
					
						@include("sadmin/layouts/notifications/simple_" . snake_case(class_basename($notif->type)) )
					
					@endforeach
				</ul>
				@include('sadmin/layouts/pagination',['data' => $notifications])
			</div>
		</div>

	</div>
</div>

@endsection