@extends("sadmin/layouts/app")

@section("title","اشتراكات المستخدمين")

@push("active-subscriptions")
active open 
@endpush

@push("active-subscriptions-1")
active
@endpush

@section("content")

<div class="portlet bg-inverse light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-archive"></i> تفعيل اشتراك المستخدم : {{ $subscribe->user->name }}
		</div>
		<div class="actions">
			<a href="{{ route('sadmin.subscriptions.index') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-undo"></i>عودة الى قائمة الإشتراكات</a>
		</div>
	</div>
	<div class="portlet-body flip-scroll">

		@include("sadmin/layouts/msgs")

		<div class="row">
			<form class="col-xs-12 col-sm-6" method="post" action="{{ route('sadmin.subscriptions.update',$subscribe->id) }}" >
				{{csrf_field()}}
				{{method_field('PUT')}}
				<div class="form-group">
					<a href="/upssubscribe/{{ $subscribe->file }}" target="_blank" >
						<img src="/upssubscribe/{{ $subscribe->file }}" alt="" class="img-responsive">
					</a>
				</div>
				<div class="form-group">
					حالة الإشتراك : 
					@if(!$subscribe->activated)
					<span class="label label-danger">غير مفعل</span>
					@else
					<span class="label label-primary">مفعل</span>
					<span class="label label-info">{{ $subscribe->name }}</span>
					@endif
				</div>
				<div class="form-group">
					<label>الإشتراك</label>
					<select {{ $subscribe->ended ? 'disabled' : '' }} name="subscribecatalog" class="form-control" >
						@foreach($subs as $s)						
						<option {{ $s->id == $subscribe->saveUsersubscribeid ? 'selected' : '' }} value="{{$s->id}}" >{{$s->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group"><input {{ $subscribe->ended ? 'disabled' : '' }} type="submit" class="btn btn-primary btn-sm" value="تفعيل"></div>
				@if($subscribe->ended)
				<div class="note note-danger">
					<p>
						لا يمكن تفعيل الإشتراك لانه منتهي
					</p>
				</div>
				@endif
			</form>
		</div>

	</div>
</div>

@endsection