@extends("sadmin/layouts/app")

@section("title","اشتراكات المستخدمين")

@push("active-users")
active open 
@endpush

@push("active-users-4")
active
@endpush

@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-archive"></i> اشتراكات المستخدمين
		</div>
	</div>
	<div class="portlet-body flip-scroll">

		@include("sadmin/layouts/msgs")

		<table-subscriptions json="subscriptions" table="subscriptions" ></table-subscriptions>
	</div>
</div>

@endsection