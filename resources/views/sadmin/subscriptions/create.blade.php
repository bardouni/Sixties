@extends("sadmin/layouts/app")

@section("title","اشتراكات المستخدمين")

@push("active-subscriptions")
active open 
@endpush

@push("active-subscriptions-2")
active
@endpush

@section("content")

<div class="portlet bg-inverse light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-archive"></i> تفعيل اشتراك 
		</div>
		<div class="actions">
			<a href="{{ route('sadmin.subscriptions.index') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-undo"></i>عودة الى قائمة الإشتراكات</a>
		</div>
	</div>
	<div class="portlet-body flip-scroll">

		@include("sadmin/layouts/msgs")

		<div class="row">
			<form class="col-xs-12 col-sm-6" method="post" action="{{ route('sadmin.subscriptions.store') }}" >
				{{csrf_field()}}
				{{method_field('post')}}
				
				
				<div class="form-group">
					<label>الإشتراك</label>
					<select name="subscribecatalog" class="form-control" >
						@foreach($subs as $s)
						<option value="{{$s->id}}">{{$s->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label>المستخدم</label>
					<select name="user" class="form-control" >
						@foreach($users as $user)
						<option value="{{$user->id}}">{{$user->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group"><input type="submit" class="btn btn-primary btn-sm" value="تفعيل"></div>				
			</form>
		</div>

	</div>
</div>

@endsection