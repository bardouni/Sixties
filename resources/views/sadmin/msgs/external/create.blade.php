@extends("sadmin/layouts/app")

@section("title","رسالة جديدة")

@push("active-msgs")
open active
@endpush

@push("active-msgs-3")
active
@endpush

@section("content")

<div class="row">
	<form action="{{ route('sadmin.externalmsgs.store') }}" class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2" method="post" >
		
		@include("sadmin/layouts/msgs")

		{{csrf_field()}}
		{{method_field('POST')}}
		<div class="form-group"><label>مستقبل الرسالة</label><input type="text" value="{{ old('email') }}" class="form-control" name="target" /></div>
		<div class="form-group"><label>موضوع الرسالة</label><input type="text" class="form-control" name="subject" value="{{old('subject')}}"></div>
		<div class="form-group"><label>نص الرسالة</label><textarea name="contentmsg" class="form-control" rows="5">{{old('contentmsg')}}</textarea></div>
		<div class="form-group">
			<button class="btn btn-primary btn-sm">إرسال</button>
		</div>
	</form>
</div>

@endsection