@extends("sadmin/layouts/app")

@section("title","رسائل خارجية")

@push("active-msgs")
open active
@endpush

@push("active-msgs-3")
active
@endpush

@section("content")

<div class="portlet bg-inverse light">
	<div class="portlet-title">
		<div class="caption font-purple">
			<i class="fa font-purple fa-comments fa-fw"></i> الرسائل
		</div>
		<div class="actions">
			<a href="{{ route('sadmin.externalmsgs.create') }}" class="btn btn-default btn-envelop-o">إنشاء رسالة جديدة</a>
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		
		@include("sadmin/layouts/msgs")

		<table-externalmsgs json="externalmsgs" table="externalmsgs" ></table-externalmsgs>
	</div>
</div>

@endsection