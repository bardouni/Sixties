@extends("sadmin/layouts/app")

@section("title","رسالة رقم : " . $msg->id  )

@push("active-msgs")
open active
@endpush

@push("active-msgs-3")
active
@endpush

@section("content")
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">

		<div class="portlet bg-inverse light ">
			<div class="uab portlet-title">
				<div class="caption font-blue ">
					<i class="fa font-blue fa-envelope"></i> الرسالة
				</div>
			</div>

			<div class="portlet-body flip-scroll">

				@include("sadmin/layouts/msgs")

				<table class="table table-bordered table-striped table-condensed flip-content">
					<tbody>						
						
						<tr>								
							<td> رقم الرسالة </td>
							<td>{{ $msg->id }}</td>
						</tr>								
						<tr>
							<td> المرسل </td>
							<td>{{ $msg->name }}</td>
						</tr>								
						<tr>
							<td> البريد الاكتروني</td>
							<td>{{ $msg->email }}</td>
						</tr>
						<tr>
							<td> محتوى الرسالة</td>
							<td>{{ $msg->content }}</td>
						</tr>								
						<tr>
							<td>عمليات</td>
							<td>
								<form action="{{ route('sadmin.externalmsgs.destroy',$msg->id) }}" method="post" >
									{{csrf_field()}}
									{{method_field("delete")}}
									<button type="submit" class="btn red btn-sm btn-outline" >
										<i class="fa fa-fw fa-trash"></i>
										حذف الرسالة
									</button>
								</form>
							</td>
						</tr>
						<tr>								
							<td>الرد</td>
							<td>
								@if(!$msg->reply)
								<div class="text-danger">لا يوجد رد</div>
								@else
								{{$msg->reply}}
								@endif
							</td>
						</tr>
					</tbody>
				</table>				
				<div class="row">
					<form action="{{ route('sadmin.externalmsgs.store') }}" class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2" method="post" >
						{{csrf_field()}}
						{{method_field('POST')}}
						<input type="hidden" value="{{ $msg->email }}" name="target" />
						<input type="hidden" value="{{ $msg->id }}" name="msg" />
						<div class="form-group"><label>موضوع الرسالة</label><input type="text" class="form-control" name="subject" value="{{old('subject')}}"></div>
						<div class="form-group"><label>نص الرسالة</label><textarea name="contentmsg" class="form-control" rows="5">{{old('contentmsg')}}</textarea></div>
						<div class="form-group">
							<button class="btn btn-default btn-sm">إرسال</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection