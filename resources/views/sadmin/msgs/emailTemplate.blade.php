<!DOCTYPE html>
<html lang="ar" dir="rtl" style="height: 100%;width: 100%;text-align: right !important;" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $subject }}</title>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="height: 100%;background-color: #F0F0F0;color:#333333;font-family: arial ,sans-serif" >
        <div class="small-container" style=";max-width: 610px; margin-left: auto; margin-right: auto; padding-top: 2em; padding-bottom: 2em;" >
            <div class="content" style="text-align: right;background-color: #fff ; border-radius: 5px ; padding: 1em ;" >
                <h4 style="color: #5C94CD; font-size: 18pt; text-align: center; padding: 1pt; font-weight: 700; margin-bottom: 50px;" >{{ $subject }}</h4>                
                <div style="text-align: right;" dir="rtl" >
                    {{$content}}
                </div>
                <hr style="margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eeeeee; " >
                <p style="font-size: 12px; color: #777;text-align: center;" >© {{date('Y')}} {{ config('app.name') }} جميع الحقوق محفوظة .</p>
            </div>
        </div>
    </body>
</html>