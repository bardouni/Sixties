<form class="rows" action="{{ route('sadmin.msgs.newmsgs',$group->id) }}" method="post" >
	{{csrf_field()}}
	{{method_field('POST')}}
	<div class="col-xs-12 col-sm-8 col-sm-offset-2">
		<div class="form-group"><textarea name="contentmsg" rows="5" class="form-control"></textarea></div>		
		<div class="form-group text-center">
			<button type="submit" class="btn btn-success">إرسال</button>
		</div>
	</div>
</div>