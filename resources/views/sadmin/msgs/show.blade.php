@extends("sadmin/layouts/app")

@section("title", "الرسائل :" . $group->title)

@push('active-msgs')
active open 
@endpush
@push('active-msgs-1')
active
@endpush

@section("content")

<div class="portlet portlet-msgs light bg-inverse">
	<div class="portlet-title text-center">
		<div class="caption">
			<i class="fa fa-envelope-open-o"></i> {{ $group->title }}
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		<div class="clearfix">
			<ul class="media-list">
				@foreach($msgs as $msg)
				
				<li class="media media-msgs">
					<a class="pull-left" href="javascript:;">
						<img class="media-object" src="{{ isset($msg->fromUser) ? $msg->fromUser->completAvatarUrl : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAAD6JJREFUeJztnWmQXUUVgL9kQobscRKTkMlCSJhAQkTZhERFIGCIRhbZCqhCrIi7sgglVmFZUkgJYilSKiBSsgkFAlpAkE0gBANGwCAhkIWEQPaN7JNkZvxxZpKZyZt5777X3efe2+erOjVvkjfd557b5/ZyT58GwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwyiJLtoKGHvoClQ3f64HGhV1MZoxBwlHd+DwZhkPHASMAAYDNex1jhbqgXXAamApsAh4C/gvMBfYFUTryDEH8ctRwKnAycDRwP6Oyt0GvAo8DcwAXndUrmF451DgBmAJ0BRIFgHXA3X+L88wklMFnAXMJJxTdCTPA6cjcxrDUKUKuAhYiL5jtJd3gAswRzGUmALMQ98Rislc4CRPNjCMfagFHkW/4SeVB4EhHuxhGHu4CPgI/cZerqwHznduFSN6+gAPoN/AXcndQE+nFjKipY5szDWSylxgtEM7GRFyPDIs0W7MvmQNMNGZtYyoOB0J99BuxL5lOzDVkc2MSDgHiXXSbryhZCdwhhPLGbnnNOJyjhapR+LGDKNDJgE70G+sWrINOKZiKxq5ZDSwFv1Gqi2rkDB8w9hDT2TZU7txpkXm4C4038gBd6LfKNMmv6/IokZuOBv9xphWmVaBXY0cMBB5WabdENMqK4D+ZVvXyDx3od8I0y63lW1dI9NMRL/xZUEagCPKtLGRYWaj3/iyIi+UaWMjo3wZ/UaXNTm5LEsbmWQO+g0ua/JSWZY2Msfn0W9sWZXjkps728SY8eL72gpkmOhsF1tmxSHAMqCbtiIZpR5JWrFOW5FQxNaDXIA5RyVUA+dpKxGS2BzkXG0FckBUNoxpiFULfKCtRA5oRIaqa7QVCUFMPcgUbQVyQlfgFG0lQhGTg1j6TXdM1lYgFDE5yCRtBXJENLaMZQ4yBAndNtxRA2zQVsI3sfQgn9BWIIdEYdNYHGSctgI5JAqbxuIgB2krkENGaSsQglgcZLi2AjlkpLYCIYjFQT6urUAOicKmsThIjbYCOWSAtgIhiMVB7LAY9/TQViAEsTjIftoK5JBqbQVCEIuDNGorYGSTWBxkh7YCOaReW4EQxOIg27QVyCGbtRUIQSwOslZbgRwShU1jcRALVHTPSm0FQhCLg9hOQvcs1VYgBLE4yHxtBXLIu9oKhCAWB5mnrUAOeUtbgRDEsmGqJ/ARlvLHFTuBPs0/c00sPcg24A1tJXLEG0TgHBCPgwDM0lYgR7yorUAoYnKQp7QVyBFPaysQiljmICDHGq/DInsrZTOyF8RCTXLGDuBJbSVywBNE4hwQl4MA3KOtQA64T1uBkMQ0xALoDiwnkt1wHlgNDAN2aSsSith6kJ3AndpKZJg7iMg5IL4eBCTDyWLspWFSdiGpfj7UViQksfUgICdMPaCtRAa5h8icI2YORp6I2odiZkV2AaPLsnTGibEHAViAzUWScDuwSFsJIyyDgI3oP53TLhuAgWXaOPNUaSugyNZmOVVbkZRzGRHFXhltqQJeRv8pnVZ5gThXOo1WjAG2oN8Y0yabiCSDu1Gci9FvkGmT8yuyqJE7bkW/UaZFbqnQlkYO6Y5MRrUbp7Y8i0UZGB3wMSQZgXYj1ZI3gf4VW9HINcORl2LajTW0LASGOrCfEQGxOckC7Jg6IyG1wFz0G69veR05R94wEtMXeAb9RuxLnmy+RsMom27ATeg3ZtdyI3GHGhmOOQtYj37DrlTWAac5to1hALLK8wT6jbxceQJbqTICcBGSwEC7wZcqK4ALvFjCMDqgP3ADkvdX2wE6kq3AddhE3FBkKPArJOOgtkO0yCZkEm7Lt0Zq6A9cgRwso+UY84BLgX6er9UwKuI44Gbgffw7xRKkBzsmxIXFhu0W888E4GTgs8CngQMqLO8D4FVgJpJlPYqTnrQwBwnPIGAcspNxBDAYqAF6IyH3IMmhtyDvXVYiPdFCZAgVxfHLaSF2B6kGxiINtkXGA/8BvoVMvGOgFrgLmdTPayfvEMlpUoakrzkPyTE7H9hNx2P7BcCROmoGZSqwho7tsBt4G7gNOBvp7Yyc0B2YDPwCeA1oJNkEuB5ZFcoj+wG/JLlNGoA5wPXASc3lGBnjMODXyHjdxUrR38nXkQkHAq/gxjarEEc7JOQFGMnpDUwHZuNnOXUZshKVdb6CZEv0YaNZSIaYXsGuxihKX+Ba5O2xj5vefjx+DdnMaVwN/A7/NmpCUrpegzmKKtXA5XQ+wfQlz1H5+4yQ1CFnm4e200rgu9g8JShdgK8CSwl/w1vLamCK30t1woXox4ktxqKJgzCMdG2BbUSCAtP4hOyJHPGgbaPW8hjyYtTwwDmkd0ffbNKVx3YC8pJP2y6FZDW2o9EpfYG70b+xxWQjsi1Xm0tI9x6VFrkdWXk0KmAE8hZX+2YmkT8A+/swRhH6AveXoa+mWMqhChiPRK5q38RyZC5wqHuTdMiRSDCj9nWXI4uRwE0jAZNI73yjVNkKfM21YQrwAyQkRvt6K5FVxBH35oQpZGMMXarcC/RxaiGhBng0BdfnSjYDxzu1UA45gnye9uQ6MngiYXYrhpYNhB2aZorhwHL0b5IvqUeGQ5XQBbiafJ/x/h72rmQf+hBHougmJDK4nD0Vg4B/pED/EPIK0KMMG+WSLsAM9G9KSEkaGXwi+e5dC8lDCeyTay5F/2ZoSCmRwVXAz5BNStr6asj0TmwTBXXka8WqHHmWwpHBtcg55dr6acomYGQB20RBFfAv9G9CGmQVbSODi+0Tj0meI9LkIlehb/w0SSOS6/dGku8Tz7t8DyW0PHMY8m5AI2bJyB5bgNFIJHBQtE4YslSZRhK6Iw/TGaEr1uhBxiLpMu34LyMJO5G2syRkpRqN9FYkUtcwklCFpF56JGSloXuQo5HEy4ZRDo3A4cD/QlUYOn3NFYHrM/JFV+CykBWG7EEGIyEWaUxwYGSH7cipXhtDVBayB5mOOYdROT2QtE9BCNWDVCFbK0cEqs/IN+8iuYCbfFcUqgf5IuYchjvqkMzy3gnlIBcGqseIhyCZGkMMsaqRwDsf+7KNeFmHLPw0+KwkRA8yGXOOtLMd+DGyhJqVY+cGkI/jKLgd/WhQk8LSANxH27SptcCfyMa+99+Qcboiex20DWnSVhYhR6gd1PGtYxjwU+QsR219O5KlnejvBN9zkGORTVGGDg1IiPgSJJn1v5FdivMTljMayVl1DHIS8Chk/J+G91qHIwk/MsmV6D9lYpJ6ZM43EJn3+X4A9kbmAhPRO4PkO56v0St/Q7/RxCTzSrstXnipE718yv0+L8rnKlYX5MlihKNJWwEFPuOz8G4eyz4E6eqNcJQ7JxiOpH3tihxJsKSMMrqXWXel1CLHWi/xUbjPHmSSx7KNwvRM+P3BwMPIatCjzZ/fAx5HGl4SNDMhensf4tNBjvJYtlGYvgm+WwPMBM5g38n8VOBlkuXITVK3a7wdneDTQWxbbXh6U/ow6+fAwc2f3wamAaciS8EgwaU3Jah7QILvuiaTbW0d+qs6McrQEu7NfrQ9ZqL1RHcke/Ny7aS0cwR7KV/zhyXoWBa+epAhlJfB3KicUhxkBNKoW5jT6vNS5Og7EEc6mOKUUqdPhgL9fRTsy0Ey2eXlhJElfKf9MGx3u993tvpc7ahO34zzUagvB/GirFESncVX+SINZ8R7eSj7eg8y2lO5RnFKeThNaPf7VGTe0ULr5eLxwOwi5aVhxJCpNvcQ+pPVWOWVIvfmujLKvLlImU+l4LrvKaJjWfgaYg3zVK5RnAl0vtR7Vgf/voWON0udU6TOTxVTKgBe2pwvB0n6FtZwRw8kBLwjCqWb/TMSFlQD/LbA/3c2FB9NOkKKMuMgVcgyr6HHcQm/fzUSKr8b+BFt5yPFODZhXb7w8lD24SBD8BsEaRRncsLvt36H0I9k7eKUhHX5Yn889GQ+HGSQhzKNZJxIssje1vluL0/wd12ALyT4vm+cn6/uw0G8vNE0EtEbOCHB91sfTPN4gr87Gg+NsgL6uS7QHCS/nF/i9zYDj7X6/QXkTPZSCJK8LQHO254PB3HuxUZZnEnhPRpN7X7vg4SWtLxPaGTf2KpCk/Yq4NwKdXSN9SBGyfSh8BP++TLKKvQ3Z5Ku4RV4aHs+VpusB0kPPwTuoG2v8W3gRUpv3OuAvxT496sqU80LztueDwfR3FlmtGUssmPw4Vb/tgu4u8JyTySdO0adtz0fQyytzftGYX6B23vSlWQ7DUPivO35cBB7SZguxpDs3UYxLgE+6bA8lzjP9OjDQdKQjtJoy0+AwxyUMwrJ6ZtWzEGMsugB/JXKjqGoRrYxpHmVMhMOYkOsdFIH3Et5jagrshp2hFON3JMJB7EeJL1MQ1a0Stln3kIVsuqVtrfmhciEgyQJlTbC8yUk9qqUTCQDgUcoPWxFm12uC/ThIPUeyjTccgLwJh3vLgSY0vydaUE0csMG1wX6cJAdHso03FMDPAjMAk5jb/rRU4BnkF4maxvfNrou0MeEepuHMg1/TEQSVy9EhiiH6qpTEZlwkFUeyjT8M0ZbAQc4dxAfQ6xS9xIYhmsyMQfxlkjYMIqw1nWBPg55HAKs8FCuYXTGbiTc3ekc2EcPshI5etgwQvIGHhaIfCWOm1P8K4bhlFk+CvXlIE95KtcwOuJlH4X6Omi+DnjHU9mGUYhheFgg8tWDvAu86qlsw2jPDDytnhZKZOyKRiSEwTB8czGwzEfBvoZYICHV85FD3g3DFy8Cx/sq3GcP0oCEnXQWMWoYlfINYJG2EpVgp02Z+JJb8IzPIVYL/ZBjwcYGqCurNCBvghuQhZPu+FtAyQuPAacjNvNGCAcBOABJX1kXqL7QNCLRA8uRSII1zb+vBtYjUaYbmn9uAbYib3230vEuuG7IPK699AIGdCKDkSXPgYS7v6F5DfgcYj+vhDTgAOAu5ETVrLEJeK9ZlgLvN8sy4APEKbw+ycqgGjl1aVg7ORA5KnoUbU+zzQJNwB+BK4GPQlQY+gnTBZgOXEv6Eh+vQDYNLWr1s0XWK+rlk8GIs7SWMc2Stt2Ei4CvA/8MWalWF9wbWbv+JqWd6+2CBuSp37rhL2yWxQTorjNGb+SAzhaHGc1eJxpOmPRODcBzwP1IAu3tAepsQxrGqOOQYdckJKXlgWWWsxnpBZazd/izlL1Do/fxkPUiUroBI9jrMC3Dt9pWP8vJtL4VWICEKc1E9syrRoanwUHa04O9Bu+HZAOsRnRtQpJCbEUcYj2Snn8t1gOkjV5IYoh+7aQn8qCqRw7uqUfu3SIkXKRJQ1nDMAzDMAzDMAzDMAzDMAzDMAzDMIw9/B+vhKU2DzeRvgAAAABJRU5ErkJggg==' }}" alt="64x64" style="width: 64px; height: 64px;">
					</a>
					<div class="media-body">
						<h4 class="media-heading uab {{ $msg->from_user_id == $oneSide ? 'font-blue' : 'font-green' }}">						
							@if($msg->from_user_id == 0)
							الإدارة
							@else
							{{$msg->fromUser->name}}
							@endif
							<br>
							<small><small>
								<i class="fa fa-fw fa-clock-o"></i>
								{{ $msg->humanstime }}
							</small></small>
						</h4>
						{{ $msg->content }}
					</div>
				</li>

			</ul>
			@endforeach

			<!-- add Send Msgs Here -->
			@if($group->withAdmin)

				@include("sadmin/msgs/formsubmit")

			@endif
		</div>


	</div>
</div>

@endsection