@extends("sadmin/layouts/app")

@section("title","رسالة جديدة")

@push('active-msgs')
active open 
@endpush
@push('active-msgs-2')
active
@endpush

@section("content")

<div class="col-xs-12 col-sm-8 col-sm-offset-2">	
	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-envelope"></i>الرسائل
			</div>		
		</div>
		<div class="portlet-body">

			<div class="clearfix">
				@include("sadmin/layouts/msgs")
			</div>
			
			<form action="{{ route('sadmin.msgs.store') }}" method="post" >
				{{csrf_field()}}
				{{method_field('POST')}}
				<div class="form-group">
					<label>الوجهة</label>
					<select name="target" class="form-control">
						@foreach($users as $user)
						<option value="{{ $user->id }}">{{$user->name}}</option>
						@endforeach
					</select>						
				</div>
				<div class="form-group">
					<label>موضوع الرسالة</label>
					<input type="text" class="form-control" name="titlemsg" value="{{ old('titlemsg') }}" />
				</div>
				<div class="form-group">
					<label>محتوى الرسالة</label>
					<textarea name="contentmsg" class="form-control">{{ old('contentmsg') }}</textarea>
				</div>
				<div class="form-group"><button class="btn blue">إرسال</button></div>
			</form>
			
		</div>
	</div>
</div>

@endsection