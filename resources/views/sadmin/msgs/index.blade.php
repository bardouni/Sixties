@extends("sadmin/layouts/app")

@section("title","الرسائل")

@push('active-msgs')
active open 
@endpush
@push('active-msgs-1')
active
@endpush

@section("content")

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-envelope"></i>الرسائل
		</div>		
	</div>
	<div class="portlet-body flip-scroll">		
		<table-msgs json="msgs" table="msgs" ></table-msgs>
	</div>
</div>

@endsection