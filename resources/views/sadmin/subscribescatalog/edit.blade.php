@extends("sadmin/layouts/app")

@section("title",$catalog->name)

@push('active-subscribescatalog')
active open
@endpush

@push('active-subscribescatalog-1')
active
@endpush


@push('scripts')
<script src="/sadminassets/js/editor/jquery.tinymce.min.js" ></script>
<script src="/sadminassets/js/editor/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: 'textarea',
		plugins: [],
		toolbar:'' ,
		menubar:false ,
		language: 'ar' ,
		height:'300px'
	})
</script>
@endpush


@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-star-o"></i> الباقة : {{$catalog->name}}
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		@include('sadmin/layouts/msgs')
		<div class="row">
			<form class=" col-xs-12 col-sm-6" action="{{ route('sadmin.subscribescatalog.update',$catalog->id) }}"  method="post" >
				{{csrf_field()}}
				{{method_field('PUT')}}
				<div class="form-group">
					<label>رقم الباقة</label>
					<input disabled="true" value="{{ old('id') ?: $catalog->id }}" class="form-control" />
				</div>
				<div class="form-group">
					<label> اسم الباقة </label>
					<input value="{{ old('name') ?: $catalog->name }}" name="name" class="form-control" />
				</div>
				<div class="form-group">
					<label>السعر</label>
					<input value="{{ old('price') ?: $catalog->price }}" name="price" class="form-control" />
				</div>
				<div class="form-group">
					<label>عدد الأيام</label>
					<input value="{{ old('days') ?: $catalog->days }}" name="days" class="form-control" />
				</div>
				<div class="form-group">
					<label>الترتيب</label>
					<input value="{{ old('order') ?: $catalog->order }}" name="order" class="form-control" />
				</div>
				<div class="form-group">
					<label>الحالة</label>
					<p>
						<label><input type="radio" value="0" name="visible" {{ old('visible') ?: $catalog->visible ? '' : 'checked' }} />ملغي</label>
						<label><input type="radio" value="1" name="visible" {{ old('visible') ?: $catalog->visible ? 'checked' : '' }} />مفعل</label>
					</p>
				</div>

				<div class="form-group">
					<input value="1" type="checkbox" name="popular" {{ old('popular') ?: $catalog->popular ? 'checked' : '' }} />
					<label>الافضل</label>
				</div>

				<div class="form-group">
					<label>خصائص</label>					
					<attributes-catalog :attrs="{{json_encode(old('data') ?: $catalog->data)}}" />
				</div>

				<div class="form-group">
					<label>رسالة الإشتراك</label>
					<textarea name="msg" id="editor" >{!! old('msg') ?: $catalog->msg !!}</textarea>
				</div>

				<div class="form-group">
					<button class="btn btn-primary">حفظ</button>				
				</div>

			</form>				
		</div>
	</div>
</div>

@endsection