@extends("sadmin/layouts/app")

@section("title","الباقات")

@push('active-subscribescatalog')
active open
@endpush

@push('active-subscribescatalog-1')
active
@endpush

@section("content")
<div class="portlet bg-inverse light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-star-o"></i> الباقات
		</div>
		<div class="actions">
			<a href="{{ route('sadmin.subscribescatalog.create') }}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> إضافة باقة </a>
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		@include('sadmin/layouts/msgs')
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class="flip-content">
				<tr>
					<th>رقم الباقة</th>
					<th> اسم الباقة </th>
					<th>السعر</th>
					<th>عدد الأيام</th>
					<th>الترتيب</th>
					<th>الحالة</th>
					<th>الافضل</th>
					<th>خصائص</th>
				</tr>
			</thead>
			<tbody>
				@foreach($catalogs as $catalog)
				<tr>
					<td>{{ $catalog->id }}</td>
					<td><a href="{{ route('sadmin.subscribescatalog.edit',$catalog->id) }}" >{{ $catalog->name }}</a></td>
					<td>{{ $catalog->price }}</td>
					<td>{{ $catalog->days }}</td>
					<td>{{ $catalog->order }}</td>
					<td>
						@if($catalog->visible)
						<span class="label label-success">مفعل</span>
						@else
						<span class="label label-danger">ملغي</span>
						@endif
					</td>
					<td>
						@if($catalog->popular)
						<span class="label label-primary">نعم</span>
						@else
						<span class="label label-default">باقة عادية</span>
						@endif
					</td>
					<td>
						@foreach($catalog->data as $attribute)
						<ul>
							<li>{{$attribute}}</li>
						</ul>
						@endforeach
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>		
	</div>
</div>

@endsection