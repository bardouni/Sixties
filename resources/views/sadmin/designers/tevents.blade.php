
<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class="flip-content">
		<tr>
			<th>#</th>
			<th>اسم الحدث</th>
			<th>وصف الحدث</th>
			<th>تاريخ الاضافة</th>
			<th>المشترك</th>
			<th>الحالة</th>
			<th>المجلة</th>
		</tr>
	</thead>
	<tbody>
		@foreach($events as $event)
		<tr>
			<td>{{$event->id}}</td>
			<td><a href="{{ route('sadmin.events.show',$event->id) }}">{{$event->title}}</a></td>
			<td>{{$event->description}}</td>
			<td>{{$event->created_at->format('Y-m-d')}}</td>
			<td>
				@if($event->user)
				<a href="{{ route('sadmin.users.edit',$event->user_id) }}">{{$event->user->name}}</a>
				@else
				<span class="text-danger">تم حذف المستعمل</span>
				@endif
			</td>
			<td>
				@if($event->designed)
				<span class="font-green">مصمم</span>
				@else
				@if($event->designer_id)
				<span class="label label-default">قيد التنفيد</span>
				@else
				<span class="font-purple">لم تتم المتابعة بعد</span>
				@endif
				@endif
			</td>
			<th>
				@if($event->catalog_id)
				<a href="{{ route('sadmin.catalogs.show',$event->catalog_id) }}">المجلة</a>
				@else
				<span class="font-purple">لا يوجد</span>
				@endif
			</th>
		</tr>
		@endforeach
	</tbody>
</table>



<div>
	{{$events->appends(["customers"=>$customers->currentPage(),"catalogs"=>$catalogs->currentPage()])->links()}}	
</div>