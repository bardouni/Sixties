@extends("sadmin/layouts/app")

@section("title","المصممين")

@push("active-designers")
active open
@endpush

@push("active-designers-1")
active
@endpush

@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption font-blue uab">
			<i class="fa fa-users font-blue"></i> كافة المصممين
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		@include("sadmin/layouts/msgs")

		<table-designers json="designers" ></table-designers>
	</div>
</div>

@endsection
