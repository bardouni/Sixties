<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class="flip-content">
		<tr>
			<th>#</th>
			<th>اسم العميل</th>
			<th>تاريخ الانضمام</th>
			<th>عدد الأحداث</th>
			<th>الأحداث المصممة</th>
			<th>الأحداث الغير مصممة</th>
		</tr>
	</thead>
	<tbody>
		@foreach($customers as $customer)
		<tr>
			<td>{{ $customer->id }}</td>
			<td><a href="{{ route('sadmin.users.edit',$customer->id) }}">{{ $customer->name }}</a></td>
			<td>{{ $customer->created_at->format('Y-m-d') }}</td>
			<td>{{ $customer->events()->count() }}</td>
			<td>{{ $customer->events()->designedEvents()->count() }}</td>
			<td>{{ $customer->events()->notDesigned()->count() }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
<div class="clearfix">
	<div class="row">

		<form method="post" action="{{ route('sadmin.designers.addcustomer',$designer->id) }}" class="col-xs-12 col-sm-6">
			{{csrf_field()}}
			{{method_field('POST')}}
			<div class="form-group">
				<label class="text-info" ><i class="fa fa-fw fa-user-plus"></i> متابعة مستخدم</label>
				<select name="customer" class="form-control">
					@foreach($usersNotFollowedByThisDesigner as $user)
					<option value="{{$user->id}}">{{$user->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<button class="btn btn-info btn-sm">إضافة </button>
			</div>
		</form>
		
		<form method="post" action="{{ route('sadmin.designers.trashcustomer',$designer->id) }}" class="col-xs-12 col-sm-6">
			{{csrf_field()}}
			{{method_field('POST')}}
			<div class="form-group">
				<label class="text-danger"><i class="fa fa-fw fa-trash"></i> إلغاء متابعة مستخدم</label>
				<select name="customer" class="form-control">
					@foreach($customers as $user)
					<option value="{{$user->id}}">{{$user->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<button class="btn btn-info btn-sm"><i class="fa fa-fw fa-trash"></i> إلغاء</button>
			</div>
		</form>

	</div>
</div>

<div>
{{$customers->appends(["events"=>$events->currentPage(),"catalogs"=>$catalogs->currentPage()])->links()}}
</div>