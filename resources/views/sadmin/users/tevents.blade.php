<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class="flip-content">
		<tr>
			<th>#</th>
			<th>اسم الحدث</th>
			<th>وصف الحدث</th>
			<th>تاريخ الاضافة</th>
			<th>المصمم</th>
			<th>الحالة</th>
			<th>المجلة</th>
		</tr>
	</thead>
	<tbody>
		@foreach($events as $event)
		<tr>
			<td><span class="label label-primary">{{$event->id}}</span></td>
			<td>{{$event->title}}</td>
			<td>{{$event->description}}</td>
			<td><span class="label label-default">{{$event->created_at->format('Y-m-d')}}</span></td>
			<td>
				@if($event->designer_id)
				@if($event->designer)
				<a href="{{ route('sadmin.users.edit',$event->designer_id) }}">{{$event->designer->name}}</a>
				@else
				<span class="text-danger">تم حذف المصمم</span>
				@endif
				@else
				<span class="label bg-purple">لم تتم المتابعة بعد</span>
				@endif
			</td>
			<td>
				@if($event->designed)
				<span class="label bg-green">مصمم</span>
				@else
				@if($event->designer_id)
				<span class="label label-default">قيد التنفيد</span>
				@else
				<span class="label bg-purple">لم تتم المتابعة بعد</span>
				@endif
				@endif
			</td>
			<th>
				@if($event->catalog_id)
				<a href="{{ route('sadmin.catalogs.show',$event->catalog_id) }}">المجلة</a>
				@else
				<span class="label bg-purple">لا يوجد</span>
				@endif
			</th>
		</tr>
		@endforeach
	</tbody>
</table>

<div>
	{{$events->links()}}
</div>
