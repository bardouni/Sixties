<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class="flip-content">
		<tr>
			<th>#</th>
			<th>مصمم المجلة</th>
			<th>تاريخ التصميم</th>
		</tr>
	</thead>
	<tbody>
		@foreach($catalogs as $catalog)
		<tr>
			<td><a href="{{ route('sadmin.catalogs.show',$catalog->id) }}" >{{ $catalog->id }}</a></td>
			<td>
				@if($catalog->designer)
				<a href="{{ route('sadmin.users.edit',$catalog->designer->id) }}">{{ $catalog->designer->name }}</a>
				@else
				<span class="text-danger">تم حذف المصمم</span>
				@endif
			</td>
			<td>
				<span class="label label-info">{{$catalog->created_at->format('Y-m-d')}}</span>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>