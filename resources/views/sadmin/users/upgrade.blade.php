
<div class="col-xs-12 col-sm-6">
	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-cog"></i>ترقية المستخدم
			</div>
		</div>
		<div class="portlet-body ">
			@if($user->cannot("is-admin"))
			<div class="form-group">
				<a href="{{ route('sadmin.users.newrole' ,['user'=>$user->id,'role'=>'admin']) }}" class="btn btn-block green btn-small">تحويل إلى مدير</a>
			</div>
			@endif
			@if($user->cannot("is-designer"))
			<div class="form-group">
				<a href="{{ route('sadmin.users.newrole' ,['user'=>$user->id,'role'=>'designer']) }}" class="btn btn-block blue btn-small">تحويل إلى مصمم</a>
			</div>
			@endif
			@if($user->cannot("is-simpleuser"))
			<div class="form-group">
				<a href="{{ route('sadmin.users.newrole' ,['user'=>$user->id,'role'=>'user']) }}" class="btn btn-block purple btn-small">تحويل إلى مستخدم عادي</a>
			</div>
			@endif
		</div>
	</div>
</div>
@if($user->can("is-simpleuser"))
<div class="col-xs-12 col-sm-6">
	<div class="portlet light bg-inverse">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-fw fa-location-arrow"></i>الاشتراك
			</div>
		</div>
		<div class="portlet-body ">
			<div class="clearfix">
				<label class="uab">الحالة :</label>
				@if($user->can("is-subscriber"))
					<span class="label bg-green">مشترك</span>
					@if($user->curentSubscribe)
					<br>
					<label class="uab">نوع الإشتراك :</label><span class="label label-info">{{$user->curentSubscribe->name}}</span>					
					@endif
				@else
				<span class="label bg-red">غير مشترك</span>
				@endif
			</div>
			<hr>
			<h6 class="text-center">إضافة اشتراك للمستخدم</h6>
			<form action="{{ route('admin.users.store') }}" method="post" >
				<input type="hidden" value="{{$user->id}}" name="user" />
				{{csrf_field()}}
				{{method_field('POST')}}
				<div class="form-group">
					<label>الإشتراك</label>
					<select name="subscribe" class="form-control" >
						@foreach($subscribes as $s)
						<option {{ $s->id == $user->subscribe_id ? 'selected' : '' }} value="{{ $s->id }}">{{$s->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<button class="btn btn-sm blue">تفعيل</button>
				</div>
			</form>

		</div>
	</div>
</div>
@endif
