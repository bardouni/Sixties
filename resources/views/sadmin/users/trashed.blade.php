@extends("sadmin/layouts/app")

@section("title","المستخدمين المحذوفين")

@push("active-users")
active open 
@endpush

@push("active-users-3")
active 
@endpush


@section("content")


<div class="portlet bg-inverse light">
	<div class="portlet-title">
		<div class="caption font-purple-plum uab">
			<i class="fa fa-users"></i> @yield('title')
		</div>
	</div>
	<div class="portlet-body flip-scroll">

		@include("sadmin/layouts/msgs")

		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class="flip-content">
				<tr>
					<th> رقم المشترك </th>
					<th> الاسم </th>
					<th>البريد الاكتروني</th>					
					<th>تاريخ الحذف</th>
					<th>عمليات</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td><span class="text-danger">{{ $user->deleted_at->format('Y-m-d') }}</span></td>
						<td>
							<form action="{{ route('sadmin.users.undo',$user->id) }}" method="post" >
								{{csrf_field()}}
								{{method_field("post")}}								
								<button type="submit" class="btn blue btn-sm " >
									<i class="fa fa-fw fa-undo"></i>
									استعادة المستعمل
								</button>								
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		@include("sadmin/layouts/pagination",["data"=>$users])
	</div>
</div>

@endsection