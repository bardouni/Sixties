@extends("sadmin/layouts/app")

@section("title",$user->name)

@push("active-users")
active open 
@endpush

@push("active-users-1")
active 
@endpush

@push("scripts")
<script src="/sadminassets/js/jquery.datetimepicker.full.min.js"></script>
<script>
	jQuery.datetimepicker.setLocale('ar');
	jQuery('.date').datetimepicker({
		timepicker:false,
		lang:'ar',
		format: 'Y-m-d'
	});
</script>
@endpush

@push("styles")
	<link rel="stylesheet" href="/sadminassets/css/jquery.datetimepicker.css" />
@endpush



@section("content")
<!-- Personel informations  -->
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user"></i> {{$user->name}}
		</div>

		<form action="{{ route('sadmin.users.destroy',$user->id) }}" method="post" class="actions" >

			<a href="{{ route('sadmin.users.edit',$user->id) }}" class="btn btn-info btn-sm">
				<i class="fa fa-undo"></i> إلغاء
			</a>

			{{csrf_field()}}
			{{method_field('delete')}}
			<button type="submit" class="btn btn-danger btn-sm">
				<i class="fa fa-trash"></i> حذف
			</button>
		</form>
		
	</div>
	<div class="portlet-body flip-scroll" >
		<form action="{{ route('sadmin.users.update',$user->id) }}" method="post" enctype="multipart/form-data" >
			{{csrf_field()}}
			{{method_field('put')}}

			<div class="clearfix">
				@include("sadmin/layouts/msgs")
			</div>
			<div class="clearfix" >
				<!-- name -->

				<div class="col-xs-12 col-md-6 pull-right text-center">
					<img src="{{ $user->completAvatarUrl }}" class="avatar-user-in-sadminpage" >
				</div>
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الاسم الثلاثي</label>
						<input type="text" class="form-control" value="{{ old('name') ?: $user->name }}" name="name" >
					</div>
				</div>
				<!-- birth day -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>تاريخ الميلاد</label>
						<input type="text" class="date form-control" value="{{ old('birthday') ?: $user->birthday }}" name="birthday" >                    
					</div>
				</div>
				<!-- email -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>البريد الإلكتروني</label>
						<input type="email" class="form-control" value="{{ old('email') ?: $user->email }}" name="email" >                    
					</div>
				</div>
				<!-- phone -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>رقم الهاتف</label>
						<input type="text" class="form-control" value="{{ old('phone') ?: $user->phone }}" name="phone" >                    
					</div>
				</div>
				<!-- city -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>المدينة</label>
						<input type="text" class="form-control" value="{{ old('city') ?: $user->city }}" name="city" >                    
					</div>
				</div>
				<!-- postal -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>صندوق البريد</label>
						<input type="text" class="form-control" value="{{ old('postal') ?: $user->postal }}" name="postal" >                    
					</div>
				</div>
				<!-- location -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>المنطقة</label>
						<input type="text" class="form-control" value="{{ old('location') ?: $user->location }}" name="location" >                    
					</div>
				</div>
				<!-- Neighborhood -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الحي</label>
						<input type="text" class="form-control" value="{{ old('ngbh') ?: $user->ngbh }}" name="ngbh" >
					</div>
				</div>
				<!-- street -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الشارع</label>
						<input type="text" class="form-control" value="{{ old('street') ?: $user->street }}" name="street" >                    
					</div>
				</div>
				@if($user->can("is-simpleuser"))
				<!-- designer -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>المصمم المتابع</label>
						<p>
							@if($user->follower)
							<a href="{{ route('sadmin.users.edit',$user->follower->id) }}">{{$user->follower->name}}</a>
							@else
							<span class="text-danger">لا يتابعه اي مصمم</span>
							@endif
						</p>
					</div>
				</div>
				<!-- Upadte designer -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>تعيين مصمم للمستخدم</label>
						<select name="follower_id" class="form-control" >
							<option selected value="">اختر مصممًا</option>
							@foreach($designers as $designer)
							<option {{ $user->follower_id == $designer->id ? 'selected' : '' }} value="{{ $designer->id }}">{{$designer->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				@endif
				<!-- img -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الصورة الشخصية</label>
						<input type="file" name="avatar" >
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-pencil"></i> حفظ
						</button>
					</div>
				</div>

			</div>

			<change-password />
		</form>
	</div>
</div>
<!-- tabs -->
<div class="portlet light bg-inverse">
	<div class="portlet-title pull-left tabbable-line">	
		<ul class="nav nav-tabs">
			<li class="active" >
				<a href="#msgs" data-toggle="tab">
					<i class="fa fa-fw fa-envelope"></i> الرسائل
				</a>
			</li>
			<li>
				<a href="#events" data-toggle="tab">
					<i class="fa fa-fw fa-cubes"></i> الأحداث
				</a>
			</li>			
			<li>
				<a href="#catalogs" data-toggle="tab">
					<i class="fa fa-fw fa-book"></i>المجلات
				</a>
			</li>
		</ul>
	</div>
	<div class="portlet-body">
		<div class="tab-content">
			<div id="msgs" class="tab-pane active" >
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>#</th>
							<th>عنوان الرسالة</th>
							<th>اخر رد</th>
							<th>تاريخ الانشاء</th>
							<th>تاريخ اخر رد</th>
							<th>الوجهة</th>
						</tr>
					</thead>
					<tbody>
						@foreach($groups as $group)
						<tr>
							<td><span class="label label-primary">{{$group->id}}</span></td>
							<td><a href="{{ route('sadmin.msgs.show',$group->id) }}">{{$group->title}}</a></td>
							<td>
								{{$group->lastanswer->content}}
							</td>
							<td>{{$group->created_at->format('Y-m-d')}}</td>
							<td>{{$group->lastanswer->created_at->format('Y-m-d')}}</td>
							<td>
								@if($group->withAdmin)
								<span class="font-blue">الادارة</span>
								@else
								<span class="font-yellow">المصمم</span>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>				
			</div>
			<div id="events" class="tab-pane" >
				<table-events table="events" json="events" :hide="{user_id:{!! $user->id !!}}" ></table-events>
			</div>
			<div id="catalogs" class="tab-pane" >
				<table-catalogs json="catalogs" :hide="{user_id:{!! $user->id !!}}" table="catalogs" ></table-catalogs>
			</div>
		</div>
	</div>
</div>

<div class="clearfix">
	@include("sadmin/users/upgrade")
</div>
@endsection