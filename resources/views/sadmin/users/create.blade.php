@extends("sadmin/layouts/app")

@section("title","إنشاء مستخدم")

@push("active-users")
active open 
@endpush

@push("active-users-2")
active 
@endpush

@push("scripts")
<script src="/sadminassets/js/jquery.datetimepicker.full.min.js"></script>
<script>
	jQuery.datetimepicker.setLocale('ar');
	jQuery('.date').datetimepicker({
		timepicker:false,
		lang:'ar',
		format: 'Y-m-d'
	});
</script>
@endpush

@push("styles")
<link rel="stylesheet" href="/sadminassets/css/jquery.datetimepicker.css" />
@endpush

@section("content")

<form action="{{ route('sadmin.users.store') }}" method="post" enctype="multipart/form-data" >
	{{csrf_field()}}
	{{method_field('post')}}
	<div class="portlet light bordered ">
		<div class="portlet-title">
			<div class="caption uab font-green">
				<i class="fa fa-user-plus"></i> معلومات المستخدم
			</div>

			<div class="actions">
				<a href="{{ route('sadmin.users.index') }}" class="btn btn-default btn-sm">
					<i class="fa fa-undo"></i> عودة الى قائمة المستخدمين
				</a>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-pencil"></i> حفظ
				</button>
			</div>
		</div>
		<div class="portlet-body flip-scroll" >
			<div class="clearfix">
				@include("sadmin/layouts/msgs")
			</div>
			<div class="clearfix" >
				<!-- name -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الاسم الثلاثي</label>
						<input type="text" class="form-control" value="{{ old('name') }}" name="name" >
					</div>
				</div>
				<!-- birth day -->
				<div class="col-xs-12 col-md-6 pull-left " style="position: relative;" >
					<div class="form-group">
						<label>تاريخ الميلاد</label>
						<input type="text" class="date form-control" value="{{ old('birthday') }}" name="birthday" >
					</div>
				</div>
				<!-- email -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>البريد الإلكتروني</label>
						<input type="email" class="form-control" value="{{ old('email') }}" name="email" >                    
					</div>
				</div>
				<!-- phone -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>رقم الهاتف</label>
						<input type="text" class="form-control" value="{{ old('phone') }}" name="phone" >                    
					</div>
				</div>
				<!-- city -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>المدينة</label>
						<input type="text" class="form-control" value="{{ old('city') }}" name="city" >                    
					</div>
				</div>
				<!-- postal -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>صندوق البريد</label>
						<input type="text" class="form-control" value="{{ old('postal') }}" name="postal" >                    
					</div>
				</div>
				<!-- location -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>المنطقة</label>
						<input type="text" class="form-control" value="{{ old('location') }}" name="location" >                    
					</div>
				</div>
				<!-- Neighborhood -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الحي</label>
						<input type="text" class="form-control" value="{{ old('ngbh') }}" name="ngbh" >
					</div>
				</div>
				<!-- street -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الشارع</label>
						<input type="text" class="form-control" value="{{ old('street') }}" name="street" >                    
					</div>
				</div>
				<!-- avatar -->
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>الصورة الشخصية</label>
						<input type="file" name="avatar" >
					</div>
				</div>
			</div>
			
			<hr>

			<div class="clearfix">
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>كلمة المرور</label>
						<input type="password" class="form-control" name="password" >
					</div>
				</div>
				
				<div class="col-xs-12 col-md-6 pull-left">
					<div class="form-group">
						<label>تاكيد كلمة المرور</label>
						<input type="password" class="form-control" name="password_confirmation" >
					</div>
				</div>
			</div>

		</div>
	</div>
</form>

@endsection
