@extends("sadmin/layouts/app")

@section("title","عرض المستخدمين")

@push("active-users")
active open
@endpush

@push("active-users-1")
active
@endpush

@section("content")

<div class="portlet bordered light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i> كافة المستخدمين
		</div>
	</div>
	<div class="portlet-body flip-scroll">

		@include("sadmin/layouts/msgs")

		<table-users json="users" table="users" ></table-users>

	</div>
</div>

@endsection