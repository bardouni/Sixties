@extends("sadmin/layouts/app")

@section("title","لوحة التحكم")

@push("active-home")
active open 
@endpush

@push("styles")

	<link rel="stylesheet" href="/sadminassets/css/tasks.css" />
	<link rel="stylesheet" href="/sadminassets/css/plugins.css" />

@endpush

@push("scripts")

	<script src="/sadminassets/js/jquery.easypiechart.min.js"></script>
	<script src="/sadminassets/js/barcharts.js"></script>

	<script type="text/javascript">

		$('.easy-pie-chart .number').each(function() {    
		    $(this).easyPieChart({
		    	size:83 ,
		    	barColor: this.dataset.color
		    });
		});

		var myBarChart = new Chart(document.getElementById('barsnewusers'), {
		    type: 'bar',
		    data: {
			    labels: {!! json_encode($dataCharUsers['labels']) !!} ,
			    datasets: [
			        {
			            backgroundColor:"#D6ECFB",
			            borderWidth: 0,
			            data: {!! json_encode($dataCharUsers['data']) !!} ,
			        }
			    ]
			} ,

		    options: {
		    	legend :{
		    		display:false
		    	} ,		    	
		    }
		});

	</script>

@endpush

@section("content")

@include("sadmin/layouts/component-welcomepage-labels")

<div class="row">
	<div class="col-md-6 col-sm-6">
		
		@include("sadmin/layouts/component-welcomepage-recentevents")
		
		@include("sadmin/layouts/component-welcomepage-recentusers")

		
	</div>
	<div class="col-md-6 col-sm-6">
		
		@include("sadmin/layouts/component-welcomepage-asksubscribe")

		@include("sadmin/layouts/component-welcomepage-msgs")

	</div>
</div>


@endsection