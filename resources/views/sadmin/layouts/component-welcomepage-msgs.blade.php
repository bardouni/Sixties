<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-envelope-o "></i> <!-- font-blue-steel -->
			<span class="caption-subject  bold">أحدث الرسائل</span> <!-- font-blue-steel -->
		</div>				
	</div>
	<div class="portlet-body">

		<div>
			<ul class="feeds">
				
				@foreach($recentMsgs as $msg)
				<li>
					<div class="col1">
						<div class="cont">
							<div class="cont-col2">
								<div class="desc text-left" dir="rtl" >
									<a href="{{ route('sadmin.msgs.show',$msg->group_id) }}">										
										{{str_limit($msg->content,50)}}
									</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				@endforeach

			</ul>
		</div>				

	</div>
</div>