<div class="top-menu">
	<ul class="nav navbar-nav pull-right">
		<!-- BEGIN NOTIFICATION DROPDOWN -->
		
		<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="fa fa-fw fa-bell-o"></i>
				<span class="badge badge-default">
					{{ $globalUnreadNotifications->count() }}
				</span>
			</a>
			<ul class="dropdown-menu">
				<li class="external uar">
					<h3>
						<span class="uab bold">{{ $globalUnreadNotifications->count() }}إشعار</span> جديد
					</h3>
					<a href="{{ route('sadmin..notifications') }}">الكل</a>					
				</li>
				<li>
					<ul class="dropdown-menu-list" >
						@foreach($globalUnreadNotifications as $notification)
						@include('sadmin/layouts/notifications/' . snake_case(class_basename($notification->type)))
						@endforeach
					</ul>							
				</li>
			</ul>
		</li>
		<!-- END NOTIFICATION DROPDOWN -->

		<li class="dropdown dropdown-quick-sidebar-toggler">
			<div class="dropdown-toggle">
				<form class="custom-logout-form" action="{{ route('logout') }}" onclick="this.submit();" method="post" >
					{{csrf_field()}}
					{{method_field('POST')}}
					<i class="fa fa-fw fa-power-off"></i>					
				</form>
			</div>
		</li>
		<!-- END QUICK SIDEBAR TOGGLER -->
	</ul>
</div>