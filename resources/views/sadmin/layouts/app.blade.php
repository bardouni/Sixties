<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
	<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
	<!--[if !IE]><!-->
	<html lang="ar" dir="rtl">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="utf-8"/>
		<title>@yield('title')</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<!-- <meta content="" name="description"/> -->
		<meta content="Bardouni Naoufal , https://facebook.com/bardouni.naoufal" name="author"/>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<!-- <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css"/> -->
		<link href="/ff/fonts.css" rel="stylesheet" type="text/css"/>
		<link href="/sadminassets/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
		<!-- <link href="/sadminassets/css/uniform.default.min.css" rel="stylesheet" type="text/css"/> -->
		<!-- <link href="/sadminassets/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css"/> -->
		<!-- END GLOBAL MANDATORY STYLES -->

		<!-- BEGIN THEME STYLES -->
		<link href="/sadminassets/css/components-rounded-rtl.css" id="style_components" rel="stylesheet" type="text/css"/>
		<!-- <link href="/sadminassets/css/plugins-rtl.css" rel="stylesheet" type="text/css"/> -->
		<link href="/sadminassets/css/layout-rtl.css" rel="stylesheet" type="text/css"/>
		<link id="style_color" href="/sadminassets/css/darkblue-rtl.css" rel="stylesheet" type="text/css"/>
		<link href="/sadminassets/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
		@stack("styles")
		<!-- END THEME STYLES -->

		<link rel="shortcut icon" type="image/png" href="/systemups/logo.png" />
		<script>
		    window.Laravel = <?php echo json_encode([
		        'csrfToken' => csrf_token(),		        
		    ]); ?>
		</script>
	</head>
	<!-- END HEAD -->
	<!-- BEGIN BODY -->
	<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
	<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
	<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
	<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
	<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
	<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
	<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
	<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
	<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
	<body class="page-header-fixed page-quick-sidebar-over-content">
	<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner">
				<!-- BEGIN LOGO -->
				<div class="page-logo">
					<a class="name-wbesite" href="{{ route('sadmin..index') }}">
						ركن الستينات
					</a>
					<div class="menu-toggler sidebar-toggler hide">
						<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
					</div>
				</div>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
				</a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				@include("sadmin/layouts/topmenu")
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END HEADER INNER -->
		</div>

		<!-- END HEADER -->
		<div class="clearfix">
		</div>
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper">
				<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
				<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
				<div class="page-sidebar navbar-collapse collapse">
					<!-- BEGIN SIDEBAR MENU -->
					<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
					<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
					<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
					<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
					<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
					<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
					@include("sadmin/layouts/menu")
					<!-- END SIDEBAR MENU -->
				</div>
			</div>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Modal title</h4>
								</div>
								<div class="modal-body">
									 Widget settings form goes here
								</div>
								<div class="modal-footer">
									<button type="button" class="btn blue">Save changes</button>
									<button type="button" class="btn default" data-dismiss="modal">Close</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					

					<!-- BEGIN PAGE CONTENT-->
					<div class="row" id="app" >
						<div class="col-md-12">
							@yield("content")
						</div>
					</div>
					<!-- END PAGE CONTENT-->

				</div>
			</div>
			<!-- END CONTENT -->
			
			</div>
			<!-- END CONTAINER -->
			
			<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
			<!-- BEGIN CORE PLUGINS -->
			<!--[if lt IE 9]>
			<script src="./sadminassets/global/plugins/respond.min.js"></script>
			<script src="./sadminassets/global/plugins/excanvas.min.js"></script> 
			<![endif]-->
			<script src="/sadminassets/js/jquery.min.js" type="text/javascript"></script>
			<!-- <script src="/sadminassets/js/jquery-migrate.min.js" type="text/javascript"></script> -->
			<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
			<!-- <script src="/sadminassets/js/jquery-ui.min.js" type="text/javascript"></script> -->
			<script src="/sadminassets/js/bootstrap.min.js" type="text/javascript"></script>
			<!-- <script src="/sadminassets/js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> -->

			<!-- <script src="/sadminassets/js/jquery.slimscroll.min.js" type="text/javascript"></script> -->
			<!-- <script src="/sadminassets/js/jquery.blockui.min.js" type="text/javascript"></script> -->
			<!-- <script src="/sadminassets/js/jquery.cokie.min.js" type="text/javascript"></script> -->
			<!-- <script src="/sadminassets/js/jquery.uniform.min.js" type="text/javascript"></script> -->
			<!-- <script src="/sadminassets/js/bootstrap-switch.min.js" type="text/javascript"></script> -->
			<!-- END CORE PLUGINS -->
			<script src="/sadminassets/js/metronic.js" type="text/javascript"></script>
			<script src="/sadminassets/js/layout.js" type="text/javascript"></script>
			<!-- <script src="/sadminassets/js/quick-sidebar.js" type="text/javascript"></script> -->
			<!-- <script src="/sadminassets/js/demo.js" type="text/javascript"></script> -->
		   	<script src="/js/sadmin.js"></script>
		   	@stack('scripts')
			<script>
			    jQuery(document).ready(function() {    
			        Metronic.init(); // init metronic core components
					Layout.init(); // init current layout
					// QuickSidebar.init(); // init quick sidebar
					// Demo.init(); // init demo features
			    });
		   	</script>
		<!-- END JAVASCRIPTS -->
	</body>
<!-- END BODY -->
</html>