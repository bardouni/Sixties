
<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-users"></i><!-- font-green-sharp -->
			<span class="caption-subject bold">أحدث المستخدمين</span><!-- font-green-sharp -->
		</div>				
	</div>
	<div class="portlet-body">
		
		<div >
			<ul class="feeds">
				@foreach($recentUsers as $user)
				<li>
					<div class="col1">
						<div class="cont">
							<div class="cont-col2">
								<div class="desc text-left" dir="rtl" >
									<a href="{{ route('sadmin.users.edit',$user->id) }}">
										{{ $user->name }}
										@if($user->cannot("is-subscriber"))
										<span class="label label-sm label-warning ">غير مفعل</span>
										@else
										<span class="label label-sm label-success ">مفعل</span>
										@endif
									</a>
								</div>
							</div>
						</div>
					</div>							
				</li>
				@endforeach
			</ul>
		</div>				
		
	</div>
</div>