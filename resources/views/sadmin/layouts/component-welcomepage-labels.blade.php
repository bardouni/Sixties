<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat blue-madison">
			<div class="visual">
				<i class="fa fa-users"></i>
			</div>
			<div class="details">
				<div class="number">
					{{ $usersLength }}
				</div>
				<div class="desc">
					مجموع المستخدمين
				</div>
			</div>
			<a class="more" href="{{ route('sadmin.users.index') }}">
				المستخدمين
			</a>
		</div>
	</div>

	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat green-haze">
			<div class="visual">
				<i class="fa fa-calendar-o"></i>
			</div>
			<div class="details">
				<div class="number">
					{{$activatedThisMonth->count()}}
				</div>
				<div class="desc font-size-14" >
					المستخدمين المفعلون خلال هذا الشهر
				</div>
			</div>
			<a class="more" href="{{ route('sadmin.users.index') }}">
				المستخدم
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red-intense">
			<div class="visual">
				<i class="fa fa-clock-o"></i>
			</div>
			<div class="details">
				<div class="number">
					{{ $usersNeedActivate->count() }}
				</div>
				<div class="desc">
					مستخدمين جدد
				</div>
			</div>
			<a class="more" href="{{ route('sadmin.subscriptions.index') }}">
				الاشتراكات
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat purple-plum">
			<div class="visual">
				<i class="fa fa-cubes"></i>
			</div>
			<div class="details">
				<div class="number font-size-16">
					مجموع الأحداث المصممة :{{ $designedEvents }}
				</div>
				<div class="desc">
					<small>مجموع الأحداث :{{ $events }}</small>
				</div>
			</div>
			<a class="more" href="{{ route('sadmin.events.index') }}">
				الأحداث
			</a>
		</div>
	</div>
</div>