<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-cube "></i><!-- font-purple-intense -->
			<span class="caption-subject  bold uppercase">الأحداث</span><!-- font-purple-intense -->
		</div>				
	</div>
	<div class="portlet-body">
		<div class="row">
			@if($events)
			<div class="col-md-4">
				<div class="easy-pie-chart">
					<div data-color="#F3565D" class="number transactions" data-percent="{!! number_format(($charEventsDesigned * 100) / $events , 1)  !!}">
						<span>+{{ number_format(($charEventsDesigned * 100) / $events , 1) }} </span>%
					</div>
					<a class="title" href="javascript:;">
						الأحداث المصممة
					</a>
				</div>
			</div>
			<div class="margin-bottom-10 visible-sm">
			</div>
			<div class="col-md-4">
				<div class="easy-pie-chart">
					<div data-color="#1BBC9B" class="number visits" data-percent="{!!number_format(($charEventsNotDesigned * 100) / $events , 1) !!}">
						<span>+{!!number_format(($charEventsNotDesigned * 100) / $events , 1) !!}</span>%
					</div>
					<a class="title" href="javascript:;">
						غير المصممة
					</a>
				</div>
			</div>
			<div class="margin-bottom-10 visible-sm">
			</div>
			<div class="col-md-4">
				<div class="easy-pie-chart">
					<div data-color="#F8CB00" class="number bounce" data-percent="{!!number_format(($charEventsNotFollowed * 100) / $events , 1)!!}">
						<span> -{!!number_format(($charEventsNotFollowed * 100) / $events , 1)!!} </span>%
					</div>
					<a class="title" href="javascript:;">
					غير متابعة
					</a>
				</div>
			</div>
			@else
			<div class="text-center col-xs-12 text-danger">لا يوجد اية أحداث</div>
			@endif
		</div>
	</div>
</div>

<div class="portlet light bg-inverse tasks-widget ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-cubes "></i><!-- font-blue-steel -->
			<span class="caption-subject  bold uppercase">أحدث الأحداث المضافة</span><!-- font-blue-steel -->
		</div>				
	</div>
	<div class="portlet-body">	
		<div class="task-content">
			<ul class="task-list">
				@foreach($lastEvents as $event)
				<li>
					<a href="{{ route('sadmin.events.show',$event->id) }}" class="task-title text-left" dir="rtl" >
						<span class="task-title-sp">
							{{ str_limit($event->title,50) }}
							@if($event->designed)
							<span class="label label-sm label-success">مصممة</span>
							@elseif($event->designer)
							<span class="label label-sm label-info">قيد التنفيذ</span>
							@else
							<span class="label label-sm label-danger">غير متابعة بعد</span>
							@endif
						</span>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>