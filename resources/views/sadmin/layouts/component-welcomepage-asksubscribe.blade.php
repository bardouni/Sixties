<div class="portlet light bg-inverse tasks-widget ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-users "></i> <!-- font-purple-intense -->
			<span class="caption-subject  bold">المشتركين خلال هذا الشهر</span> <!-- font-purple-intense -->
		</div>				
	</div>
	<div class="text-center portlet-body">	
		<canvas id="barsnewusers"></canvas>
	</div>
</div>


<div class="portlet light bg-inverse tasks-widget ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-fw fa-archive"></i>
			<span class="caption-subject bold uppercase">أحدث طلبات الإشتراك</span> <!-- font-red-sunglo -->
		</div>				
	</div>
	<div class="portlet-body">	
		<div class="task-content">
			<ul class="task-list">
				@foreach($lastSubscribes as $subscribe)
				<li>
					<a href="{{ route('sadmin.subscriptions.edit',$subscribe->id) }}" class="task-title text-left" dir="rtl" >
						<span class="task-title-sp">									
							@if($subscribe->user)									
								{{$subscribe->user->name}}
							@else
								<span class="text-danger">تم حذف المشترك .</span>
							@endif

							@if($subscribe->activated)
								<span class="label label-sm label-info">مفعل</span>
							@else
								<span class="label label-sm label-warning">غير مفعل</span>
							@endif
						</span>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>