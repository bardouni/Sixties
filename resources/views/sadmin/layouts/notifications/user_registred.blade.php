<li>
	<a href="{{ route('sadmin.users.edit',$notification->data['registredUser']) }}" >
	<span class="time uab">{{ $notification->created_at->diffForHumans() }}</span>
	<span class="details uar">
		<span class="label label-sm label-icon label-info">
			<i class="fa fa-user-plus"></i>
		</span>
		{{ $notification->data['msg'] }}
	</span>
	</a>
</li>