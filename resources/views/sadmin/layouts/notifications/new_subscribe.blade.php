<li>
	<a href="{{ route('sadmin.subscriptions.edit',$notification->data['subId']) }}">
	<span class="time uab">{{ $notification->created_at->diffForHumans() }}</span>
	<span class="details uar">
		<span class="label label-sm label-icon label-success">
			<i class="fa fa-fw fa-plus"></i>
		</span>
		{{$notification->data['msg']}}
	</span>
	</a>
</li>