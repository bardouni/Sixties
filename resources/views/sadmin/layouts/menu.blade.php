<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
	<li class="start @stack('active-home') ">
		<a href="{{ route('sadmin..index') }}">
			<i class="fa fa-fw fa-home"></i>
			<span class="title">لوحة التحكم</span>
		</a>
	</li>
	<li class="@stack('active-users') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-users"></i>
			<span class="title">المستخدمين</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-users-1')">
				<a href="{{ route('sadmin.users.index') }}"><i class="fa fa-fw fa-users"></i> عرض المستخدمين</a>
			</li>
			<li class="@stack('active-users-2')">
				<a href="{{ route('sadmin.users.create') }}"><i class="fa fa-fw fa-user-plus"></i> إضافة مستخدم</a>
			</li>
			<li class="@stack('active-users-3')">
				<a href="{{ route('sadmin.users.trashed') }}"><i class="fa fa-fw fa-trash"></i> المستخدمين المحذوفين</a>
			</li>
			<li class="@stack('active-users-4')">
				<a href="{{ route('sadmin.subscriptions.index') }}"><i class="fa fa-fw fa-archive"></i> عرض اشتراكات المستخدمين </a>
			</li>
		</ul>
	</li>

	<li class="@stack('active-designers') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-user-o"></i>
			<span class="title">المصممين</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-designers-1')" >
				<a href="{{ route('sadmin.designers.index') }}"><i class="fa fa-fw fa-user-o"></i> عرض المصممين </a>
			</li>
		</ul>
	</li>
	@if(auth()->user()->can('is-absolute-superadmin'))
	<li class="@stack('active-admins') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-user-circle-o"></i>
			<span class="title">الإداريين</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-admins-1')" >
				<a href="{{ route('sadmin.admins.index') }}"><i class="fa fa-fw fa-user-circle-o"></i> عرض المدراء </a>
			</li>
		</ul>
	</li>
	@endif
	<li class="@stack('active-msgs') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-envelope"></i>
			<span class="title">الرسائل</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-msgs-1')" >
				<a href="{{ route('sadmin.msgs.index') }}"><i class="fa fa-fw fa-envelope-open"></i> عرض الرسائل </a>
			</li>
			<li class="@stack('active-msgs-2')" >
				<a href="{{ route('sadmin.msgs.create') }}"><i class="fa fa-fw fa-pencil"></i> رسالة جديدة </a>
			</li>
			<li class="@stack('active-msgs-3')" >
				<a href="{{ route('sadmin.externalmsgs.index') }}"><i class="fa fa-fw fa-google-plus"></i> رسائل خارجية </a>
			</li>
		</ul>
	</li>

	<li class="@stack('active-catalogs') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-book"></i>
			<span class="title">المجلات</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-catalogs-1')" >
				<a href="{{ route('sadmin.catalogs.index') }}"><i class="fa fa-fw fa-envelope-open"></i> عرض المجلات </a>
			</li>
		</ul>
	</li>


	<li class="@stack('active-events') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-calendar"></i>
			<span class="title">الأحداث</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-events-1')" >
				<a href="{{ route('sadmin.events.index') }}"><i class="fa fa-fw fa-calendar"></i> عرض الأحداث </a>
			</li>
		</ul>
	</li>


	<li class="@stack('active-subscribescatalog') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-star"></i>
			<span class="title">الباقات</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-subscribescatalog-1')" >
				<a href="{{ route('sadmin.subscribescatalog.index') }}"><i class="fa fa-fw fa-star"></i> عرض الباقات </a>
			</li>
			<li class="@stack('active-subscribescatalog-2')" >
				<a href="{{ route('sadmin.subscribescatalog.create') }}"><i class="fa fa-fw fa-pencil-square-o"></i> إضافة باقة </a>
			</li>
		</ul>
	</li>

	<li class="@stack('active-website') ">
		<a href="javascript:;">
			<i class="fa fa-fw fa-cog"></i>
			<span class="title">بيانات الصفحة الرئيسية</span>
			<span class="selected"></span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="@stack('active-website-1')" >
				<a href="{{ route('sadmin..us') }}"><i class="fa fa-fw fa-id-card-o"></i> صفحة من نحن </a>
			</li>
			<li class="@stack('active-website-2')" >
				<a href="{{ route('sadmin..goals') }}"><i class="fa fa-fw fa-dot-circle-o"></i> أهدافنا </a>
			</li>
			<li class="@stack('active-website-3')" >
				<a href="{{ route('sadmin..vision') }}"><i class="fa fa-fw fa-crosshairs"></i> رؤيتنا </a>
			</li>
			<li class="@stack('active-website-4')" >
				<a href="{{ route('sadmin..contactpage') }}"><i class="fa fa-fw fa-envelope"></i> تواصل معنا </a>
			</li>
			<li class="@stack('active-website-5')" >
				<a href="{{ route('sadmin..terms') }}"><i class="fa fa-fw fa-copyright"></i> شروط الإستخدام </a>
			</li>			
			<li class="@stack('active-website-6')" >
				<a href="{{ route('sadmin..social') }}"><i class="fa fa-fw fa-facebook"></i> الشبكات الإجتماعية </a>
			</li>
			<li class="@stack('active-website-7')" >
				<a href="{{ route('sadmin..works') }}"><i class="fa fa-fw fa-facebook"></i> صور الصفحة الرئيسية </a>
			</li>
		</ul>
	</li>



</ul>
