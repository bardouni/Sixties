@if (session('success-message'))
<div class="note note-success">
	<p>
		{!! session('success-message') !!}
	</p>
</div>
@endif

@foreach($errors->all() as $error)
<div class="note note-danger">
	<p>
		{{ $error }}
	</p>
</div>
@endforeach