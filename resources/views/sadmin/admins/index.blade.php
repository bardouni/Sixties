@extends("sadmin/layouts/app")

@section("title","المدراء")

@push('active-admins-1')
active 
@endpush

@push('active-admins')
active open
@endpush

@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption font-purple uab">
			<i class="fa font-purple fa-user-circle-o"></i> كافة المدراء
		</div>
	</div>
	<div class="portlet-body flip-scroll">
	
		<div>
			@include("sadmin/layouts/msgs")
		</div>
		
		<table-designers json="admins" ></table-designers>
	</div>
</div>

@endsection