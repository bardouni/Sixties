@extends("sadmin/layouts/app")

@section("title","المجلات")

@push('active-catalogs')
active open
@endpush

@push('active-catalogs-1')
active 
@endpush

@section("content")

<div class="portlet light bg-inverse ">
	<div class="portlet-title">
		<div class="caption font-blue uab">
			<i class="fa fa-book font-blue "></i>كافة @yield('title')
		</div>		
	</div>	
	<div class="portlet-body flip-scroll">		
		<table-catalogs json="catalogs" table="catalogs" ></table-catalogs>
	</div>
</div>


@endsection