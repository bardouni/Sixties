@extends("sadmin/layouts/app")

@section("title","المجلات")

@push('active-catalogs')
active open
@endpush

@push('active-catalogs-1')
active 
@endpush

@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title tabbable-line">
		<div class="caption uab font-blue">
			<i class="fa fa-book font-blue"></i>مجلة رقم : {{ $catalog->id }}
		</div>
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#catalog" data-toggle="tab" aria-expanded="true">المجلة</a>
			</li>
			<li>
				<a href="#notes" data-toggle="tab" aria-expanded="false">ملاحظات حول المجلة</a>
			</li>
			<li>
				<a href="#events" data-toggle="tab" aria-expanded="false">الأحداث المرتبطة بالمجلة</a>
			</li>
		</ul>
	</div>	
	<div class="portlet-body">
		<div class="tab-content">
			<div id="catalog" class="tab-pane active" >
				<div class="clearfix">
					<div class="col-xs-12 col-sm-6 col-sm-offset-3 ">
						<table class="table table-bordered table-striped table-condensed flip-content">
							<tbody>
								<tr>
									<td>رقم المجلة</td>
									<td>{{ $catalog->id }}</td>
								</tr>
								<tr>
									<td>المصمم</td>
									<td>
										@if($catalog->designer)
										<a class="blue-madison" href="{{ route('sadmin.designers.show',$catalog->designer_id) }}">{{ $catalog->designer->name }}</a>
										@else
										<span class="text-danger">تم حذف المصمم</span>
										@endif
									</td>
								</tr>
								<tr>
									<td>العميل</td>
									<td>
										@if($catalog->user)
										<a class="purple-plum" href="{{ route('sadmin.users.show',$catalog->user->id) }}">{{ $catalog->user->name }}</a>								
										@else
										<span class="text-danger">تم حذف العميل</span>
										@endif
									</td>
								</tr>
								<tr>
									<td>تاريخ الإضافة</td>
									<td>{{ $catalog->created_at->format('Y-m-d') }}</td>
								</tr>
								<tr>
									<td>تاريخ آخر تعديل</td>
									<td>{{ $catalog->updated_at->format("Y-m-d") }}</td>
								</tr>			
							</tbody>
						</table>
						<embed src="/catalogs/{{ $catalog->src }}" width="100%" height="700" type='application/pdf' />
					</div>
				</div>
			</div>

			<div id="notes" class="tab-pane" >
				<div class="clearfix">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 ">					
						<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
								<tr>
									<td>
										من طرف
									</td>
									<td>
										ملاحظة
									</td>
									<td>
										تاريخ الاضافة
									</td>
								</tr>
							</thead>
							<tbody>
								@foreach($notes as $note)
								<tr>
									<td>
										@if($catalog->designer_id == $note->user_id)
										المصمم
										@else
										العميل
										@endif
									</td>
									<td>
										{{ $note->content }}
									</td>
									<td>
										<span class="label label-primary">{{ $note->created_at->format("Y-m-d")}}</span>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div id="events" class="tab-pane" >
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>#</th>
							<th>اسم الحدث</th>
							<th>وصف الحدث</th>
							<th>تاريخ الاضافة</th>
							<th>المصمم</th>
							<th>الحالة</th>
							<th>المجلة</th>
						</tr>
					</thead>
					<tbody>
						@foreach($events as $event)
						<tr>
							<td><span class="label label-primary">{{$event->id}}</span></td>
							<td>{{$event->title}}</td>
							<td>{{$event->description}}</td>
							<td><span class="label label-default">{{$event->created_at->format('Y-m-d')}}</span></td>
							<td>				
								@if($event->designer)
								<a href="{{ route('sadmin.users.edit',$event->designer_id) }}">{{$event->designer->name}}</a>
								@else
								<span class="text-danger">تم حذف المصمم</span>
								@endif
							</td>
							<td>
								@if($event->designed)
								<span class="label bg-green">مصمم</span>
								@else
								@if($event->designer_id)
								<span class="label label-default">قيد التنفيد</span>
								@else
								<span class="label bg-purple">لم تتم المتابعة بعد</span>
								@endif
								@endif
							</td>
							<th>
								@if($event->catalog_id)
								<a href="{{ route('sadmin.catalogs.show',$event->catalog_id) }}">المجلة</a>
								@else
								<span class="label bg-purple">لا يوجد</span>
								@endif
							</th>
						</tr>
						@endforeach
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>

@endsection