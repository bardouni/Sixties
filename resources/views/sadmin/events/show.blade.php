@extends("sadmin/layouts/app")

@section("title",$event->title)

@push('active-events')
active open
@endpush

@push('active-events-1')
active 
@endpush

@section("content")

<div class="clearfix">
	<div class="col-xs-12 col-sm-8 col-offset-sm-2">

		<div class="portlet light bg-inverse">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cubes"></i> حدث : {{ str_limit($event->title,30) }}
				</div>
			</div>
			<div class="portlet-body flip-scroll">
				
				<table class="table table-bordered table-striped table-condensed flip-content">
					<tbody>				
						<tr><td>رقم الحدث</td><td><span class="label label-primary">{{$event->id}}</span></td></tr>
						<tr><td>اسم الحدث</td><td><a href="{{ route('sadmin.events.show',$event->id) }}">{{$event->title}}</a></td></tr>
						<tr><td>وصف الحدث</td><td>{{$event->description}}</td></tr>
						<tr><td>تاريخ الاضافة</td><td><span class="label label-default">{{$event->created_at->format('Y-m-d')}}</span></td></tr>
						<tr>
							<td>المشترك</td>
							<td>
								@if($event->user)
								<a href="{{ route('sadmin.users.edit',$event->user_id) }}">{{$event->user->name}}</a>
								@else
								<span class="text-danger">تم حذف العميل</span>
								@endif
							</td>
						</tr>
						<tr>
							<td>المصمم</td>
							<td>
								@if($event->designer_id)
								@if($event->designer)
								<a href="{{ route('sadmin.users.edit',$event->designer_id) }}">{{$event->designer->name}}</a>
								@else
								<span class="text-danger">تم حذف المصمم</span>
								@endif
								@else
								<span class="label label-danger">غير متابع بعد</span>
								@endif
							</td>
						</tr>
						<tr>
							<td>الحالة</td>
							<td>
								@if($event->designed)
								<span class="label bg-green">مصمم</span>
								@else
								@if($event->designer_id)
								<span class="label label-default">قيد التنفيد</span>
								@else
								<span class="label bg-purple">لم تتم المتابعة بعد</span>
								@endif
								@endif
							</td>
						</tr>
						<tr>
							<td>المجلة</td>
							<td>
								@if($event->catalog_id)
								<a href="{{ route('sadmin.catalogs.show',$event->catalog_id) }}">المجلة</a>
								@else
								<span class="label bg-purple">لا يوجد</span>
								@endif
							</td>
						</tr>
						<tr>
							<td>تاريخ الاضافة</td>
							<td><span class="label label-info">{{$event->created_at->format('Y-m-d')}}</span></td>
						</tr>						
					</tbody>
				</table>				
				
			</div>
		</div>
	</div>
</div>


<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-file-video-o"></i> الملفات المرفقة
		</div>
	</div>
	<div class="portlet-body flip-scroll">
		<div class="row">
			@forelse($files as $file)
			@if($file->type=="image")
			<div class="col-md-3 col-sm-4" >
				<img class="img-responsive" src="/upsevents/{{ $file->src }}" />
			</div>
			@else
			<div class="col-md-6 col-sm-8" >					
				<video width="100%" controls>
					<source src="/upsevents/{{ $file->src }}" type="video/mp4" />
					</video>
				</div>
				@endif
				@empty
				<div class="col-xs-12">
					<div class="note note-danger">
						<p>
							لا يوجد اية ملفات مرفقة
						</p>
					</div>
				</div>
				@endforelse
			</div>
		</div>
	</div>
	@endsection