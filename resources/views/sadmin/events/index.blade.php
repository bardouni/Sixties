@extends("sadmin/layouts/app")

@section("title","الأحداث")

@push('active-events')
active open
@endpush

@push('active-events-1')
active 
@endpush

@section("content")

<div class="portlet light bg-inverse">
	<div class="portlet-title">
		<div class="caption font-green uab">
			<i class="fa fa-cubes font-green"></i>الأحداث
		</div>
	</div>
	<div class="portlet-body flip-scroll">		
		<table-events json="events" table="events" ></table-events>
	</div>
</div>

@endsection