@extends("sadmin/layouts/app")

@section("title","إعدادات الشبكات الإجتماعية" )

@push('active-website')
active open
@endpush

@push('active-website-6')
active
@endpush


@section("content")

<form action="{{ route('sadmin..postpage',['page' => 'social' ]) }}" method="POST" >

	@include("sadmin/layouts/msgs")

	{{csrf_field()}}
	{{method_field('POST')}}
	
	<div class="portlet light bg-inverse">
	    <div class="portlet-title">
	        <div class="caption font-purple uab"><i class="fa font-purple fa-facebook"></i> الشبكات الإجتماعية</div>
	    </div>
	    <div class="portlet-body flip-scroll">

			@foreach(["facebook", "instagram", "twitter", "youtube"] as $social)
	    	<div class="col-xs-12 col-sm-6">
		    	<div class="form-group">
		    		<label><i class="fa fa-fw fa-{{$social}}"></i> {{$social}}</label>
		    		<input value="{{ old($social) ?: (isset(\Settings::get('social')[$social])?\Settings::get('social')[$social]:'') }}" name="{{$social}}" class="form-control" type="text">
		    	</div>
	    	</div>
			@endforeach	    	
	
			<div class="form-group">
				<button class="btn btn-primary">حفظ</button>
			</div>
	    </div>
    </div>


</form>
@endsection