@extends("sadmin/layouts/app")

@section("title","أعمالنا")

@push('active-website')
active open
@endpush

@push('active-website-7')
active
@endpush


@section("content")



	@include("sadmin/layouts/msgs")

	
	<div class="portlet light bg-inverse">
	    <div class="portlet-title">
	        <div class="caption font-purple uab"><i class="fa font-purple fa-sticky-note"></i> صور أعمالنا</div>
	    </div>
	    <div class="portlet-body flip-scroll">

	    	<div class="row">
	    		<div class="col-xs-12 col-md-6">
	    			<table class="table table-hover table-bordered table-striped">
	    				<tr>
	    					<td>الصورة</td>
	    					<td>عمليات</td>
	    				</tr>
	    				@foreach(\Settings::get('works',[]) as $w)
	    				<tr>
	    					<td><img style="max-width: 100px;max-height: 100px;" src="/systemups/works/{{ $w }}"></td>
	    					<td>
	    						<form method="POST" action="{{route('sadmin..works.delete' , ['image'=>$w])}}" >
	    							{{csrf_field()}}
	    							{{method_field('delete')}} 
	    							<button class="btn btn-sm btn-danger">حذف الصورة</button>
	    						</form>		
	    					</td>
	    				</tr>
	    				@endforeach
	    			</table>
	    		</div>
	    	</div>

			<form action="{{ route('sadmin..postpage',['page' => 'works' ]) }}" method="POST" enctype="multipart/form-data" >
				{{csrf_field()}}
				{{method_field('POST')}}
			    <div class="form-group" >
			    	<label>الصور</label>
			    	<input class="file" multiple type="file" name="images[]" />
			    </div>

		    		
				<div class="form-group">
					<button class="btn btn-primary">حفظ</button>
				</div>
			</form>
	    </div>
    </div>

	<div class="portlet light bg-inverse">
	    <div class="portlet-title">
	        <div class="caption font-purple uab"><i class="fa font-purple fa-sticky-note"></i> صور السلايدر</div>
	    </div>
	    <div class="portlet-body flip-scroll">

	    	<div class="row">
	    		<div class="col-xs-12 col-md-6">
	    			<table class="table table-hover table-bordered table-striped">
	    				<tr>
	    					<td>الصورة</td>
	    					<td>عمليات</td>
	    				</tr>
	    				@foreach((\Settings::get('slider') ?: []) as $w)
	    				<tr>
	    					<td><img style="max-width: 100px;max-height: 100px;" src="/systemups/slider/{{ $w }}"></td>
	    					<td>
	    						<form method="POST" action="{{route('sadmin..slider.delete' , ['image'=>$w])}}" >
	    							{{csrf_field()}}
	    							{{method_field('delete')}} 
	    							<button class="btn btn-sm btn-danger">حذف الصورة</button>
	    						</form>		
	    					</td>
	    				</tr>
	    				@endforeach
	    			</table>
	    		</div>
	    	</div>

			<form action="{{ route('sadmin..postpage',['page' => 'slider' ]) }}" method="POST" enctype="multipart/form-data" >			    
				{{csrf_field()}}
				{{method_field('POST')}}
			    <div class="form-group" >
			    	<label>الصور</label>
			    	<input class="file" multiple type="file" name="images[]" />
			    </div>		    		
				<div class="form-group">
					<button class="btn btn-primary">حفظ</button>
				</div>
			</form>
	    </div>
    </div>

@endsection