@extends("sadmin/layouts/app")

@section("title","صفحة : " . $pageName )

@push('active-website')
active open
@endpush

@push('active-website-' . $cuurentPage )
active
@endpush

@push('scripts')
<script src="/sadminassets/js/editor/jquery.tinymce.min.js" ></script>
<script src="/sadminassets/js/editor/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector: 'textarea',
		plugins: [],
		toolbar:'' ,
		menubar:false ,
		language: 'ar' ,
		height:'500px'
	})
</script>
@endpush


@section("content")

<form action="{{ route('sadmin..postpage',['page' => $target ]) }}" method="POST" >

	@include("sadmin/layouts/msgs")

	{{csrf_field()}}
	{{method_field('POST')}}
	<div class="form-group">
		<textarea name="content" id="edit" >{{ $content }}</textarea>
	</div>

	<div class="form-group">
		<button class="btn btn-primary">حفظ</button>
	</div>

</form>
@endsection