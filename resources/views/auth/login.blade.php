@extends('front/layouts/app')

@section("title","تسجيل دخول")

@section('content')

@include('front/layouts/header')

<section class="log">

    <h2 class="uab title" >
        تسجيل الدخول
    </h2>

    <form role="form" novalidate method="POST" action="{{ url('login') }}">
        {{ csrf_field() }}
        
        <div class="p-y-1">
            @foreach($errors->all() as $error)
                <p class="danger-message">
                    {{ $error }}
                </p>
            @endforeach
        </div>

        <div class="form-group">

            <input type="email" class="custom-input-form" value="{{old('email')}}" name="email" placeholder="البريد الإلكتروني" required autofocus>
            
        </div>

        <div class="form-group">

            <input type="password" class="custom-input-form" placeholder="كلمة المرور" value="{{old('password')}}" name="password" required>
                        
        </div>

        <div class="container-forgot" >
            <a href="{{ url('password/reset') }}">
                نسيت كلمة المرور
            </a>
        </div>

        <div class="container-button">
            <input type="hidden" name="remember" value="true" >
            <button type="submit" class="btn btn-primary">
                دخول
            </button>                
            
        </div>
    </form>

</section>

@endsection

