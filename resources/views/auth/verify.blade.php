@extends("front/layouts/app")

@section("title","تأكيد البريد الإلكتروني")

@section("content")

@include("front/layouts/header")

<section class="small-container p-v-1">

		<form action="{{ url('verify') }}" method="get" >

			@include('front/layouts/msgs')
			<div class="title">المرجو تأكيد البريد الإلكتروني</div>
			<p>تم إرسال كود التفعيل على حسابك : {{ $auth->email }}</p>

			<div class="form-group"><input name="code" type="text" placeholder="كود التفعيل" class="custom-input-form text-right"></div>
			<div class="form-group text-center"><button class="simple-btn btn">تأكيد</button></div>
		</form>		

</section>

@endsection