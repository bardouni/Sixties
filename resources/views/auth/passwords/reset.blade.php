@extends('front/layouts/app')

@section("title","إعادة تعيين كلمة المرور")

@section('content')

@include('front/layouts/header')

<section class="log">

    <h2 class="uab title" >
        ادخال كلمة المرور الجديدة
    </h2>

    <form novalidate role="form" method="POST" action="{{ url('password/reset') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">
        
        <input type="hidden" name="email" value="{{ $email }}" autofocus>
                    
        <div class="form-group">

            <input type="password" class="custom-input-form" name="password" placeholder="كلمة المرور الجديدة" required>
            @if ($errors->has('password'))
            <p class="danger-message">
                {{ $errors->first('password') }}
            </p>
            @endif

        </div>

        <div class="form-group">

            <input type="password" class="custom-input-form" name="password_confirmation" placeholder="أعد كتابة كلمة المرور" required>
            @if ($errors->has('email'))
            <p class="danger-message">
                {{ $errors->first('email') }}
            </p>
            @endif
        </div>

        <div class="container-button">

            <button type="submit" class="btn btn-primary">
                إعادة تعيين
            </button>
            
        </div>
    </form>

</section>
@endsection
