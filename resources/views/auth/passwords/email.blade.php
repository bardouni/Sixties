@extends('front/layouts/app')

@section("title","نسيت كلمة المرور")

@section('content')

@include('front/layouts/header')

<section class="log">

    <h2 class="uab title" >
        نسيت كلمة المرور
    </h2>


    <form role="form" novalidate method="POST" action="{{ url('password/email') }}">

        @if (session('status'))
        <div class="success-message">
            {{ session('status') }}
        </div>
        @endif
        
        {{ csrf_field() }}

        <div class="form-group">            
            <div class="form-group" >
                <input type="email" class="custom-input-form" placeholder="البريد الإلكتروني" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                <p class="danger-message">
                    {{ $errors->first('email') }}
                </p>
                @endif
            </div>
        </div>

        <div class="container-button">

            <button type="submit" class="btn btn-primary">
                استرجاع كلمة المرور
            </button>
            
        </div>
    </form>

</section>

@endsection