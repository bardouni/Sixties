@extends('front/layouts/app')

@section("title","إنشاء حساب")

@push('styles')
<link rel="stylesheet" href="/sadminassets/css/jquery.datetimepicker.css" />
@endpush

@push('scripts')
<script src="/sadminassets/js/jquery.datetimepicker.full.min.js"></script>
<script>
    $.datetimepicker.setLocale('ar');
    $('.datetimepicker').datetimepicker({
        timepicker:false,
        lang:'ar',
        format: 'Y-m-d'
    });
</script>
@endpush

@section('content')

@include('front/layouts/header')

<section class="log">

    <h2 class="uab title" >
        إنشاء حساب
    </h2>

    <form class="register" role="form" method="POST" action="{{ url('register') }}" enctype="multipart/form-data" >

        {{ csrf_field() }}
        
        @if(count($errors) > 0)
        <div class="clearfix">
            <div class="danger-message">
                @foreach($errors->all() as $error)
                {{$error}} <br>
                @endforeach
            </div>
        </div>
        @endif

        <div class="clearfix" >
            <!-- name -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="الاسم الثلاثي *" value="{{ old('name') }}" name="name" >
                </div>
            </div>
            <!-- birth day -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group" >
                    <input type="text" class="datetimepicker custom-input-form" placeholder="تاريخ الميلاد *" value="{{ old('birthday') }}" name="birthday" >
                </div>
            </div>
            <!-- email -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="email" class="custom-input-form" placeholder="البريد الإلكتروني *" value="{{ old('email') }}" name="email" >
                </div>
            </div>
            <!-- phone -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="رقم الهاتف *" value="{{ old('phone') }}" name="phone" >
                </div>
            </div>
            <!-- city -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="المدينة" value="{{ old('city') }}" name="city" >
                </div>
            </div>
            <!-- postal -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="صندوق البريد" value="{{ old('postal') }}" name="postal" >
                </div>
            </div>
            <!-- location -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="المنطقة" value="{{ old('location') }}" name="location" >
                </div>
            </div>
            <!-- Neighborhood -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="الحي" value="{{ old('ngbh') }}" name="ngbh" >
                </div>
            </div>
            <!-- street -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="text" class="custom-input-form" placeholder="الشارع" value="{{ old('street') }}" name="street" >
                </div>
            </div>
            <!-- avatar -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <div class="custom-input-form p-a-05 clearfix"><span class="lbl">أضف صورة شخصية</span> <span class="btn btn-add-file"><input name="avatar" type="file">استعراض</span></div>
                </div>
            </div>
            <!-- password -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="password" class="custom-input-form" placeholder="كلمة المرور *" value="{{ old('password') }}" name="password" >
                </div>                
            </div>
            <!-- password confirmation -->
            <div class="col-xs-12 col-md-6 pull-right">
                <div class="form-group">
                    <input type="password" class="custom-input-form" placeholder="تاكيد كلمة المرور *" value="{{ old('password_confirmation') }}" name="password_confirmation" >
                </div>
            </div>
            <!-- Pack -->
            <div class="col-xs-12 col-md-12 pull-right">
                <div class="form-group">
                    <select name="subscribe_id" class="custom-input-form">
                        @foreach(App\SubsCatalog::visible()->get() as $catalog)
                        <option {{ ($catalog->id == request()->p) ? "selected" :"" }} value="{{$catalog->id}}">{{ $catalog->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix text-center uab">

        <custom-check-box lbl="الموافقة على" name="terms" :ischecked.once="{{ is_null(old('terms')) ? 'false' : 'true' }}" ></custom-check-box>
        <a target="_blank" href="{{url('terms')}}"> شروط الإشتراك</a>

    </div>

    <div class="container-button">

        <button type="submit" class="btn btn-primary">
            اشتراك
        </button>

    </div>

</form>


</section>

@endsection
