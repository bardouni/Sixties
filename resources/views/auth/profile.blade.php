@extends("front/layouts/app")

@section("title","الملف الشخصي" )

@push('styles')
<link rel="stylesheet" href="/sadminassets/css/jquery.datetimepicker.css" />
@endpush

@push('scripts')
<script src="/sadminassets/js/jquery.datetimepicker.full.min.js"></script>
<script>
    $.datetimepicker.setLocale('ar');
    $('.datetimepicker').datetimepicker({
        timepicker:false,
        lang:'ar',
        format: 'Y-m-d'
    });
</script>
@endpush

@section("content")

@include("front/layouts/header")

<section class="log">

	<form class="register" role="form" method="POST" action="{{ url('updateprofile') }}" enctype="multipart/form-data" >

		{{csrf_field()}}
		{{method_field('POST')}}

		<div class="text-center" >
			<div class="container-avatar" style="background-image: url({{ $user->completAvatarUrl }});" >
				<img class="hp" src="/systemups/400x400.png" />				
			</div>
		</div>


		@include('front/layouts/msgs')

		<div class="clearfix" >
			<!-- name -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="الاسم الثلاثي" value="{{ old('name') ?: $user->name }}" name="name" >
				</div>
			</div>
			<!-- birth day -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="datetimepicker custom-input-form" placeholder="تاريخ الميلاد" value="{{ old('birthday') ?: $user->birthday }}" name="birthday" >
				</div>
			</div>
			<!-- email -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="email" class="custom-input-form" placeholder="البريد الإلكتروني" value="{{ old('email') ?: $user->email }}" name="email" >
				</div>
			</div>
			<!-- phone -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="رقم الهاتف" value="{{ old('phone') ?: $user->phone }}" name="phone" >
				</div>
			</div>
			<!-- city -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="المدينة" value="{{ old('city') ?: $user->city }}" name="city" >
				</div>
			</div>
			<!-- postal -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="صندوق البريد" value="{{ old('postal') ?: $user->postal }}" name="postal" >
				</div>
			</div>
			<!-- location -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="المنطقة" value="{{ old('location') ?: $user->location }}" name="location" >
				</div>
			</div>
			<!-- Neighborhood -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="الحي" value="{{ old('ngbh') ?: $user->ngbh }}" name="ngbh" >
				</div>
			</div>
			<!-- street -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<input type="text" class="custom-input-form" placeholder="الشارع" value="{{ old('street') ?: $user->street }}" name="street" >
				</div>
			</div>
			<!-- avatar -->
			<div class="col-xs-12 col-md-6 pull-right">
				<div class="form-group">
					<div class="custom-input-form p-a-05 clearfix"><span class="lbl">أضف صورة</span> <span class="btn btn-add-file"><input name="avatar" type="file">استعراض</span></div>
				</div>
			</div>
			

			<!-- change Password Checkbox -->

			<change-password pchangepassword="{{ old('changepassword') }}" poldpassword="{{ old('oldpassword') }}" ppassword="{{ old('password') }}" ppassword_confirmation="{{ old('password_confirmation') }}"></change-password>
		</div>

		<div class="container-button">

			<button type="submit" class="btn btn-primary">
				حفظ
			</button>

		</div>

	</form>


</section>

@endsection