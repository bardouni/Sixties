@extends("front/layouts/app")

@section("title",$code)

@section("content")

<section class="errors-page">

    <section class="container-errors">
        <div>
            <fieldset class="error" >
                <legend >خطأ</legend>           
                <h1>{{$code}}</h1>
            </fieldset>
            <p class="p-error text-center" >
                {{$message}} <br>
                {!! 
                    $action
                    or
                    'عودة الى <a class="uab" href="' . URL::previous() .'"> الصفحة السابقة</a>'                    
                !!}
            </p>
        </div>
    </section>
    
</section>

@endsection