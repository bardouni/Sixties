@extends("front/layouts/app")

@section("title","ملاحظات")

@section("content")

@include("front/layouts/header")

<div class="small-container p-v-2">
	<h1 class="title" >
		@yield('title')
	</h1>
	<div class="table-container">
		<table class="table simple-table table-striped" >
			<thead>
				<tr>
					<td>الملاحظة</td>
					<td>العميل</td>
					<td>تاريخ الملاحظة</td>
				</tr>
			</thead>
			<tbody>
				@forelse($notes as $note)
				<tr>
					<td><a class="uab" href="{{ route('follow.catalogs.show',$note->catalog->id) }}" >{{ $note->id }}</a></td>
					<td>
						@if($note->user)
						<a class="uab" href="{{ route('follow.users.show',$note->user->id) }}">{{ $note->user->name }}</a>
						@else
						<span class="danger-color">تم حذف العميل</span>
						@endif
					</td>
					<td>{{ $note->created_at->format('Y-m-d') }}</td>					
				</tr>
				@empty
				<tr>
					<td colspan="10" >
						<div class="danger-message">لا يوجد اية أحداث بعد</div>
					</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

@endsection