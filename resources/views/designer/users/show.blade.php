@extends("front/layouts/app")

@section("title", $user->name )

@section("content")

@include("front/layouts/header")

<section class="follow follow-one">
	<div class="container-table">

		<h1 class="title" >
			{{$user->name}}
		</h1>

		<h2 class="title uab custom2" >
			الفعاليات غير المصممة ({{ $events->count() }})
		</h2>

		@include("front/layouts/eventslist",["data"=>$events])

		<h2 class="title uab custom2" >
			الفعاليات المصممة ({{ $devents->count() }})
		</h2>

		@include("front/layouts/eventslist",["data"=>$devents])

		<div class="text-center p-v-1" >
			<a href="{{ route('follow.catalogs.create',['to'=>$user->id])}}" class="btn btn-send-catalog">ارسال مجلة</a>			
		</div>
		
	</div>
</section>

@endsection