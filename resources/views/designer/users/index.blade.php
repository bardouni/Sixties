@extends("front/layouts/app")

@section("title","المستخدمين المتابعين")

@section("content")

@include("front/layouts/header")

<section class="follow p-b-3">
	<div class="container-table ">

		<h1 class="title" >
			المستخدمين
		</h1>

		<form action="{{ route('follow.users.store') }}" method="post" >
			
			@include("front/layouts/msgs")
						
			{!! csrf_field() !!}
			{!! method_field("POST") !!}
			<div class="table-container">
				<table class="table simple-table table-striped" >
					<thead>
						<tr>
							<td>الرقم</td>
							<td>الاسم</td>
							<td>عدد الأحداث</td>
							<td>متابعة</td>
						</tr>
					</thead>
					<tbody>
						@forelse($users as $user)
						<tr class="{{ $user->id == request()->u ? 'focusing' : '' }}" >
							<td>{{ $user->id }}</td>
							<td><a href="{{ route('follow.users.show',$user->id) }}">{{ $user->name }}</a></td>
							<td>{{ $user->events()->count() }}</td>
							<td>
								<follow-user :user="{{$user->id}}" :following="{{ ($user->follower_id == $auth->id ) ? 'true' : 'false' }}" />
							</td>
						</tr>
						@empty
						<tr>
							<td>
								<div class="danger-message">لا يوجد اية أحداث بعد</div>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>

			<div class="text-center p-v-1">
				<button type="sublit" class="btn simple-btn ">حفظ</button>
			</div>
		
		</form>
	</div>
</section>

@endsection