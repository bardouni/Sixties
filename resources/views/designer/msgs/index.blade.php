@extends('front/layouts/app')

@section("title","الرسائل")

@section("content")

@include('front/layouts/header')

<section class="msgs-page">
	
	<h1 class="title">
		الرسائل
	</h1>

	<section class="msgs-container">
		
		@include("front/layouts/msgs")

		<div class="table-responsive">
			<div class="table-container" >
				<table class="table simple-table table-striped" >
					<thead>
						<tr class="uab">
							<td>آخر رد</td>	
							<td>العنوان</td>
							<td>التاريخ</td>
						</tr>		
					</thead>
					<tbody>
						@forelse($groups as $group)
						<tr>
							<td>
								<a class="info-link info-color" href="{{ route('designer.conversations.show',$group->id) }}" >
									@if($group->lastanswer)

										@include("designer/msgs/label",["msg"=>$group->lastanswer])

										{{$group->lastanswer->content}}

									@endif
								</a>
							</td>
							<td>{{ $group->title }}</td>
							<td>{{ $group->created_at->format("Y-m-d") }}</td>
						</tr>
						@empty
						<tr>
							<td colspan="50" >
								<div class="danger-message">
									لا توجد اية رسائل
								</div>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>

		<div class="text-center">
			<form-add-new-msg 
				:to-user="{{ (request()->user) ? request()->user : 'null' }}" 
				:is-designer="{{ $auth->can('is-designer') ? 'true' : 'false' }}" 
				:is-admin="{{ $auth->can('is-admin') ? 'true' : 'false' }}" 
				:pshow-form="{{ count($errors) > 0 ? 'true' : 'false' }}" 
				ptitle="{{ old('title') }}" 
				pcontent="{{ old('content') }}" 
				:has-follower="{{ $auth->can('has-follower') ? 'true' : 'false' }}"
				action="/follow/conversations"
			>
				<div class="form-group">
					<label>رسالة الى</label>
					<select name="target" class="text-right custom-input-form">
						<option value="admin">الادارة</option>
						@foreach($customers as $customer)
						<option {{ request()->to == $customer->id ? 'selected' : '' }} value="{{$customer->id}}" >{{$customer->name}}</option>
						@endforeach
					</select>
				</div>
			</form-add-new-msg>
		</div>

	</section>

</section>

@endsection