@extends("front/layouts/app")

@section("title","الأحداث")

@section("content")

@include("front/layouts/header")

<section class="follow">
	<div class="container-table p-b-3">

		<h1 class="title" >
			الأحداث
		</h1>

		<div class="table-container">
			<table class="table simple-table table-striped" >
				<thead>
					<tr>
						<td>رقم الحدث</td>
						<td>عنوان الحدث</td>
						<td>المشترك</td>
						<td>المجلة</td>
					</tr>
				</thead>
				<tbody>
					@foreach($events as $event)
					<tr>
						<td>{{$event->id}}</td>
						<td><a href="{{ route('follow.events.show',$event->id) }}">{{$event->title}}</a></td>
						<td><a href="{{route('follow.users.show',$event->user->id)}}">{{$event->user->name}}</a></td>
						<td>
							@if($event->catalog)
								<a href="{{ route('follow.catalogs.show', $event->catalog->id ) }}">معاينة</a>
							@else
								<span class="danger-color">لا يوجد</span>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<div class="p-v-2 text-center">
			<a class="btn simple-btn" href="{{ route('follow.catalogs.create') }}">ارسال مجلة</a>
		</div>

	</div>
</section>

@endsection
