@extends("front/layouts/app")

@section("title",$user->name)

@section("content")

@include("front/layouts/header")

<section class="show-event">
	
	<div class="small-container">

		<div class="p-v-1">
			@include('front/layouts/msgs')
		</div>

		<div class="form-group">
			<label>عنوان الحدث</label>
			<div class="custom-input-form text-right">
				{{$event->title}}
			</div>
		</div>

		<div class="form-group">
			<label>نص الحدث</label>
			<div class="custom-input-form text-right">{{$event->description}}</div>
		</div>

		<div class="form-group">
			<label>الملفات المرفقة</label>
			
			<list-files :is-designer="true" :files="{{ $event->files->toJson() }}" ></list-files>
		</div>

		<div class="form-group text-center">
			<a href="{{ $event->user ? route('designer.conversations.index',['to' => $event->user->id]) : '/' }}" class="btn btn-contact">مراسلة العميل</a>
			<a href="{{ route('follow.events.downloadfiles',$event->id) }}" class="btn simple-green-button" >تحميل جميع الملفات</a>
		</div>
	</div>

</section>

@endsection
