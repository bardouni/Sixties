@extends("front/layouts/app")

@section("title","ارسال مجلة")

@section("content")

@include("front/layouts/header")

<section class="send-catalog">
	
	<div class="small-container">

		<div class="p-y-1">
			@include("front/layouts/msgs")
		</div>

		<send-catalog to="{{ request()->to ?: old('customer') }}" ></send-catalog-form>
		
	</div>

</section>

@endsection