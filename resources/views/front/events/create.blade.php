@extends('front/layouts/app')

@section("title","اضافة حدث")

@section("content")

@include('front/layouts/header')

<section class="create-event">

	<form action="/events" method="POST" >

		@include('front/layouts/msgs')

		{{csrf_field()}}
		{{method_field("POST")}}

		<input type="hidden" value="{{$generated_token}}" name="generated_token" />
		<div class="form-group"><input type="text" name="title" class="text-right custom-input-form" placeholder="عنوان الحدث" ></div>
		<div class="form-group"><textarea rows="4" name="description" class="text-right custom-input-form" placeholder="نص الحدث ..." ></textarea></div>
		<div class="clearfix">
			<div class="side-images">
				<form-add-file generated-token="{{$generated_token}}" type="image" lbl="أضف صورة" />
			</div>
			<div class="side-video">
				<form-add-file generated-token="{{$generated_token}}" type="video" lbl="أضف فيديو" />
			</div>
		</div>
		<div class="clearfix">
			<div class="form-group text-center p-y-2">
				<input type="submit" class="btn btn-send-event" value="حفظ">
			</div>
		</div>

	</form>


</section>


@endsection
