@extends("front/layouts/app")

@section("title",$event->name)

@section("content")

@include("front/layouts/header")

<section class="show-event">
	
	<div class="small-container">
		<div class="form-group">
			<label>عنوان الحدث</label>
			<div class="custom-input-form text-right">
				{{$event->title}}
			</div>
		</div>

		<div class="form-group">
			<label>نص الحدث</label>
			<div class="custom-input-form text-right">{{$event->description}}</div>
		</div>

		<div class="form-group">
			<label>المصمم المتابع</label>
			@if($event->designer)
			<div class="custom-input-form text-right">
				{{$event->designer->name}}
			</div>
			@else
			<p class="text-danger">
				الحدث لا يتابعه أي مصمم
			</p>
			@endif
		</div>

		<div class="form-group">
			<label>الملفات المرفقة</label>
			
			<list-files :is-designer="true" :files="{{ $event->files->toJson() }}" ></list-files>

		</div>
		
	</div>

</section>

@endsection