@extends("front/layouts/app")

@section("title","المجلة")

@section("content")

@include("front/layouts/header")

<section class="show-catalog">
	<h1 class="title">
		@yield('title')
	</h1>
	<div class="small-container p-y-2">
	
		<div class="p-y-1">
			@include("front/layouts/msgs")
		</div>

		<div>
			<!-- <embed src="/catalogs/{{ $catalog->src }}" width="100%" height="700" type='application/pdf' /> -->
			<object data="/catalogs/{{ $catalog->src }}" type="application/pdf" width="100%" height="700">
			  <p><a href="/catalogs/{{ $catalog->src }}" class="uab" >رابط المجلة</a></p>
			</object>
		</div>

		<form class="text-center p-v-1" method="post" enctype="multipart/form-data" action="{{ route('follow.catalogs.update', $catalog->id ) }}" >
			{{method_field('put')}}
			<a href="/archive/{{ $catalog->id }}/download" class="download-catalog-btn btn simple-green-button">تحميل المجلة</a>
			@if(auth()->user()->can("is-designer"))
			<form-uploaf-newversion-catalog ></form-uploaf-newversion-catalog>
			@endif
		</form>
		
		<div class="p-y-1">
			@if($catalog->notes()->count() > 0)
			<div class="text-center">				
				<label>ملاحظات حول المجلة</label>				
			</div>
			<ul class="list-notes" >
				@foreach($catalog->notes as $note)
				<li>
					<div dir="rtl" lang="ar" class="info-message text-right">
						<span class="uab "  style="color: #4D4D4D;" >							
							@if($catalog->designer_id == $note->user_id)
							المصمم :
							@else
							أنا :
							@endif
						</span>
						{{$note->content}}
					</div>
				</li>
				@endforeach
			</ul>
			@endif
		</div>
		
		
		<form class="p-v-1" action="{{ route('notes.store',$catalog->id) }}" method="POST" >
			{{csrf_field()}}
			{{method_field("POST")}}
			<div class="form-group">
				<textarea name="note" rows="4" class="text-right custom-input-form" placeholder="لديك ملاحظة ؟" ></textarea>
			</div>
			<div class="form-group text-center">
				<input type="submit" class="btn simple-btn" value="أضف ملاحظة" />
			</div>
		</form>
		
	</div>

</section>

@endsection