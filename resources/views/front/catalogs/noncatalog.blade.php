@extends("front/layouts/app")

@section("title","المجلة")

@section("content")

@include("front/layouts/header")

<div class="small-container p-v-2 p-h-1">
	<p class="danger-message">
		عذرًا! المجلة غير مصممة بعد. يتم تصميم المجلة في نهاية الاشتراك .
	</p>
</div>

@endsection