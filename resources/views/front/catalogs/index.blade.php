@extends("front/layouts/app")

@section("title","الأرشيف")

@section("content")

@include("front/layouts/header")

<section class="catalogs">

	<h1 class="title">@yield('title')</h1>

	<div class="container-table p-y-2">
		<div class="table-container">
			<table class="table simple-table table-hover table-striped">
				<thead>
					<tr>
						<td>الرقم</td>
						<td>التاريخ</td>
						<td>العنوان</td>
						<td>الصور</td>
						<td>الفيديو</td>
						<td>المجلة</td>
					</tr>
				</thead>
				<tbody>
					@foreach($events as $event)
					<tr>
						<td>{{ $event->id }}</td>
						<td>{{ $event->created_at->format("Y-m-d") }}</td>
						<td><a class="uab" href="{{ route('events.show',$event->id) }}">{{ $event->title }}</a></td>
						<td>{{ $event->files()->onlyImages()->count() }}</td>
						<td>{{ $event->files()->onlyVideo()->count() }}</td>
						<td>
							@if($event->designed)
							<a class="info-link info-color" href="{{ route('archive.show',$event->catalog->id) }}">معاينة</a>
							@else
							<span class="danger-color">قيد التنفيذ</span>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@endsection