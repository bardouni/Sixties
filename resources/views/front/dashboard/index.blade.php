@extends('front/layouts/app')

@section("title","لوحة التحكم")

@section("content")

@include('front/layouts/header')


<section class="user-dashboard">
	
	<div class="boxs clearfix">	
		
		<div>
			@include("front/layouts/msgs")
		</div>

		@if($auth->can('is-admin'))

		<div class="box">
			<a href="/admin/events">
				<img class="img-responsive" src="/systemups/dashboardicons/11-1.png">
			</a>
		</div>
		
		<div class="box">
			<a href="/admin/users">
				<img class="img-responsive" src="/systemups/dashboardicons/13-1.png">
			</a>
		</div>

		@elseif($auth->can('is-designer'))
		
		<div class="box">
			<a href="{{route('follow.events.index')}}">
				<img class="img-responsive" src="/systemups/dashboardicons/11-1.png">
			</a>
		</div>
		
		<div class="box">
			<a href="{{ route('follow.users.index') }}">
				@if($usersWithoutFollower)
				<img class="img-responsive" src="/systemups/dashboardicons/11.png">
				@else
				<img class="img-responsive" src="/systemups/dashboardicons/12.png">
				@endif
			</a>
		</div>

		@elseif($auth->can('is-subscriber'))

		<div class="box">
			<a href="{{ route('events.create') }}">
				<img class="img-responsive" src="/systemups/dashboardicons/7.png">
			</a>
		</div>
		
		<div class="box">
			<a href="{{ route('archive.index') }}">
				<img class="img-responsive" src="/systemups/dashboardicons/8.png">
			</a>
		</div>
		
		<div class="box">
			<a href="{{ $auth->catalogUrl }}">
				<img class="img-responsive" src="/systemups/dashboardicons/9.png">
			</a>
		</div>
		
		<div class="box">
			<a href="{{ url('renwal') }}">
				<img class="img-responsive" src="/systemups/dashboardicons/10.png">
			</a>
		</div>

		@elseif($auth->hasNoSubscribe)

			@include('front/layouts/subscribeMsg',["catalog"=>$auth->initialCatalog]);

		@else

		<div class="box">
			<a href="{{ route('archive.index') }}">
				<img class="img-responsive" src="/systemups/dashboardicons/8.png">
			</a>
		</div>
		
		<div class="box">
			@if($auth->hadCatalog)
			<a href="{{ route('archive.show',$auth->hadCatalog->id) }}">
			@else
			<a href="{{ route('archive.index') }}">
			@endif
				<img class="img-responsive" src="/systemups/dashboardicons/9.png">
			</a>
		</div>

		@endif

	</div>

</section>

@endsection
