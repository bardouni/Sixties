@extends('front/layouts/app')

@section('title','الرئيسية')

@push('active-page-home')
active
@endpush

@section("content")

<section class="slide">

    @include('front/layouts/header')

    <div id="carousel-top-slider" class="carousel slide" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">      
    @foreach((\Settings::get('slider') ?: []) as $s)
    <div class="in-slide item {{ ($loop->first) ? 'active':'' }} "  style="background-image: url('/systemups/slider/{{$s}}');">
      <img style="min-height: 350px;" src="/systemups/hlpb.png" class="hlpimg" />
      <div class="carousel-caption">
        <p>
            حياتك ثمينة <br>
            حول لحظاتها <span class="uab">الذهبية</span> إلى مجلات <span class="uab">حقيقية</span> <br>
            <a href="#subscribe" class="subscribe-button uar" >
                اشترك الآن!
            </a>
        </p>
      </div>
    </div>
    @endforeach
  </div>

</div>

</section>

<!-- <section class="features clearfix">
    <h2 class="title" >
        المميزات
    </h2>
    <div class="features-boxs">
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/1.png" alt="دعم الفيديو">
            <h4 class="uab secondary-title">دعم الفيديو</h4>
            <p>
                مجموعة من الفلاتر المميزة لمعالجة الصور وتصحيح عيوبها.
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/2.png" alt="تصاميم مميزة">
            <h4 class="uab secondary-title">تصاميم مميزة</h4>
            <p>
                تصاميم مميزة وأنيقة لإعطاء صورك فرادة وحيوية.
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/3.png" alt="معالجة أولية">
            <h4 class="uab secondary-title">معالجة أولية</h4>
            <p>
                إمكانية رفع مقاطع الفيديو وإضافتها إلى مجلة ذكرياتك.
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/4.png" alt="إخراج احترافي">
            <h4 class="uab secondary-title">إخراج احترافي</h4>
            <p>
                باقات اشتراك متنوعة تُساعدك على اختيار ما يناسبك.
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/5.png" alt="لا تضيّع شيئًا">
            <h4 class="uab secondary-title">لا تضيّع شيئًا</h4>
            <p>
                مع مجلة احترافية مُفهرسة تبعًا للمناسبات لن تفقد صورك مجددًا.
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" >
            <img src="/systemups/features/6.png" alt="اشتراكات متنوعة">
            <h4 class="uab secondary-title">اشتراكات متنوعة</h4>
            <p>
                فريقنا مكوّن من مصممين خبراء ينتظرون صورك لابتكار لوحة فنية متقنة.
            </p>
        </div>
    </div>
</section> -->

@include("front/layouts/works",["data"=>$works,"title"=>"أعمالنا"])

@include('front/layouts/subscribeTemplate',['showTitle'=>true])

<footer class="footer">
    <p class="uab" >
        لديك استفسار ؟ لا تتردد في <a class="yellow-span" href="{{ url('/contact') }}" >التواصل معنا</a>
    </p>
    <hr class="hr-before-social" >
    <div class="social" >
        @foreach(["facebook", "instagram", "twitter", "youtube"] as $social)
            @continue(!isset(\Settings::get("social")[$social]) || trim(\Settings::get("social")[$social]) == "")
            <a href="{{ \Settings::get('social')[$social] }}"><i class="fa fa-fw fa-{{ $social }}"></i></a>
        @endforeach
    </div>
</footer>

@endsection

