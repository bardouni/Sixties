@if(Auth::check())
	<dropdown-menu-auth
	token="{{ csrf_token() }}"
	:is-designer="{{ auth()->user()->can('is-designer') ? 'true' : 'false' }}"
	:is-admin="{{ auth()->user()->can('is-admin') ? 'true' : 'false' }}"
	:is-user="{{ auth()->user()->role == 'user' ? 'true' : 'false' }}"	
	:is-sadmin="{{ auth()->user()->can('is-superadmin') ? 'true' : 'false' }}"
	:id="{{ auth()->user()->id }}"
	complet-name="{{ str_slug(auth()->user()->name) }}"
	avatar="{{ auth()->user()->completAvatarUrl }}"	
	name="{{ explode(' ',auth()->user()->name)[0] }}">	
	</dropdown-menu-auth>
@else
	<a class="link-login button-auth" href="{{ url('login') }}" >دخول</a>
@endif
