<!DOCTYPE html>
<html lang="ar" dir="rtl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="/systemups/logo.png" />
	<meta name="x-author" content="bardouni naoufal facebook.com/bardouni.naoufal" /> 
        <title>@yield('title',config('app.name'))</title>
        
        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="/ff/fonts.css">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="/css/app.css">
        
        @stack('styles')

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                'isAtuh' => Auth::check()
            ]); ?>
        </script>
        
    </head>
    <body>
        <section id="app">

            @yield('content')

        </section>
        
        <script src="/js/app.js" ></script>

        @stack('scripts')
            
    </body>
</html>
