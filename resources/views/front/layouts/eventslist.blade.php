<section class="works">    

    <div class="works-boxs clearfix events">
        @foreach($data as $e)
        <div class="work pull-right col-xs-6 col-sm-3">
            <div class="container-img" style="background-image: url({{ $e->avatar }});" >
                <img src="http://placehold.it/190x243" >
                <a href="{{ !is_null($e->catalog) ? route('follow.catalogs.show',$e->catalog->id ) : '#' }}" class="description-work">
                    <p class="uar" >{{$e->description}}</p>
                </a>
            </div>
        </div>
        @endforeach

    </div>
</section>
