<section class="works">
    @if(isset($title))
    <h2 class="title" >
        {{$title}}
    </h2>
    @endif

    <div class="works-boxs clearfix">
        @foreach((\Settings::get('works' ,[])) as $e)
        <div class="work col-xs-6 col-sm-3">
            <div class="container-img" style="background-image: url(/systemups/works/{{ $e }});" >
                <img src="http://placehold.it/190x243" >               
            </div>
        </div>
        @endforeach

    </div>
</section>

@push("styles")

@endpush

@push("scripts")
<script>
    $('.carousel').carousel()
</script>
@endpush