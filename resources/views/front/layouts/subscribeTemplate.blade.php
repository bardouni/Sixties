<section class="subsribe" id="subscribe" >
    
    @if($showTitle)
    <h2 class="title">الاشتراكات</h2>
    @endif
    
    <div class="subsribe-box uar clearfix">
        @foreach($subs as $s)
        <div class="in-subsribe-box {{ $s->popular ? 'popular' : '' }}" >
            <div class="info-subscribe" >
                @if($s->popular)
                <img src="/systemups/popular.png" class="img-popular" >
                @endif
                <div class="top-div">                
                    <h3 class="title-subscibe uab">{{$s->name}}</h3>
                </div>
                <ul class="ul-other-attrs">
                @foreach($s->data as $attr)
                <li class="other-attr" >{{$attr}}</li>
                @endforeach                
                </ul>
                <div class="price-subcribe">
                    <span class="uab">{{ $s->price }}</span> ريال
                </div>
                <div class="container-btn-subscribe" >
                    <a class="btn-subcribe uab btn" href="{{ route('subscribe.show', $s->id )}}" >
                        <strong>اشترك الآن </strong>
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</section>
