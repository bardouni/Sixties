@extends("front/layouts/app")

@section("title",$title)

@push("active-page-" . request()->path())
active
@endpush

@section("content")

@include("front/layouts/header")

<div class="small-container">

	{!! $content !!}

	@if(isset($contactForm) && $contactForm )
	
	<form action="/contact" method="post" >

		@include('front/layouts/msgs')

		{{csrf_field()}}
		{{method_field('POST')}}
		<div class="form-group"><input type="text" class="custom-input-form text-right" name="name" value="{{old('name')}}" placeholder="الإسم"></div>
		<div class="form-group"><input type="text" class="custom-input-form text-right" name="email" value="{{old('email')}}" placeholder="البريد الإلكتروني"></div>
		<div class="form-group">
			<textarea name="contentmsg" placeholder="أكتب شيئا" class="custom-input-form text-right">{{old('contentmsg')}}</textarea>
		</div>
		<div class="text-center form-group"><button class="btn simple-btn">إرسال</button></div>


	</form>

	@endif

</div>

@endsection