<section class="subsribe p-h-1">
	<h1 class="title">@yield('title')</h1>
	<div class="small-container ">
					
		{!! $catalog->msg !!}
		
		<div class="p-v-2">
			<form enctype="multipart/form-data" action="{{route('subscribe.store')}}"  method="POST" >
				{{csrf_field()}}
				{{method_field("post")}}
				<div class="clearfix">
				<div class="container-add-file-button clearfix form-group">
						<div class="btn-add-file btn simple-btn">
							استعراض
							<input name="picpay" type="file" >
						</div>
						<span class="lbl">أضف صورة الحوالة</span>
					</div>
				</div>

				<div class="text-center" >
					<button class="btn simple-btn">إرسال</button>
				</div>
			</form>
		</div>

	</div>
</section>