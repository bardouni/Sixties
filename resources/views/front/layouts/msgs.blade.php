@if (session('success-message'))
<div class="success-message">
    {!! session('success-message') !!}
</div>
@endif

@foreach($errors->all() as $error)
<div class="danger-message">
    {{ $error }}
</div>
@endforeach