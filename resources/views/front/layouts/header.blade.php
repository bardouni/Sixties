<header class="main-header">
	<section class="logo-section">
		<img src="/systemups/logo.png" alt="{{ config('app.name') }}" class="logo" />
	</section>

	<nav class="main-nav clearfix">
	
		<ul class="menu-1 clearfix" dir="ltr" >

			
			<li class="menu-item-auth clearfix" >
				@include("front/layouts/linkauth")

				<button @click="tg" class="btn-toggle-menu" >
					<i class="fa fa-fw fa-bars"></i>
				</button>
			</li>

			<li :class="{'open-menu menu-item parent-menu-2' : stateNavBar,'menu-item  parent-menu-2':!stateNavBar}" dir="rtl" >
				<ul class="menu-2" >

					<li><a class="link-menu-2 @stack('active-page-home')" href="{{url('/')}}">الرئيسية</a></li>
					<li><a class="link-menu-2 @stack('active-page-aboutus')" href="{{ url('/aboutus') }}">من نحن</a></li>
					<li><a class="link-menu-2 @stack('active-page-ourvision')" href="{{url('ourvision')}}">رؤيتنا</a></li>
					<li><a class="link-menu-2 @stack('active-page-goals')" href="{{url('goals')}}">أهدافنا</a></li>
					<li><a class="link-menu-2 @stack('active-page-subscribe')" href="{{url('subscribe')}}">الاشتراكات</a></li>
					<li><a class="link-menu-2 @stack('active-page-contact')" href="{{url('contact')}}">اتصل بنا</a></li>

				</ul>
			</li>

		</ul>

	</nav>
	
</header>
