@extends("front/layouts/app")

@push("active-page-subscribe")
    active
@endpush

@section("title","الاشتراكات")

@section("content")

@include("front/layouts/header")

<section class="content-subscribe-page">

	@include('front/layouts/subscribeTemplate',['showTitle'=>false])
	
</section>

@endsection
