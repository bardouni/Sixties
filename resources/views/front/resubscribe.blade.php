@extends("front/layouts/app")

@section("title","تجديد الإشتراك")

@section("content")

@include("front/layouts/header")

<div class="small-container p-v-1">
	
	@if(!is_null($subscribe))
	@if(\Carbon\Carbon::now()->diffinDays( (new \Carbon\Carbon($subscribe->started_at))->addDays($subscribe->days) ) < 7)
	اشتراكك سينتهي خلال 
	<span class="uab">{{ \Carbon\Carbon::now()->diffinDays( (new \Carbon\Carbon($subscribe->started_at))->addDays($subscribe->days) ) }}</span>
	يومًا.
	@else
	<p>تمّ الاشتراك في تاريخ :<span class="uab">{{$subscribe->started_at}}</span> </p>
	<p>اشتراكك صالح لغاية :<span class="uab">{{ (new \Carbon\Carbon($subscribe->started_at))->addDays($subscribe->days)->format('Y-m-d') }}</span></p>		
	@endif
	@else
	<p>للإشتراك ، اضغط <a href="{{ url('subscribe') }}" class="uab" >على الرابط</a></p>
	@endif

	<hr>
	<div>
		@include("front/layouts/msgs")
	</div>
	<div>
		{!! $catalog->msg !!}
	</div>
	<div class="subsribe">
		<div class="p-v-2">
			<form enctype="multipart/form-data" action="{{route('subscribe.store')}}"  method="POST" >
				{{csrf_field()}}
				{{method_field("post")}}
				<div class="clearfix">
					<div class="container-add-file-button clearfix form-group">
						<div class="btn-add-file btn simple-btn">
							استعراض
							<input name="picpay" type="file" >
						</div>
						<span class="lbl">أضف صورة الحوالة</span>
					</div>
				</div>

				<div class="text-center" >
					<button class="btn simple-btn">إرسال</button>
				</div>
			</form>
		</div>
	</div>

</div>

@endsection