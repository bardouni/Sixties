@extends("front/layouts/app")

@section("title",$group->title)

@section("content")

@include("front/layouts/header")

<section class="msgs-page">
	<h1 class="title">
		{{$group->title}}
	</h1>

	<section class="msgs-container">

		<div class="msgs ckearfix">

			@foreach($msgs as $msg)
			<div>
				<div class="clearfix parent-msg">
					<div class="p-msg {{ ($msg->from_user_id == $auth->id || ($auth->can('is-admin') && $msg->from_user_id == 0) ) ? 'right-msg' : 'left-msg' }} " >
						<div class="msg">

							@include("admin/msgs/label",compact("auth","msg"))

							{{$msg->content}}
						</div>
						<div class="time">
							{{ $msg->humanstime }}
						</div>
					</div>
				</div>
			</div>
			@if($msg->files()->count() > 0)
			<div class="clearfix" >
				<list-files is-designer="true" :files='{!! $msg->files->toJson() !!}' pfolder="upsmsgs" ></list-files>
			</div>
			@endif
			@endforeach

		</div>

		<element-receive-msgs
		:is-designer="{{ $auth->can('is-designer') ? 'true' : 'false' }}"
		:is-admin="{{ $auth->can('is-admin') ? 'true' : 'false' }}"

		:has-follower="{{ !$auth->can('has-follower') ? 'false' : 'true' }}"
		:last-msg="{{ $last_msg_id }}"
		:group-id="{{ $group->id }}"
		:me="{{ $auth->id }}"> </element-receive-msgs>

	</section>	

</section>

@endsection