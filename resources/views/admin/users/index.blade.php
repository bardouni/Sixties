@extends("front/layouts/app")

@section("title","المشتركين")

@section("content")

@include("front/layouts/header")

<section class="users p-b-3">
	<div class="container-table ">

		<h1 class="title" >
			@yield("title")
		</h1>

		<div class="table-container">
			<table class="table simple-table table-striped" >
				<thead>
					<tr class="uab" >
						<td>الرقم</td>
						<td>تاريخ التسجيل</td>
						<td>الاسم</td>
						<td>رقم الجوال</td>
						<td>المجلة</td>
						<td>الحالة</td>
					</tr>
				</thead>
				<tbody>
					@forelse($users as $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->created_at->format("Y-m-d")}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->phone}}</td>
						<td>
							@if( $user->hadCatalog )
							<a href="{{ route('admin.catalog',$user->hadCatalog->id) }}">معاينة</a>
							@else
							<span class="not-designed">لا يوجد</span>
							@endif
						</td>
						<td>
							@if($user->cannot("is-subscriber"))
							<activat-button @activate-account="startActiving" :user="{{$user->id}}" >
							@else
							<span class="is-active">مفعل</span>
							@endif
						</td>
					</tr>
					@empty
					<tr>
						<td>
							<div class="danger-message">لا يوجد اي مستعمل</div>
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>

		<div class="modal fade" id="modalActivateAccount" tabindex="-1" role="dialog" aria-labelledby="ActivateAccountModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="uab modal-title" id="ActivateAccountModalLabel">تفعيل حساب</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>نوع الإشتراك</label>
							<select v-model="subscribe" class="custom-input-form" >
								@foreach($subs as $sub)
								<option value="{{$sub->id}}">{{ $sub->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">الغاء</button>
						<button type="button" class="btn btn-primary" @click="continueActivate()" >تفعيل</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection