@extends("front/layouts/app")

@section("title","الأحداث")

@section("content")

@include("front/layouts/header")

<section class="admin-events">
	<div class="container-table">
		<h1 class="title">@yield("title")</h1>

		<div class="table-container">
			<table class="table simple-table table-striped" >
				<thead>
					<tr class="uab" >
						<td>الاسم</td>
						<td>العنوان</td>
						<td>الصور</td>
						<td>الفيديو</td>
						<td>المصمم</td>
						<td>الحالة</td>
					</tr>
				</thead>
				<tbody>
					@forelse($events as $event)
					<tr>
						<td>
							@if($event->user)
							<a href="{{ route('admin.users.show',$event->user->id) }}" >{{ $event->user->name }}</a>
							@else
							<span class="danger-color">تم حذف المستخدم</span>
							@endif
						</td>
						<td>{{ $event->title }}</td>
						<td>{{ $event->files()->OnlyImages()->count() }}</td>
						<td>{{ $event->files()->OnlyVideo()->count() }}</td>
						<td>
						@if(!is_null($event->designer))
						<span class="info-color">{{$event->designer->name}}</span>
						@else
						<span class="danger-color">لا يوجد</span>
						@endif
						</td>
						<td>
							@if($event->catalog)
								<a href="{{route('admin.catalog',$event->catalog->id)}}">مصمم</a>
							@else
								<span class="danger-color">غير مصمم</span>
							@endif
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="150" >
							<div class="danger-message"></div>
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>

</section>

@endsection