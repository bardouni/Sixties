<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Msg extends Model
{
    protected $guarded = [] ;

    protected $appends = ["humanstime","sideFloat","label"] ;

    public function scopeNotread($query)
    {
    	return $query->whereNull('read_at') ;
    }

    public function getHumanstimeAttribute()
    {
    	\Carbon\Carbon::setLocale('ar');
    	return $this->created_at->diffForHumans() ;
    }

    public function files()
    {
        return $this->hasMany(\App\ElementMsg::class , "msg_id" , "id") ;
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class,"from_user_id","id") ;
    }


    public function fromUser()
    {
        return $this->belongsTo(\App\User::class,"from_user_id","id") ;
    }
    public function toUser()
    {
        return $this->belongsTo(\App\User::class,"to_user_id","id") ;
    }



    public function getSideFloatAttribute()
    {

        if($this->from_user_id == auth()->user()->id) 
            return "right" ;
        else {
            if(auth()->user()->can("is-admin") && $this->from_user_id == 0 )
                return "right" ;
            else
                return "left" ;
        }
    }

    public function getLabelAttribute()
    {
        if($this->from_user_id == auth()->user()->id){
            return "أنت" ;
        } else if ($this->from_user_id == 0){
            return "الادارة" ;
        } else {
            if($this->fromUser->can("is-designer"))
                return "المصمم" ;
            else
                return "العميل" ;
        }
    }
}
