<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $guarded = [] ;

    public function user()
    {
    	return $this->belongsTo(\App\User::class,"user_id","id") ;
    }
    public function catalog()
    {
    	return $this->belongsTo(\App\Catalog::class,"catalog_id","id") ;
    }
}
