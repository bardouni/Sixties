<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $error = null ;       

        if($exception instanceof \Illuminate\Auth\AuthenticationException){
            return response()->view("errors.all",[
                "code"      => 403,
                "message"   => "المرجو تسجيل الدخول الى حسابك اولا." ,
                "action"    => 'صفحة <a class="uab" href="/login">تسجيل الدخول</a>'
            ],403) ;
        }
        elseif($exception instanceof \Illuminate\Auth\Access\AuthorizationException){
            return response()->view("errors.all",[
                "code"      => 403,
                "message"   => "لا يمكنك الولوج الى هته الصفحة ." ,
                "action"    => 'توجه الى <a class="uab" href="/"> الصفحة الرئسية</a>'
            ],403) ;
        }
        elseif($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException){
            return response()->view("errors.all",[
                "code"      => 404,
                "message"   => "الصفحة التي تطلبها غير موجودة ."
            ],404) ;
        }
        elseif($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException){
            return response()->view("errors.all",[
                "code"      => 404,
                "message"   => "لقد تم حذف الصفحة ."
            ],404) ;
        }
        elseif($exception instanceof \Illuminate\Session\TokenMismatchException){
            return response()->view("errors.all",[
                "code"      => 401,
                "message"   => "لقد استغرقت وقتا طويلا قبل إرسال البيانات ، نرجو منك الإعادة إعادة المحاولة بعد تحديث الصفحة ."
            ],401) ;
        }
        elseif($exception instanceof \Illuminate\Validation\ValidationException){
            // return response()->view("errors.all",[
            //     "code"      => 400,
            //     "message"   => "البيانات المرسلة خاطئة ."
            // ],400) ;
            return parent::render($request, $exception);
        }
        return response()->view("errors.all",[
            "code"      => 500,
            "message"   => "حدت خطأ أثناء العملية ." ,
            "action"    => 'توجه الى <a class="uab" href="/"> الصفحة الرئسية</a>'
        ],403) ;
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
