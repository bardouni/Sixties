<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
	use Notifiable , SoftDeletes;

	protected $table = "users" ;

	protected $guarded = [] ;

	protected $hidden = [
	'password', 'remember_token',
	];

	protected $casts = [
	"subscriber" 		=> "boolean",
	"deleted_at"		=> "datetime",
	"follower_id"		=> "integer"
	] ;

	public function events()
	{
		return $this->hasMany(\App\Event::class,'user_id','id') ;
	}

	public function filesevents()
	{
		return $this->hasMany(\App\ElementEvent::class,'user_id','id') ;
	}

	public function follower()
	{
		return $this->belongsTo(\App\User::class,"follower_id","id") ;
	}

	public function getLastEventAttribute()
	{
		return $this->events->last() ;
	}

	public function scopeWithNoFolower($query)
	{
		return $query->whereNull("follower_id") ;
	}
	public function catalogs()
	{
		return $this->hasMany(\App\Catalog::class,"user_id","id") ;
	}
	public function getHadCatalogAttribute()
	{
		return $this->catalogs()->get()->last() ;
	}
	public function getCatalogUrlAttribute()
	{
		if($this->catalogs()->count() > 0)
			return route('archive.show' , $this->catalogs()->get()->last()->id ) ;
		return route("archive.nonecatalog") ;
	}

	public function notes()
	{
		return $this->hasMany(\App\Note::class,"user_id","id") ;
	}
	public function subscribes()
	{
		return $this->hasMany(\App\Subscribe::class,"user_id","id") ;
	}
	public function setAvatarAttribute($value)
	{
		if(is_null($value))
			return ;

		if(isset($this->attributes["avatar"])) {
			\File::delete("avatars/".$this->attributes["avatar"]) ;
		}

		$name =  str_random(40) . time() . rand(1000,9999) .".". $value->getClientOriginalExtension() ;
		$value->move("avatars",$name) ;
		$this->attributes["avatar"] = $name ;
	}
	public function getCurentSubscribeAttribute()
	{
		return $this->subscribes()->active()->get()->last() ;
	}
	public function getHasNoSubscribeAttribute()
	{
		return $this->subscribes()->where("activated","1")->count() === 0 ;
	}

	public function initialCatalog()
	{
		return $this->belongsTo(\App\SubsCatalog::class , "subscribe_id" , "id") ;
	}
	// msgs

	public function msgsIn()
	{
		return $this->hasMany(\App\Msg::class,"to_user_id","id") ;
	}

	public function msgsOut()
	{
		return $this->hasMany(\App\Msg::class,"from_user_id","id") ;
	}

	public function groupMsgsIn()
	{
		return $this->hasMany(\App\GroupMsg::class,"to_user_id","id") ;
	}

	public function groupMsgsOut()
	{
		return $this->hasMany(\App\GroupMsg::class,"from_user_id","id") ;
	}

	public function getGroupmsgsAttribute()
	{
		return \App\GroupMsg::where("to_user_id",$this->id)->orWhere("from_user_id",$this->id) ;
	}

	// designer

	public function eventsThroughUsers()
	{
		return $this->hasManyThrough(
			\App\Event::class , \App\User::class ,
			"follower_id" , "user_id" , "id"
			) ;
	}

	public function eventsThroughUsersById()
	{
		return $this->hasMany(\App\Event::class , "designer_id","id")  ;
	}

	public function scopeNotdesigner($query)
	{
		return $query->where("role","user");
	}

	public function scopeAvailableUsersWithMyCustomers($query)
	{
		return $query->where([
			["follower_id","=",null],
			["role","=","user"]
			])->orWhere([
			["role","=","user"],
			["follower_id","=",$this->id]
			]) ;
		}
		public function customers()
		{
			return $this->hasMAny(User::class,"follower_id","id") ;
		}
		public function catalogsDesigned()
		{
			return $this->hasMany(\App\Catalog::class,"designer_id","id") ;
		}

		public function scopeDesigners($query)
		{
			return $query->where("role","designer") ;
		}


	// users

		public function scopeOnlyUsers($query)
		{
			return $query->where("role","user") ;
		}

		public function scopeNotAdmin($query)
		{
			return $query->where([["role","<>","admin"],["role","<>","superadmin"]]) ;
		}

		public function scopeAdmins($query)
		{
			return $query->where("role","admin") ;
		}

		public function scopeAdminOrSuperAdmin($query)
		{
			return $query->where("role","admin")->orWhere("role","superadmin") ;
		}

		public function getAvatarUrlAttribute()
		{
			if(!$this->attributes['avatar'])
				return "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+" ;
			return $this->attributes['avatar'] ;
		}

		public function getCompletAvatarUrlAttribute()
		{
			if($this->attributes['avatar'])
				return "/avatars/". $this->avatarUrl ;

			return $this->avatarUrl ;
		}

		public function notesThroughCustomers()
		{
			return $this->hasManyThrough('App\Note','App\User' , 'follower_id','user_id','id') ;
		}

	}
