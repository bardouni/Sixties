<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [] ;

    protected $table = "events" ;

    protected $casts = [
    	"designed" => "boolean"
    ] ;

    public function files()
    {
    	return $this->hasMany(\App\ElementEvent::class,"event_id",'id') ;
    }

    public function getAvatarAttribute()
    {
    	if( $this->files()->whereIn("file_type",["image/png","image/jpeg"])->count() > 0 )
    		return "/upsevents/{$this->files()->first()->src}" ;

    	return "/systemups/emptyavatar.png" ;
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class,"user_id","id") ;
    }

    public function scopeDesignedEvents($query)
    {
        return $query->where("designed",true) ;
    }

    public function catalog()
    {
        return $this->belongsTo(\App\Catalog::class,"catalog_id","id") ;
    }

    public function scopeNotDesigned($query)
    {
        return $query->where("designed",false) ;
    }

    public function designer()
    {
        return $this->belongsTo(\App\User::class,"designer_id","id") ;
    }

}
