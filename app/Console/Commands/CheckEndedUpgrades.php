<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckEndedUpgrades extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkendedupgrades';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Ended Upgrades';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->Check(10 , "عشرة أيام") ;
        $this->Check(30 , "تلاثون يوما") ;
        $this->CheckEnds() ;
    }

    public function Check($days , $lbl)
    {
        $subscribes = \App\Subscribe::with("user")->whereRaw("ADDDATE(started_at,days) = ADDDATE(CURDATE() , ?)" , $days )->where("ended",false)->where("activated",true)->get() ;        
        foreach ($subscribes as $subscribe) {
            if($subscribe->user)
                \Mail::to($subscribe->user->email)->send(new \App\Mail\NotifayAboutEndSubscribe($lbl)) ; 
        }
        
    }

    public function CheckEnds()
    {
        $subscribes = \App\Subscribe::with("user")->whereRaw("ADDDATE(started_at,days) <= CURDATE()")
        ->where("ended",false)
        ->where("activated",true)
        ->get() ;

        foreach ($subscribes as $subscribe) {
            $subscribe->ended = true ;
            $subscribe->save() ;
            if($subscribe->user){
                \Mail::to($subscribe->user->email)->send(new \App\Mail\SubscribeEnded()) ; 
            }
        }
        
    }
}
