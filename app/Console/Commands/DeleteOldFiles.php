<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteOldFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteoldfiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete old event files whitch not added to event .';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\ElementEvent::whereRaw("date(created_at) < ADDDATE(CURDATE(),-1)")->whereNull('event_id')->get()->map(function ($e)
        {
            $e->delete() ;
        }) ;

        \App\Zip::whereRaw("date(created_at) < ADDDATE(CURDATE(),-1)")->get()->map(function ($z)
        {
            $z->delete() ;
        }) ;
        
    }
}
