<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementEvent extends Model
{
    protected $guarded = [] ;

    protected $appends = ["type"] ;

    protected $typeImages = ["image/jpeg","image/png"] ;

    public function scopeOnlyImages($query)
    {
    	return $query->where("file_type","image") ;
    }

    public function scopeOnlyVideo($query)
    {
    	return $query->where("file_type","video") ;
    }

    public function setFileTypeAttribute($value)
    {
        if(strpos($value, "image") !== false ){
            $this->attributes["file_type"] = "image" ;
        } else {
            $this->attributes["file_type"] = "video" ;
        }
    }

    public function getTypeAttribute()
    {
        return $this->file_type ;
    }

}
