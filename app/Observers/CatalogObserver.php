<?php

namespace App\Observers;

use App\Catalog ;
use App\User ;

class CatalogObserver
{
    public function created(Catalog $catalog)
    {
    	User::findOrFail($catalog->user_id)->notify(new \App\Notifications\NewCatalog($catalog)) ;
    }

    public function updated(Catalog $catalog)
    {
    	User::findOrFail($catalog->user_id)->notify(new \App\Notifications\UpdateCatalog($catalog)) ;
    }

}