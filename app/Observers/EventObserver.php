<?php

namespace App\Observers;

use App\Event ;

class EventObserver
{
    
    public function created(Event $event)
    {
    	if($event->user->can("has-follower")){
    		$event->user->follower->notify(new \App\Notifications\UserAddEvent($event->user->id , $event->id));
    	}
    }

}