<?php

namespace App\Observers;

use App\Subscribe;
use App\User ;

class SubscribeObserver
{

    public function created(Subscribe $subscribe)
    {

        if(auth()->user()->cannot("is-admin")){

        	User::adminOrSuperAdmin()->get()->map(function ($admin) use ($subscribe)
        	{        		
        	    $admin->notify(new \App\Notifications\NewSubscribe($subscribe->id));
        	}) ;  

        } else {
        	$this->activate($subscribe) ;
        }

    }

    private function activate(Subscribe $subscribe)
    {
    	if($subscribe->activated){
    		$subscribe->user->update([
    			"subscriber" => true 
    		]);    		
    	}
    }

    public function updated(Subscribe $subscribe)
    {
		if($subscribe->activated){
            $subscribe->user->update([
                "subscriber" => !$subscribe->ended
            ]) ;
		}
    }
}