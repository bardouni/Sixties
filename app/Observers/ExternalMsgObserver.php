<?php

namespace App\Observers;

use App\User ;
use App\ExternalMsg ;

class ExternalMsgObserver
{
    
    public function created(ExternalMsg $msg)
    {        
    	User::adminOrSuperAdmin()->get()->map(function ($admin) use ($msg)
    	{
    	    $admin->notify(new \App\Notifications\NewExternalMessage($msg->email , $msg->id ));
    	}) ;
    }

}