<?php

namespace App\Observers;

use App\Zip ;

class ZipObserver
{
    
    public function deleting(Zip $zip)
    {
    	\File::delete("./public/zips/".$zip->src) ;
        return true ;
    }

}