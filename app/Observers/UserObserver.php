<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    
    public function created(User $user)
    {
        User::adminOrSuperAdmin()->get()->map(function ($admin) use ($user)
        {
            $admin->notify(new \App\Notifications\UserRegistred($user->id));
        }) ;
    }

}