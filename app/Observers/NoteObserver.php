<?php

namespace App\Observers;

use App\Note ;

class NoteObserver
{
    
    public function created(Note $note)
    {
    	$auth = auth()->user() ;
    	
        if($auth->cannot("is-designer") && $auth->follower){
        	$auth->follower->notify(new \App\Notifications\NewNote($note)) ;
        } else {
        	if($note->catalog && $note->catalog->user){
        		$note->catalog->user->notify(new \App\Notifications\NewNote($note)) ;
        	}
        }
    }


}