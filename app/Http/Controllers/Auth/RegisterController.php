<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class RegisterController extends Controller
{   
    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except("updateprofile");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|alpha_spaces|unique:users,name',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            // 'city' => 'required',
            // 'location' => 'required',
            // 'street' => 'required',
            'birthday' => 'date|required',
            'phone' => 'required|numeric',
            // 'postal' => 'required',
            // 'ngbh' => 'required',
            'terms' => 'accepted',
            'avatar' => 'image',            
            'subscribe_id' => ['required',
                Rule::exists('subs_catalogs','id')->where(function ($query) {
                    $query->where('visible',true);
                })
            ]
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

            'city' => $data['city'] ,
            'location' => $data['location'] ,
            'street' => $data['street'] ,
            'ngbh' => $data['ngbh'] ,
            
            'birthday' => $data['birthday'] ,
            'phone' => $data['phone'] ,
            'postal' => $data['postal'] ,
            'subscribe_id' => $data['subscribe_id'] ,
            'avatar' => (isset($data["avatar"]) ? $data["avatar"] : null)
        ]);
    }

    public function updateprofile(Request $request)
    {
        $auth = auth()->user() ;

        $this->validate($request , [
            'name' => 'required|max:255|alpha_spaces|unique:users,name',
            'email' => 'required|email|max:255|unique:users,email,'. $auth->id ,
            'city' => 'required',
            'location' => 'required',
            'street' => 'required',
            'birthday' => 'required|date',
            'phone' => 'required|numeric',
            'postal' => 'required',
            'ngbh' => 'required',            
            'avatar' => 'image',

            'oldpassword' => 'required_with:changepassword|correct_password:' .$auth->id ,
            'password' => 'min:6|confirmed|required_with:changepassword'
        ]);


        $data = array_except($request->all(),["password_confirmation","changepassword","oldpassword","remember_token","created_at","updated_at","follower_id","role","deleted_at","subscriber"]) ;

        if(isset($data['password'])){
            $data['password'] = bcrypt($data['password']) ;
        }

        auth()->user()->update($data) ;

        
        return back()->with([
            "success-message" => "تم حفظ البيانات" 
        ]);

    }

}
