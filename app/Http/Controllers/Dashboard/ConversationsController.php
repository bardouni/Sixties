<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupMsg ;

class ConversationsController extends Controller
{

	public function __construct()
	{
		$this->middleware("AccessGroupMsgs")->only("show") ;
		// $this->middleware("HasFollower")->only("store") ;
	}

	public function index()
	{
		$auth = auth()->user() ;
		$groups = $auth->getGroupmsgsAttribute()->get() ;

		return view('front/msgs/index',compact("groups","auth")) ;
		
	}
	
	public function show(GroupMsg $group)
	{
		$auth = auth()->user() ;

		$group->msgs()->where("to_user_id",$auth->id)->update([
		    "read_at" => (new \DateTime())
		]) ;

		$msgs = $group->msgs()->with("files")->orderBy("id","asc")->get() ;
		
		$last_msg_id = count($msgs) > 0 ? $msgs->last()->id : 0 ;
		
		$withAdmin = ($group->from_user_id == 0 || $group->to_user_id == 0 ) ;

		return view("front/msgs/group",compact("group","msgs","auth","last_msg_id","withAdmin")) ;
				
	}

	public function store(Request $request)
	{
		$auth = auth()->user() ;

		$this->validate($request ,[
			"titlemsg" => "required" ,
			"contentmsg" => "required",
			"target"=>"in:admin" . ( $auth->can("has-follower") ? ",designer" : "" )
		]) ;

		$target = ($request->target == "admin") ? 0 : $auth->follower_id ;
			
		$group = $auth->groupMsgsOut()->create([
			"to_user_id" => $target ,
			"title" => $request->titlemsg 
		]) ;

		$auth->msgsOut()->create([
			"content" => $request->contentmsg ,
			"group_id" => $group->id ,
			"to_user_id" => $target
		]) ;

		return back()->with(["success-message" => "تم ارسال الرسالة"]) ;
	}

}
