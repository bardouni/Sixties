<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Catalog ;

class ArchiveController extends Controller
{

    public function index()
    {
    	$events = auth()->user()->events ;
        
    	return view("front/catalogs/index",compact("events")) ;
    }

    public function show(Catalog $catalog , Request $request)
    {
        $auth = auth()->user() ;

        // abort if not the owner 
        abort_if($catalog->user_id != $auth->id , 404) ;
        
        $auth->unreadNotifications()->where([["data","like", '%"catalog_id":'. $catalog->id . ',%']])->update(["read_at" => \Carbon\Carbon::now() ]) ;
        // dd(request()->notes) ;
    	return view("front/catalogs/show",compact("catalog","auth")) ;
    }

    public function download(Catalog $catalog)
    {
        return response()->file("catalogs/".$catalog->src) ;
    }

    public function nonCatalog()
    {
        if(auth()->user()->catalogs()->count() > 0)
            return redirect()->route('archive.show' , auth()->user()->catalogs()->get()->last()->id ) ;

        return view("front/catalogs/noncatalog") ;
    }

}
