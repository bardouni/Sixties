<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event ;
class EventsController extends Controller
{
    
    public function create()
    {
    	$generated_token = rand(999,1000) . auth()->user()->id . time() ;

        $auth = auth()->user() ;

    	return view('front/events/create' , compact("generated_token","auth")) ;
    }

    public function store(Request $request)
    {

        $auth = auth()->user() ;

        $this->validate($request , [
            "title" => "required" ,
            "description" => "required"
        ]) ;

    	$designer_id = $auth->follower ? $auth->follower->id : null ;
        
        $createdEvent = $auth->events()->create([
            "title" => $request->title ,
            "description" => $request->description ,
            "designer_id" => $designer_id 
        ]) ;

        

        $files = $auth->filesevents()->where([
            ["event_id","=",null] ,
            ["generated_token","=",$request->generated_token]
        ])->get() ;
        
        $createdEvent->files()->saveMany($files) ;
        
        return back()->with(["success-message"=>"تم اضافة الحدث بنجاح."]);
    }

    public function storeFiles(Request $request)
    {
        
    	$this->validate($request,[
            "files.*" => "required|mimes:bmp,mp4,jpeg,jpg,jpe,png,mov,avi"
    	]) ;
        
    	$array_src = [] ;

        if(is_array($request->file('files'))) {
        	foreach ($request->file('files') as $file) {

    	    	$filename = auth()->user()->id . time() . rand(1000,9999) . "." . $file->getClientOriginalExtension() ;

                $createdFile = auth()->user()->filesevents()->create([
                    'src' => $filename ,
                    'generated_token' => $request->generated_token ,
                    'file_type' => $file->getMimeType()
                ]) ;
                                
    	    	$file->move("upsevents" , $filename ) ;

                array_push($array_src , ['src' => $filename ,"file_type" =>  $createdFile->file_type , "id" => $createdFile->id ] ) ;

        	}
        } else {            
            abort(422) ;
        }
    	return $array_src ;
    }

    public function deleteFile($file)
    {
        $el = auth()->user()->filesevents()->findOrFail($file) ;

        $el->delete() ;

        return [
            'code' => true ,
            'msg' => "image Deleted"
        ] ;
    }

    public function show(Event $event)
    {
        // abort if not user event
        abort_if($event->user_id != auth()->id() , 404 ) ;

        return view("front/events/show",compact("event")) ;
    }

}
