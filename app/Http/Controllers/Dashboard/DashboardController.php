<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;

use Mail ;
use App\Mail\verifyMail ;


class DashboardController extends Controller
{

	public function __construct()
	{
        $this->middleware('auth') ;
		$this->middleware('IsSimpleUser')->only("renwal") ;

	}

    public function welcome()
    {
    	$auth = \Auth::user() ;

        $usersWithoutFollower = User::withNoFolower()->count() > 0 ;

    	return view('front/dashboard/index',compact("auth","usersWithoutFollower")) ;
    }

    public function profile()
    {
    	$user = auth()->user() ;

    	return view("auth/profile",compact("user")) ;
    }

    public function verify()
    {
        $auth = auth()->user() ;
        if(is_null(\DB::table('verefyemailtable')->where([["user_id","=",auth()->user()->id]])->first())){
            
            $gnCode = rand(100000,999999) ;

            \DB::table('verefyemailtable')->insert([
                "code" => $gnCode ,
                "user_id" => auth()->user()->id 
            ]) ;
            
            Mail::to(auth()->user()->email)->send(new verifyMail($gnCode , "رسالة تفعيل الحساب")) ;

        }
        return view("auth/verify",compact("auth")) ;
    }

    public function postVerify(Request $request)
    {
        $this->validate($request,[
            "code" => "required",
        ]) ;        

        $code = \DB::table('verefyemailtable')->where([
                ["code","=",$request->code],
                ["user_id","=",auth()->user()->id]
            ])->first() ;
        
        if(is_null($code)){            
            return redirect('/me/verify')->withErrors([
                "الكود المدخل خاطئ" ,
            ]) ;
        } else {

            auth()->user()->update([
                "emailverified" => true
            ]) ;

            \DB::table('verefyemailtable')->where([["user_id","=",auth()->user()->id]])->delete() ;

            return redirect("dashboard")->with( "success-message" , "تم تفعيل حسابك ." ) ;
        }

    }

    public function renwal()
    {
        $subscribe = auth()->user()->curentSubscribe ;
        $catalog = auth()->user()->initialCatalog ? auth()->user()->initialCatalog : \App\SubsCatalog::visible()->first() ;
        return view("front/resubscribe",compact("subscribe","catalog")) ;
    }
    
}
