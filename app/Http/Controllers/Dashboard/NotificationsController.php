<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Msg ;
class NotificationsController extends Controller
{
    public function index()
    {
	    $auth = auth()->user() ;
		
	    if($auth->can("is-admin")){
		    $msgs = Msg::notread()->where(function ($query){
		    	return $query->where("from_user_id",0)->orWhere("to_user_id",0) ;

		    })->get() ;
	    } else {
	    	$msgs = $auth->msgsIn()->notread()->get() ;
	    }

		return [
			"notifications" => $auth->unreadNotifications()->where("type","App\Notifications\UserAddEvent")->get() ,
			"msgs" => $msgs ,
			"catalogNotification" => $auth->unreadNotifications()->whereIn("type",["App\Notifications\UpdateCatalog","App\Notifications\NewCatalog"])->get() ,
			"notes" => $auth->unreadNotifications()->where("type","App\Notifications\NewNote")->get() 
		] ;
    }

    public function markAsRead()
    {
    	auth()
    	->user()
    	->unreadNotifications()
    	// ->whereNot("type","App\Notifications\UserAddEvent")
    	->each(function ($notif)
    	{
    		$notif->markAsRead() ;
    	});
    	// ->markAsRead() ;
    }

}
