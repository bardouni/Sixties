<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Note ;
use App\Catalog ;

class NotesController extends Controller
{
    public function store(Request $request , Catalog $catalog )
    {
    	$this->validate($request,[
    		"note" => "required"
    	]) ;

    	$auth = auth()->user() ;
    	
        abort_if( $catalog->user_id != $auth->id && $catalog->designer_id != $auth->id , 503 ) ;

    	$auth->notes()->create([
    		"content" => $request->note ,
    		"catalog_id" => $catalog->id
    	]) ;

    	// need ti notify the designer 

    	return back()->with(["success-message"=>"تم إرسال الملاحظة"]);
    }
}
