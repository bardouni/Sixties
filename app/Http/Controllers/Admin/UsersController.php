<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;
use App\SubsCatalog ;
use App\Subscribe ;
class UsersController extends Controller
{
    public function index()
    {
    	$users = User::onlyUsers()->get() ;
        $subs = SubsCatalog::visible()->get() ;
    	return view("admin/users/index",compact("users","subs")) ;
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            "user" => "required|exists:users,id" ,
            "subscribe" => "required|exists:subs_catalogs,id"
        ]) ;

        $user = User::findOrFail($request->user) ;

        $subscribeCatalog = SubsCatalog::findOrFail($request->subscribe) ;

        $subscribe = Subscribe::notActivated()->where("user_id",$request->user)->get()->last() ;

        if(is_null($subscribe)){
            $user->subscribes()->create([
                "days"=>$subscribeCatalog->days ,
                "name"=>$subscribeCatalog->name ,
                "activated"=>true ,
                "ended" => false ,
                "started_at" => \Carbon\Carbon::now() 
            ]) ;
        } else {
            $subscribe->update([
                "days"=>$subscribeCatalog->days ,
                "name"=>$subscribeCatalog->name ,
                "activated"=>true ,
                "ended" => false ,
                "started_at" => \Carbon\Carbon::now() 
            ]) ;
        }

    	if(!$request->ajax()) {
            return back()->with([
                "success-message" => "تم تفعيل اشتراك المستخدم"
            ]) ;
        }
    }

    public function show(User $user)
    {
        $events = $user->events()->where("designed",false)->get() ;
        $devents = $user->events()->where("designed",true)->get() ;
        return view("designer/users/show",compact("user","devents","events")) ;
    }    
}
