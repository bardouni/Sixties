<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event ;
use App\Catalog ;
class EventsController extends Controller
{
    public function index()
    {
    	$events = Event::with("catalog")->get() ;
    	return view("admin/events/index",compact("events")) ;
    }

    public function catalog(Catalog $catalog)
    {
        $auth = auth()->user() ;
        return view("front/catalogs/show",compact("catalog","auth")) ;
    }

}
