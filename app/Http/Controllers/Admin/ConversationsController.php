<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupMsg ;
use App\User ;
use App\Msg ;

class ConversationsController extends Controller
{

    public function index()
    {
    	$auth = auth()->user() ;
    	$groups = GroupMsg::adminMsgs()->get() ;
    	$users = User::notAdmin()->get() ;
    	return view('admin/msgs/index',compact("groups","auth","users")) ;
    	
    }
    public function store(Request $request)
    {
    	$this->validate($request ,[
    		"titlemsg" => "required" ,
    		"contentmsg" => "required",
    		"target"=>"required"
    	]) ;

    	$auth = auth()->user() ;

    	$target = \App\User::findOrFail( $request->target ) ;
    		
    	$group = GroupMsg::create([
    		"to_user_id" => $target->id ,
    		"title" => $request->titlemsg ,
    		"from_user_id" => 0
    	]) ;
    
    	Msg::create([
    		"content" => $request->contentmsg ,
    		"group_id" => $group->id ,
    		"to_user_id" => $target->id ,
    		"from_user_id" => 0
    	]) ;

    	return back()->with(["success-message" => "تم ارسال الرسالة"]) ;
    }

    public function show(GroupMsg $group)
    {
    	$auth = auth()->user() ;

    	Msg::where("to_user_id",0)->update([
    	    "read_at" => (new \DateTime())
    	]) ;

    	$msgs = $group->msgs()->orderBy("id","asc")->get() ;
    	
    	$last_msg_id = count($msgs) > 0 ? $msgs->last()->id : 0 ;
    	    	
    	return view("admin/msgs/group",compact("group","msgs","auth","last_msg_id")) ;
    			
    }

}
