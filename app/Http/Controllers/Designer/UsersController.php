<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;

class UsersController extends Controller
{
	public function index()
	{
		$auth = auth()->user() ;

		$users = $auth->AvailableUsersWithMyCustomers()->orderBy("follower_id","asc")->get() ;
		
		return view("designer/users/index",compact("users","auth")) ;
	}
	public function store(Request $request)
	{
		$users_true = array_keys(array_where($request->users,function ($value,$key){return ($value == "true");})) ;

		$users_false = array_keys(array_where($request->users,function ($value,$key){return ($value == "false") ;})) ;

		$auth = auth()->user() ;
		
		$auth->AvailableUsersWithMyCustomers($auth->id)->whereIn("id",$users_true)->update([
			"follower_id" => $auth->id
		]) ;

		$auth->AvailableUsersWithMyCustomers($auth->id)->whereIn("id",$users_false)->update([
			"follower_id" => null
		]) ;
		
		\App\Event::whereIn("user_id",$users_true)->update([
			"designer_id" => $auth->id
		]) ;

		return back()->with(["success-message"=>"تم حفظ التعديلات ."]) ;
	}

	public function show(User $user)
	{
		$events = $user->events()->where("designed",false)->get() ;
		$devents = $user->events()->where("designed",true)->get() ;
		return view("designer/users/show",compact("user","devents","events")) ;
	}

	public function usersJson()
	{
		return auth()->user()->customers ;
	}
}
