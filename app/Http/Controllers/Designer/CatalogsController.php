<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;
use App\Catalog ;
use App\Event ;

class CatalogsController extends Controller
{
	public function create()
	{
		return view("designer/catalogs/create") ;
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			"customer"=>"required",
			"events"=>"required" ,
			"catalog" => "required|mimes:pdf"
		]);

		\DB::beginTransaction();

		$user = User::findOrFail($request->customer) ;

		$auth = auth()->user() ;

		abort_if($user->follower_id != $auth->id , 403) ;

		$events = $auth->eventsThroughUsers()->find(array_keys($request->events)) ;

		$ids = array_pluck($events->toArray(),"id") ;

		$evs = Event::whereIn("id",$ids)->update([
			"designed" => true
		]) ;

		$catalog = $user->catalogs()->create([
			"src" => $request->catalog ,
			"designer_id" => $auth->id
		]) ;

		$catalog->events()->saveMany($events) ;

		\DB::commit();
		
		\Mail::to($user->email)->send(new \App\Mail\emailcatalog( asset("catalogs/".$catalog->src) )) ;

		return back()->with([
			"success-message" => "تم ارسال المجلة ."
		]) ;
	}

	public function show(Catalog $catalog)
	{
		$auth = auth()->user() ;

		// if not the catalog designer
		abort_if($catalog->designer_id != $auth->id , 404) ;

		return view("front/catalogs/show",compact("catalog","auth")) ;
	}

	public function update(Catalog $catalog , Request $request)
	{
		$this->validate($request,[
			"catalog" => "required|mimes:pdf"
		]);

		$catalog->update([
			"src" => $request->catalog
		]);

		if(!$catalog->user){
			return back()->withErrors([
				"error"	=> ["لقد تم حذف هذا المستخدم."]
			]);
		}

		$user = $catalog->user ;

		\Mail::to($user->email)->send(new \App\Mail\emailcatalog( asset("catalogs/".$catalog->src) , true )) ;

		return back()->with([
			"success-message" => "تم تعديل المجلة بنجاح ."
		]);

	}

}
