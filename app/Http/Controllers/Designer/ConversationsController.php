<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\GroupMsg ;

class ConversationsController extends Controller
{

	public function __construct()
	{
		$this->middleware("AccessGroupMsgs")->only("show") ;
	}

	public function index()
	{
		$auth = auth()->user() ;
		$groups = $auth->getGroupmsgsAttribute()->get() ;
		$customers = $auth->customers ;
		return view('designer/msgs/index',compact("groups","auth","customers")) ;
	}

	public function store(Request $request)
	{
		$auth = auth()->user() ;
		
		$this->validate($request ,[
			"titlemsg" => "required" ,
			"contentmsg" => "required",
			"target"=>"required"
		]) ;
		
		$target = ($request->target == "admin") ? 0 : $request->target ;
		
		abort_if($target != 0 && \App\User::findOrFail($target)->follower_id != $auth->id , 403 , "not your customer " ) ;

		$group = $auth->groupMsgsOut()->create([
			"to_user_id" => $target ,
			"title" => $request->titlemsg 
		]) ;

		$auth->msgsOut()->create([
			"content" => $request->contentmsg ,
			"group_id" => $group->id ,
			"to_user_id" => $target
		]) ;

		return back()->with(["success-message" => "تم ارسال الرسالة"]) ;
	}

	
	public function show(GroupMsg $group)
	{
		$auth = auth()->user() ;

		$group->msgs()->where("to_user_id",$auth->id)->update([
		    "read_at" => (new \DateTime())
		]) ;

		$msgs = $group->msgs()->orderBy("id","asc")->get() ;
		
		$last_msg_id = count($msgs) > 0 ? $msgs->last()->id : 0 ;
		
		$withAdmin = ($group->from_user_id == 0 || $group->to_user_id == 0 ) ;
		
		return view("designer/msgs/group",compact("group","msgs","auth","last_msg_id","withAdmin")) ;

	}

}
