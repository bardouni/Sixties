<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotesController extends Controller
{
    public function index()
    {
		$notes = auth()->user()->notesThroughCustomers()->orderBy("id","desc")->get() ;

    	return view("designer/notes/index",compact("notes")) ;
    }
   
}
