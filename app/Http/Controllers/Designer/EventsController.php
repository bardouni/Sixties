<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event ;
use App\User ;
class EventsController extends Controller
{

	public function __construct()
	{
		$this->middleware("AccessEvent")->only("show") ;
	}

	public function index()
	{
		$auth = auth()->user() ;

		$events = $auth->eventsThroughUsers()->with("catalog")->get() ;
		
		return view("designer/events/index",compact("events")) ;
	}

	public function show(Event $event)
	{
		$user = $event->user ;

		return view("designer/events/show",compact("event","user")) ;
	}

	public function eventsUserJson(User $user)
	{
		abort_if(( is_null($user) || $user->follower->id != auth()->user()->id) , 403) ;
		return $user->events()->notDesigned()->get() ;
	}

	public function zip(Event $event)
	{
		abort_if($event->designer_id != auth()->user()->id , 404) ;

		if($event->files->count() < 1){
			return back()->withErrors(["لم يتم إرفاق اية ملفات ."]);
		}

		$files = [] ;

		foreach ($event->files as $file) {
			if(!\File::exists("./upsevents/{$file->src}"))
				continue ;
			array_push($files, glob("./upsevents/{$file->src}"));
		}

		if( count($files) < 1){
			return back()->withErrors(["لم يتم إرفاق اية ملفات ."]);
		}

		$name = auth()->id() . time() . rand() . ".zip" ;

		\Zipper::make("zips/$name")->add($files);

		\App\Zip::create([
			"src" => "zips/$name"
		]);

		return redirect("zips/$name") ;

	}

	public function newEvents()
	{
		$auth = auth()->user() ;

		$notifs = $auth->unreadNotifications()->where("type","App\Notifications\UserAddEvent")->get() ;

		$eventsIds = [] ;

		foreach ($notifs as $notif) {
			if (!isset($notif->data["event_id"]))
				continue;
			array_push($eventsIds, $notif->data["event_id"]) ;
		}

		$events = $auth->eventsThroughUsers()->whereIn("events.id",$eventsIds)->with("catalog")->get() ;

		return view("designer/events/index-last",compact("events")) ;
	}

	public function readnewEvents()
	{
		auth()->user()->unreadNotifications->markAsRead() ;

		return back() ;
	}

}
