<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupMsg ;
use App\Msg ;

class MsgsController extends Controller
{
	public function __construct()
	{
		$this->middleware("AccessGroupMsgs") ;
	}

    public function store(Request $request , GroupMsg $group)
    {
    	$this->validate($request,[
    	    "content" => "required"
    	]) ;

    	$auth = auth()->user() ;

    	$target = ($group->to_user_id == $auth->id ) ? $group->from_user_id : $group->to_user_id ;
    	
    	abort_if($target != 0 && \App\User::findOrFail($target)->follower_id != $auth->id , 403 );
    	
    	$msg = $auth->msgsOut()->create([
    	    "to_user_id" => $target ,
    	    "content" => $request->content,
    	    "group_id" => $group->id
    	]) ;

    	if($request->file('files')) {
    	    foreach ($request->file('files') as $file) {
    	        
    	        $filename = auth()->user()->id . time() . rand(1000,9999) . "." . $file->getClientOriginalExtension() ;

    	        $createdFile = $msg->files()->create([
    	            'src' => $filename ,
    	            'file_type' => $file->getMimeType()
    	        ]) ;

    	        $file->move("upsmsgs" , $filename ) ;
    	            
    	        // array_push($array_src , ['src' => $filename , "id" => $createdFile->id ] ) ;

    	        $msg->files()->save($createdFile) ;
    	    }
    	}
    }

    public function show(Request $request , GroupMsg $group , $msg)
    {

        $auth = auth()->user() ;

        $msgs = $group->msgs()->with("files")->where("id" , ">" , $msg )->get() ;

        $group->msgs()->whereNull("read_at")->where("to_user_id",$auth->id)->update([
            "read_at" => (new \DateTime())
        ]) ;

        return $msgs ;
    }

}
