<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Settings ;
class HomeController extends Controller
{    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subs = \App\SubsCatalog::visible()->orderBy("order","asc")->get() ;
        $works = \Settings::get("works") ;
        return view('front/welcome',compact("works","subs"));
    }

    public function aboutus()
    {
        return view("front/layouts/page",[
            "content" => Settings::get('contentUs') ,
            "title" => "من نحن" ,            
        ]) ;
    }

    public function goals()
    {
        return view("front/layouts/page",[
            "content" => Settings::get('goals') ,
            "title" => "أهدافنا"
        ]) ;
    }

    public function vision()
    {
        return view("front/layouts/page",[
            "content" => Settings::get('vision') ,
            "title" => "رؤيتنا"
        ]) ;
    }

    public function terms()
    {
        return view("front/layouts/page",[
            "content" => Settings::get('terms') ,
            "title" => "شروط الإستخدام"
        ]) ;
    }

    public function postcontact(Request $request)
    {
        $this->validate($request , [
            "name" => "required" ,
            "email" => "email|required" ,
            "contentmsg" => "required"
        ]) ;

        \App\ExternalMsg::create([
            "content" => $request->contentmsg ,
            "name" => $request->name ,
            "email" => $request->email ,
        ]) ;

        return back()->with([
            "success-message" => "تم ارسال الرسالة بنجاح ." 
        ]) ;
    }

    public function contact()
    {
        return view("front/layouts/page",[
            "content" => Settings::get('contact') ,
            "title" => "تواصل معنا" ,
            "contactForm" => true
        ]) ;
    }

    public function passresttoken($token)
    {
        $email = \DB::table('password_resets')->where('token', $token)->first();

        abort_if(is_null($email), '404');

        $email = $email->email;

        return view('auth/passwords/reset', compact('token', 'email'));
    }

}
