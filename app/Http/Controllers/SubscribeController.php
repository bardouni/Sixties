<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SubsCatalog ;

class SubscribeController extends Controller
{

	public function __construct()
	{
		$this->middleware("GuestOrNotSubscribed")->only("index") ;
		$this->middleware("AuthOrRegister")->only("show","store") ;
		$this->middleware("EmailVerified")->only("show","store") ;
		$this->middleware("NotSubscribed")->only("show","store") ;
	}

	public function index()
	{
		$subs = SubsCatalog::visible()->orderBy("order","asc")->get() ;
		return view('front/subscribes/index' , compact("subs"));
	}

	public function show(SubsCatalog $catalog)
	{
		abort_if(!$catalog->visible,404) ;
		
		return view("front/subscribe/show",compact("catalog")) ;
	}

	public function store(Request $request)
	{
		$this->validate($request , [
			"picpay" => "required|image"
		]) ;

		$auth = auth()->user() ;

		$auth->subscribes()->create([
			"file" => $request->picpay ,
			"ended" => false
		]) ;

		return back()->with(["success-message"=>"تم إرسال صورة الحوالة، شكرًا لاشتراككم، سيتم تفعيل الحساب في أقرب وقت، نرجو العودة قريبًا ."]) ;
	}
}
