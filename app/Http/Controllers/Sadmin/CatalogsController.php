<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Catalog ;

class CatalogsController extends Controller
{
    public function index()
    {
    	$catalogs = Catalog::paginate(10,["*"],"catalogs") ;
    	return view("sadmin/catalogs/index",compact("catalogs")) ;
    }
    public function show(Catalog $catalog)
    {
    	$notes = $catalog->notes ; 
    	$events = $catalog->events ;
    	return view("sadmin/catalogs/show",compact("catalog","notes","events")) ;
    }
}
