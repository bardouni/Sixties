<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Settings ;
use App\Subscribe ;
use App\User ;
use App\Event ;
use App\Msg ;
class HomeController extends Controller
{
    public function index()
    {
        $usersNeedActivate = User::whereHas("subscribes",function ($query)
        {
            $query->where([["ended","=",false],["activated","=",false]]) ;

        })->get() ;

        $activatedThisMonth = User::whereHas("subscribes",function ($query)
        {
            $query->where("activated",true)->whereMonth("updated_at","=","11")->whereYear("updated_at","=","2016") ;

        })->get() ;

        $events = Event::all() ;

        $r = \DB::table("users")->select(\DB::raw("count(*) as `value`, date(`created_at`) as `date` "))->groupBy(\DB::raw('date(`created_at`)'))->orderBy(\DB::raw("date(`created_at`)"),"asc")->take(10)->get() ;

    	return view("sadmin/welcome",[
            "usersLength" => User::count() ,
            "usersNeedActivate" => $usersNeedActivate ,
            "activatedThisMonth" => $activatedThisMonth ,
            "designedEvents" => $events->where("designed",true)->count(),
            "events" => $events->count() ,
            "lastEvents" => Event::take(10)->orderBy("id","desc")->get(),
            "recentUsers" => User::take(10)->orderBy("id","desc")->get(),
            "lastSubscribes" => Subscribe::where([["ended","=",false]])->orderBy("id","desc")->take(10)->get(),

            "charEventsDesigned" => (new Event)->where("designed",true)->count() ,
            "charEventsNotDesigned" => (new Event)->where("designed",false)->count() ,
            "charEventsNotFollowed" => (new Event)->where("designer_id",null)->count() ,
            "recentMsgs" => Msg::take(10)->orderBy("id","desc")->get() ,
            "dataCharUsers" => [
                "labels" => array_pluck($r,"date") ,
                "data" => array_merge(array_pluck($r,"value"),[0])
            ]
        ]);
    }

    public function notifications()
    {
    	$auth = auth()->user() ;
    	$notifications = $auth->notifications()->paginate(15,["*"],"notifications") ;
    	$auth->unreadNotifications->markAsRead() ;
    	return view("sadmin/notifications",compact("notifications")) ;
    }

    public function us()
    {
        return view("sadmin/pages/template",[
            "content" => Settings::get("contentUs") ,
            "pageName" => "من نحن" ,
            "cuurentPage" => 1 ,
            "target" => "contentUs"
        ]) ;
    }

    public function terms()
    {
        return view("sadmin/pages/template",[
            "content" => Settings::get("terms") ,
            "pageName" => "شروط الإستخدام" ,
            "cuurentPage" => 5 ,
            "target" => "terms"
        ]) ;
    }

    public function goals()
    {
        return view("sadmin/pages/template",[
            "content" => Settings::get("goals") ,
            "pageName" => "أهدافنا" ,
            "cuurentPage" => 2 ,
            "target" => "goals"
        ]) ;
    }


    public function vision()
    {
        return view("sadmin/pages/template",[
            "content" => Settings::get("vision") ,
            "pageName" => "رؤيتنا" ,
            "cuurentPage" => 3 ,
            "target" => "vision"
        ]) ;
    }

    public function postPage(Request $request)
    {

        if($request->page == "social"){
            $this->validate($request ,[
                "facebook"  => "present",
                "instagram"    => "present",
                "youtube"  => "present",
                "twitter"   => "present"
            ]);
            $data = array_only($request->all() , ["facebook","instagram","youtube","twitter"]) ;
            \Settings::set('social',$data) ;
        } else if ($request->page == "works"){
            // dd($request->file('images')) ;
            $this->validate($request ,[
                "images"    => "array",
                "images.*"  => "image"
            ]);

            $imgs = \Settings::get("works",[]) ;

            foreach (($request->file("images") ?: []) as $image) {
                array_push($imgs, $image->getClientOriginalName()) ;
                $image->move("systemups/works/" , $image->getClientOriginalName() ) ;
            }
            \Settings::set("works",$imgs) ;

        } else if ($request->page == "slider"){

            $this->validate($request ,[
                "images"    => "array",
                "images.*"  => "image"
            ]);

            $imgs = (\Settings::get("slider",[]) ?: []) ;

            foreach (($request->file("images") ?: []) as $image) {
                array_push($imgs, $image->getClientOriginalName()) ;
                $image->move("systemups/slider/" , $image->getClientOriginalName() ) ;
            }
            \Settings::set("slider",$imgs) ;

        } else {
            Settings::set($request->page , $request->content ) ;
        }

        return back()->with([
            "success-message" => "تم حفظ التعديلات ."
        ]);
    }

    public function contact()
    {
        return view("sadmin/pages/template",[
            "content" => Settings::get("contact") ,
            "pageName" => "تواصل معنا" ,
            "cuurentPage" => 4 ,
            "target" => "contact"
        ]) ;
    }

    public function social()
    {
        return view("sadmin/pages/social") ;
    }


    public function works()
    {
        return view("sadmin/pages/works") ;
    }

    public function deletework(Request $request)
    {
        $works = \Settings::get('works' , []) ;

        foreach ($works as $i=>$w) {
            if ($w == $request->image){
                array_splice( $works , $i ,1) ;
                \File::delete("/systemups/works/" . $w ) ;
                break ;
            }
        }

        \Settings::set('works' , $works )  ;
        return back()->with([
            "success-message"   => "تم حفظ التعديلات ."
        ]);
    }

    public function deleteslider(Request $request)
    {
        $slider = ( \Settings::get('slider') ?: [] ) ;

        foreach ($slider as $i=>$w) {
            if ($w == $request->image){
                array_splice( $slider , $i ,1) ;
                \File::delete("/systemups/slider/" . $w ) ;
                break ;
            }
        }

        \Settings::set('slider' , $slider )  ;
        return back()->with([
            "success-message"   => "تم حفظ التعديلات ."
        ]);
    }

}
