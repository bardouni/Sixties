<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;
class DesignersController extends Controller
{
    public function index()
    {    	
    	return view("sadmin/designers/index") ;
    }

    public function destroy(User $designer)
    {
    	$designer->update([
    		"role" => "user"
    	]) ;

    	return back()->with(["success-message"=>"تم إلغاء ترقية المصمم"]) ;
    }
    
    public function addcustomer(User $designer , Request $request)
    {
        return $this->workWithCustomers("تم إضافة المستخدم للمصمم" , $designer->id , $request ) ;
    }
    public function trashcustomer(Request $request)
    {
        return $this->workWithCustomers("إلغاء متابعة المستخدم من طرف المصمم" , null , $request ) ;
    }

    public function workWithCustomers($msg ,$value,$request)
    {
         $this->validate($request , [
            "customer" => "required|exists:users,id" ,
        ]) ;

        User::findOrFail($request->customer)->update([
            "follower_id" => $value
        ]) ;

        return back()->with([
            "success-message" => $msg
        ])  ;
    }

}
