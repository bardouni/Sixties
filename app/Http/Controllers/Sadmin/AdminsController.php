<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;

class AdminsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:is-absolute-superadmin') ;
    }
    public function index()
    {
    	$admins = User::admins()->paginate(10,["*"],"admins") ;
    	return view("sadmin/admins/index",compact("admins")) ;
    }

    public function destroy(User $admin)
    {
    	$admin->update([
    		"role" => "user"
    	]) ;

    	return back()->with(["success-message"=>"تم حذف المدير من الادارة"]) ;
    }
}
