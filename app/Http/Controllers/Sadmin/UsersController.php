<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User ;
use App\SubsCatalog ;
class UsersController extends Controller
{
	public function index()
	{
		return view("sadmin/users/index") ;
	}

	public function edit(User $user)
	{
		$groups = $user->groupmsgs->get() ;

		if($user->can("is-designer")){
			$designer = $user ;
			$events = $designer->eventsThroughUsersById()->paginate(10,["*"],"events") ;
			$customers = $designer->customers()->paginate(10,["*"],"customers") ;
			$countCustomers = User::OnlyUsers()->count() ;
			$catalogs = $designer->catalogsDesigned()->paginate(10,["*"],"catalogs") ;
		    $usersNotFollowedByThisDesigner = User::withNoFolower()->onlyUsers()->get() ;
		    return view("sadmin/designers/edit",compact("designer","user","events","groups","customers","countCustomers","catalogs","usersNotFollowedByThisDesigner")) ;
		} else if ($user->can("is-simpleuser")) {
			$subscribes = SubsCatalog::visible()->get() ;
			$events = $user->events()->paginate(10,["*"],"events") ;
			$designers = User::designers()->get() ;
			$catalogs = $user->catalogs ;
			return view("sadmin/users/edit",compact("user","subscribes","catalogs","events","groups","designers")) ;
		} else {
			return view("sadmin/admins/edit",compact("user")) ;
		}

	}

	public function update(User $user , Request $request)
	{
		$this->validate($request,[
			"email" => "required|unique:users,email,".$user->id ,
			"password" => "required_with:changepassword|min:6",
			'name' => 'required|max:255|alpha_spaces',
			// 'city' => 'required',
			// 'location' => 'required',
			// 'street' => 'required',
			'birthday' => 'required|date',
			'phone' => 'required|numeric',
			// 'postal' => 'required',
			// 'ngbh' => 'required',
			'avatar' => "image"
		]) ;

		$data = array_except($request->all() , ["changepassword","password_confirmation"]) ;

		if(isset($data["password"]))
			$data["password"] = bcrypt($data["password"]) ;

		if(isset($data['follower_id'])){
			$data['follower_id'] = (int)$data['follower_id'] ;
		}

		$user->update($data) ;

		return back()->with([
			"success-message" => "تم حفظ التعديلات"
		]) ;
	}

	public function newrole(User $user , $role )
	{

		$user->update([
			"role" => $role
		]) ;

		return back()->with([
			"success-message" => "تم ترقية الحساب"
		]) ;

	}

	public function create()
	{
		return view("sadmin/users/create") ;
	}

	public function store(Request $request)
	{

		$this->validate($request, [
			'name' => 'required|max:255|alpha_spaces',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
			// 'city' => 'required',
			// 'location' => 'required',
			// 'street' => 'required',
			'birthday' => 'required|date',
			'phone' => 'required|numeric',
			// 'postal' => 'required',
			// 'ngbh' => 'required',
			'avatar' => "image"
		]);

		$data = array_except($request->all() , "password_confirmation") ;

		$data["password"] = bcrypt($data["password"]) ;
		$data["subscribe_id"] = 1 ;

		User::create($data) ;

		return back()->with([
			"success-message" => "تم إنشاء المستخدم"
		]) ;

	}

	public function destroy(User $user)
	{
		$user->delete() ;

		return redirect()->route("sadmin.users.index")->with([
			"success-message" => "تم حذف المستعمل ."
		]) ;
	}

	public function trashed()
	{
		$users = User::onlyTrashed()->paginate(10,["*"],"users") ;
		return view("sadmin/users/trashed",compact("users")) ;
	}

	public function undo($user)
	{
		User::withTrashed()->findOrFail($user)->restore() ;

		return back()->with([
			"success-message" => "تم استعادة المستعمل بنجاح ."
		]) ;
	}

}
