<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubsCatalog ;
class SubscribesCatalogController extends Controller
{
    public function index()
    {
    	$catalogs = SubsCatalog::all() ;
    	return view("sadmin/subscribescatalog/index" , compact("catalogs") ) ;
    }

    public function destroy(SubsCatalog $catalog)
    {
    	$catalog->visible = !$catalog->visible ;
    	$catalog->save() ;
    	
    	return $this->successResponseBack() ;
    }
    public function edit(SubsCatalog $catalog)
    {
    	return view("sadmin/subscribescatalog/edit",compact("catalog")) ;
    }

    public function update(SubsCatalog $catalog , Request $request)
    {
    	$this->validate($request , [
    		"name" => "required|unique:subs_catalogs,name," . $catalog->id ,
    		"price" => "required|numeric" ,
    		"days" => "required|numeric" 
    	]) ;
    	
    	$data = array_except( $request->all() , ["_token","_method"] ) ;
    	$catalog->update($data) ;

    	if($request->popular){
    		SubsCatalog::where("id","<>",$catalog->id)->update(["popular"=>false]) ;
    	}

    	return $this->successResponseBack() ;

    }

    protected function successResponseBack($msg = null)
    {
    	return back()->with([
    		"success-message" => $msg ?: "تم حفظ التعديلات بنجاح"
    	]) ;
    }

    public function create()
    {
    	return view("sadmin/subscribescatalog/create") ;
    }

    public function store(Request $request)
    {
    	$this->validate($request , [
    		"name" => "required|unique:subs_catalogs,name" ,
    		"price" => "required|numeric" ,
    		"days" => "required|numeric"
    	]) ;
    	
    	$data = array_except( $request->all() , ["_token","_method"] ) ;
    	
    	$catalog = SubsCatalog::create($data) ;

    	if($request->popular){
    		SubsCatalog::where("id","<>",$catalog->id)->update(["popular"=>false]) ;
    	}

    	return $this->successResponseBack("تم اضافة الباقة") ;
    }

}


