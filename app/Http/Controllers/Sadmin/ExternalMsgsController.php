<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ExternalMsg ;
use Mail ;

class ExternalMsgsController extends Controller
{
    public function index()
    {
    	$msgs = ExternalMsg::paginate(15,["*"],"msgs") ;
    	return view("sadmin/msgs/external/index",compact("msgs")) ;
    }

    public function show(ExternalMsg $msg)
    {    	
    	return view("sadmin/msgs/external/show",compact("msg")) ;
    }

    public function destroy(ExternalMsg $msg)
    {
    	$msg->delete() ;
    	return redirect()->route("sadmin.externalmsgs.index")->with([
    		"success-message" => "تم حذف الرسالة ."
    	]) ;
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		"contentmsg" => "required",
    		"subject" => "required",
    		"target" => "required|email",
    	])	;
    	
    	Mail::to($request->target)->send(new \App\Mail\SendExternalMessage($request->contentmsg ,$request->subject)) ;
    	
    	ExternalMsg::where("id",$request->msg)->update([
    		"reply" => $request->contentmsg
    	]) ;
    	
		return back()->with([
			"success-message" => "تم إرسال الرسالة" ,
		]) ;
    }

    public function create()
    {
    	return view("sadmin/msgs/external/create") ;
    }

}
