<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event ;

class EventsController extends Controller
{
    public function index()
    {
    	$events = Event::paginate(10,["*"],"events") ;
    	return view("sadmin/events/index",compact("events")) ;
    }

    public function show(Event $event)
    {
    	$files = $event->files ;
    	return view("sadmin/events/show",compact('event','files')) ;
    }
}
