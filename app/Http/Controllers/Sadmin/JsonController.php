<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JsonController extends Controller
{
	public function users(Request $request)
	{    	
		return $this->searchMaker(\App\User::onlyUsers()->with("follower"), $request->all() )->orderBy("id","desc")->paginate(10) ;
	}
	public function designers(Request $request)
	{
		return $this->searchMaker(\App\User::designers(), $request->all() )->orderBy("id","desc")->paginate(10) ;
	}
	public function admins(Request $request)
	{
		return $this->searchMaker(\App\User::admins(), $request->all() )->paginate(10) ;
	}
	public function msgs(Request $request)
	{
		return $this->searchMaker(new \App\GroupMsg(), $request->all() )->paginate(10) ;
	}
	public function catalogs(Request $request)
	{
		return $this->searchMaker(\App\Catalog::with("designer","user"), $request->all() )->paginate(10) ;
	}
	public function events(Request $request)
	{
		return $this->searchMaker(\App\Event::with("designer","user","catalog"), $request->all() )->orderBy("id","desc")->paginate(10) ;
	}
	public function subscriptions(Request $request)
	{
		return $this->searchMaker(\App\Subscribe::with("user"), $request->all() )->orderBy("id","desc")->paginate(10) ;
	}
	public function externalmsgs(Request $request)
	{
		return $this->searchMaker(new \App\ExternalMsg() , $request->all() )->paginate(10) ;
	}

	private function searchMaker($model , $columns)
	{
		foreach ($columns as $k => $v) {
			
			if( $this->avoid($k,$v))
				continue ;

			if($k == "id"){
				$model = $model->where($k,$v) ;
			} else {
				$model = $model->where($k , "like" , "%$v%") ;                
			}
		}

		return $model ;
	}

	public function avoid($k,$v)
	{
		return (is_null($v) || trim($v) == "" || $k == 'page') ;
	}

}
