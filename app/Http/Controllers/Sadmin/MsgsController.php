<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GroupMsg ;
use App\Msg ;
use App\User ;

class MsgsController extends Controller
{
    public function index()
    {    	
    	return view("sadmin/msgs/index") ;
    }

    public function show(GroupMsg $group)
    {
    	$msgs = $group->msgs ;
        $oneSide = $group->from_user_id ;
    	return view("sadmin/msgs/show",compact("group","msgs","oneSide")) ;
    }

    public function create()
    {
    	$users = User::notAdmin()->get() ;
    	return view("sadmin.msgs.create",compact("users")) ;
    }

    public function store(Request $request)
    {
    	$this->validate($request , [
            "titlemsg" => "required",
            "contentmsg" => "required" ,
            "target" => "required|exists:users,id"
        ]) ;

        $group = GroupMsg::create([
            "to_user_id" => $request->target ,
            "title" => $request->titlemsg ,
            "from_user_id" => 0
        ]);

        Msg::create([
            "group_id" => $group->id ,
            "to_user_id" => $request->target ,
            "content" => $request->contentmsg ,
            "from_user_id" => 0
        ]);

        return back()->with([
            "success-message" => "تم ارسال الرسالة" ,
        ]) ;

    }

    public function newMsg(Request $request , GroupMsg $group)
    {
        $this->validate($request , [
            "contentmsg" => "required" ,            
        ]) ;
        
        $group->msgs()->create([
            "to_user_id" => ( $group->from_user_id == 0 ? $group->to_user_id : $group->from_user_id ) ,
            "content" => $request->contentmsg ,
            "from_user_id" => 0
        ]);

        return back()->with([
            "success-message" => "تم ارسال الرسالة" ,
        ]) ;
    }

}
