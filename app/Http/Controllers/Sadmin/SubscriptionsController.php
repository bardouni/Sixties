<?php

namespace App\Http\Controllers\Sadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subscribe ;
use App\SubsCatalog ;
use App\User ;

class SubscriptionsController extends Controller
{
    public function index()
    {    	
    	return view("sadmin/subscriptions/index") ;
    }

    public function destroy(Subscribe $subscribe)
    {
    	$subscribe->update([
    		"ended" => true 
    	]) ;

    	return back()->with([
    		"success-message" => "تم إنهاء الإشتراك"
    	]) ;
    }

    public function edit(Subscribe $subscribe)
    {
    	$subs = SubsCatalog::visible()->get() ;
    	return view("sadmin/subscriptions/edit",compact("subscribe","subs")) ;
    }

    public function update(Subscribe $subscribe , Request $request)
    {
    	$SubscribeCataloge = SubsCatalog::findOrFail($request->subscribecatalog) ;

    	$subscribe->update([
    		"activated" => true ,
    		"ended" => false,
    		"name" => $SubscribeCataloge->name ,
    		"days" => $SubscribeCataloge->days ,
            "started_at" => \Carbon\Carbon::now() 
    	]) ;

    	return back()->with([
			"success-message" => "تم تفعيل الإشتراك"
    	]) ;
    }

    public function create()
    {
    	$users = User::onlyUsers()->get() ;
    	$subs = SubsCatalog::visible()->get() ;
    	return view('sadmin/subscriptions/create',compact("users","subs")) ;
    }

    public function store(Request $request)
    {
        
    	$this->validate($request , [
    		"user" => "required|exists:users,id",
    		"subscribecatalog" => "required|exists:subs_catalogs,id" ,
            "started_at" => Carbon\Carbon::now() 
    	]) ;
    	
    	$SubscribeCataloge = SubsCatalog::findOrFail($request->subscribecatalog) ;

    	Subscribe::create([
    		"user_id" => $request->user ,
    		"activated" => true ,
    		"ended" => false,
    		"name" => $SubscribeCataloge->name ,
    		"days" => $SubscribeCataloge->days ,
            "started_at" => \Carbon\Carbon::now() 
    	]) ;

    	return back()->with([
    		"success-message" => "تم تفعيل اشتراك المستخدم " 
    	]) ;
    }

}
