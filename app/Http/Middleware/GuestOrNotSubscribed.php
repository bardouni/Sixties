<?php

namespace App\Http\Middleware;

use Closure;

class GuestOrNotSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Auth::check() || auth()->user()->cannot('is-subscriber'))
            return $next($request);

        return redirect('/') ;
    }
}
