<?php

namespace App\Http\Middleware;

use Closure;

class AuthOrRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if(!\Auth::check()){            
            return redirect("/register" . ($request->catalog ? "?p=" .$request->catalog->id : "" )) ;
        }

        return $next($request);
    }
}
