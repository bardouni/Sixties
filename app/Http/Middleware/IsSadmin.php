<?php

namespace App\Http\Middleware;

use Closure;

class IsSadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->cannot("is-superadmin"))
            return abort(404) ;

        return $next($request);
    }
}
