<?php

namespace App\Http\Middleware;

use Closure;

class AccessGroupMsgs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if($request->user()->can("access-group-msgs",$request->group))
            return $next($request);

        return abort(403,"you can't access this convertation ") ;
    }
}
