<?php

namespace App\Http\Middleware;

use Closure;

class IsDesigner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->can("is-designer"))
            return $next($request);
        throw new \Illuminate\Auth\Access\AuthorizationException() ;        
    }
}
