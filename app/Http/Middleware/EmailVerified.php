<?php

namespace App\Http\Middleware;

use Closure;

class EmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if($request->user() && $request->user()->cannot("is-admin") && $request->user()->cannot("email-verified"))
            return redirect("me/verify") ;

        return $next($request);
    }
}
