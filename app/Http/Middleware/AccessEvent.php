<?php

namespace App\Http\Middleware;

use Closure;

class AccessEvent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->user()->can("access-event" , $request->event))
            return $next($request);

        return abort(403,"can't be here ") ;

    }
}
