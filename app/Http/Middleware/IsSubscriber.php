<?php

namespace App\Http\Middleware;

use Closure;

class IsSubscriber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->cannot('is-subscriber')){
            return abort(404) ;
        }
        return $next($request);
    }
}
