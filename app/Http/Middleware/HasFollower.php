<?php

namespace App\Http\Middleware;

use Closure;

class HasFollower
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->cannot("has-follower")){
            dd("can't send Msg") ;
        }
        return $next($request);
    }
}
