<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        
        'IsSubscriber' => \App\Http\Middleware\IsSubscriber::class,
        'HasFollower' => \App\Http\Middleware\HasFollower::class,
        'AccessGroupMsgs' => \App\Http\Middleware\AccessGroupMsgs::class,
        'IsDesigner' => \App\Http\Middleware\IsDesigner::class,
        'IsAdmin' => \App\Http\Middleware\IsAdmin::class,
        'AccessEvent' => \App\Http\Middleware\AccessEvent::class,
        'NotSubscribed' => \App\Http\Middleware\NotSubscribed::class,
        'IsSadmin' => \App\Http\Middleware\IsSadmin::class,
        'GuestOrNotSubscribed' => \App\Http\Middleware\GuestOrNotSubscribed::class,
        'EmailVerified' => \App\Http\Middleware\EmailVerified::class,
        'emailNotVerified' => \App\Http\Middleware\emailNotVerified::class,
        'AuthOrRegister' => \App\Http\Middleware\AuthOrRegister::class,
        'IsSimpleUser' => \App\Http\Middleware\IsSimpleUser::class,
        
    ];
}
