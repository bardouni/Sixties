<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementMsg extends Model
{
    protected $guarded = [] ;

    public function setFileTypeAttribute($value)
    {
    	if(strpos($value, "image") !== false ){
    		$this->attributes["file_type"] = "image" ;
    	} else {
    		$this->attributes["file_type"] = "video" ;
    	}
    }
}
