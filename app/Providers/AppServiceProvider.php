<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ElementEvent ;

use App\Observers\UserObserver;
use App\Observers\SubscribeObserver;
use App\Observers\ExternalMsgObserver;
use App\Observers\EventObserver;
use App\Observers\CatalogObserver;
use App\Observers\NoteObserver;
use App\Observers\ZipObserver;


use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ElementEvent::deleting(function ($e)
        {
            \File::delete("./public/upsevents/{$e->src}");            
            return true ;
        });

        \View::composer('sadmin.*', function ($view) {
            \Carbon\Carbon::setLocale('ar');
            $view->with([
                "globalUnreadNotifications" => auth()->user()->unreadNotifications
            ]) ;
        });
        
        \App\Zip::observe(ZipObserver::class);
        \App\User::observe(UserObserver::class);
        \App\Subscribe::observe(SubscribeObserver::class);
        \App\ExternalMsg::observe(ExternalMsgObserver::class);
        \App\Event::observe(EventObserver::class);
        \App\Catalog::observe(CatalogObserver::class);
        \App\Note::observe(NoteObserver::class);

        Validator::extend('correct_password', function ($attribute, $value, $parameters, $validator) {
            return (\Hash::check($value, \App\User::findOrFail($parameters[0])->password ));
        });
        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u', $value);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
