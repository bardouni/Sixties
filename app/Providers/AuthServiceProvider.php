<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        \Gate::define('is-subscriber', function ($user) {
            return ($user->subscriber == true) ;
        });

        \Gate::define('is-simpleuser', function ($user) {
            return ($user->role == "user") ;
        });

        \Gate::define('is-designer', function ($user) {
            return ($user->role == "designer") ;
        });

        \Gate::define('is-admin', function ($user) {
            return ( $user->can("is-superadmin") || $user->role == "admin") ;
        });

        \Gate::define('is-superadmin', function ($user) {
           return ($user->role == "superadmin" || $user->role == 'admin' ) ;
        });

        \Gate::define('is-absolute-superadmin', function ($user) {
           return ($user->role == "superadmin" ) ;
        });

        \Gate::define('has-follower', function ($user) {
            return !is_null($user->follower) ;
        });

        \Gate::define('email-verified', function ($user) {
            return $user->emailverified ;
        });

        \Gate::define('access-group-msgs', function ($user , $group) {

            if($user->can("is-admin") && ($group->from_user_id == 0 || $group->to_user_id == 0))
                return true ;

            if($group->from_user_id == $user->id || $group->to_user_id == $user->id )
                return true ;

            return false ;

        });

        \Gate::define('access-event', function ($user , $event) {
            if(!is_null($event) && ( $event->user_id == $user->id || ( $user->id == $event->designer_id ) ) )
                return true ;
            return false ;
        });
    }
}
