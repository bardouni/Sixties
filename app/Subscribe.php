<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $guarded = [] ;
    
    // protected $appends = ["file"] ;
    
    protected $casts = [
        "activated" => "boolean",
        "ended" => "boolean"
    ] ;

    public function user()
    {
    	return $this->belongsTo(\App\User::class ,"user_id" ,"id") ;
    }

    public function setFileAttribute($value)
    {
    	if(is_null($value))
    		return ;
    	
    	$name = auth()->user()->id . time() . rand(1000,9999) .".". $value->getClientOriginalExtension() ;

    	$value->move("upssubscribe",$name) ;

    	$this->attributes["file"] = $name ;
    }

    public function scopeNotActivated($query)
    {
        return $query->where("activated","false") ;
    }

    public function scopeActive($query)
    {
        return $query->where([["activated","=",true],["ended","=",false]]) ;
    }

    public function getSaveUsersubscribeidAttribute()
    {
        return ($this->user ? $this->user->subscribe_id : -1) ;
    }

}
