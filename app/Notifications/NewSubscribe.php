<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewSubscribe extends Notification
{
    use Queueable;

    protected $subid ;
    
    public function __construct($subid )
    {
        $this->subid = $subid ;
    }

    
    public function via($notifiable)
    {
        return ['database'];
    }

    
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    
    public function toArray($notifiable)
    {
        return [
            "subId" => $this->subid,
            "msg" => "طلب إشتراك جديد"
        ];
    }
}
