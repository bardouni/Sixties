<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $guarded = [] ;

    public function events()
    {
    	return $this->hasMany(\App\Event::class,"catalog_id","id") ;
    }

    public function setSrcAttribute($value)
    {
        if($this->src)
            \File::delete("catalogs/".$this->src) ;

        $name = auth()->user()->id . time() . rand(100000,999999) . ".pdf" ;

        $value->move("catalogs" , $name) ;

    	$this->attributes["src"] = $name ;
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class,"user_id","id") ;
    }

    public function notes()
    {
        return $this->hasMany(\App\Note::class,"catalog_id","id") ;
    }

    public function designer()
    {
        return $this->belongsTo(\App\User::class ,"designer_id","id") ;
    }

}
