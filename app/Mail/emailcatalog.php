<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class emailcatalog extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $url ;
    private $newversion ;

    public function __construct($url , $newversion = false)
    {
        $this->url = $url ;
        $this->newversion = $newversion ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view("vendore/notifications/catalog")
        ->subject(!$this->newversion ? "إشعار بانشاء مجلة" : "إشعار بإرسال نسخة جديدة من المجلة")
        ->with([
            "url"   => $this->url ,
            "msg"   => (!$this->newversion ? "لقد تم إنشاء المجلة الخاصة بك، يمكنك تحميلها من الرابط التالي" : "تم إنشاء نسخة جديدة من المجلة الخاصة بك")
        ]) ;
    }
}
