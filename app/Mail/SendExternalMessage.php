<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendExternalMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $contentMsg ;
    public $subject ;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contentMsg , $subject)
    {        
        $this->contentMsg = $contentMsg ;
        $this->subject = $subject ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->view('sadmin/msgs/emailTemplate')->subject($this->subject)->with([
            "content" => $this->contentMsg ,
            "subject" => $this->subject
        ]);
    }
}
