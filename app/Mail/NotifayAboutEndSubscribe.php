<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifayAboutEndSubscribe extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $lbl ;

    public function __construct($lbl)
    {
        $this->lbl = $lbl ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('vendore/notifications/notifayaboutendsubscribe')
        ->subject("إشعار اقتراب إنتهاء مدة الإشتراك")
        ->with([
            "lbl"=>$this->lbl
        ]) ;
    }
}
