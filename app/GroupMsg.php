<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMsg extends Model
{

    protected $guarded = [] ;

	protected $appends = ["withAdmin","lastanswer"] ;

    public function getLastanswerAttribute()
    {
    	return $this->msgs->last() ;
    }
    public function msgs()
    {
    	return $this->hasMany(\App\Msg::class,"group_id","id") ;
    }

    public function scopeAdminMsgs($query)
    {
    	return $query->where("from_user_id",0)->orWhere("to_user_id",0) ;
    }

    public function getOtherContactAttribute()
    {
        return \App\User::find( $this->from_user_id == auth()->user()->id ?  $this->to_user_id : $this->from_user_id ) ;
    }
    public function getWithAdminAttribute()
    {
        return ($this->from_user_id == 0 || $this->to_user_id == 0) ;
    }

}
