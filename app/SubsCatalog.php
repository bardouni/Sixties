<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsCatalog extends Model
{
    protected $guarded = [] ;

    protected $casts = [
    	"data" => "array" ,
    	"popular" => "boolean" ,
    	"visible" => "boolean"
    ] ;

    public function scopeVisible($query)
    {
    	return $query->where("visible",true) ;
    }
    
}
